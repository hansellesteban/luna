-- MySQL dump 10.13  Distrib 5.6.21, for osx10.6 (x86_64)
--
-- Host: localhost    Database: hospital
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accountant`
--

DROP TABLE IF EXISTS `accountant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountant` (
  `accountant_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`accountant_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountant`
--

LOCK TABLES `accountant` WRITE;
/*!40000 ALTER TABLE `accountant` DISABLE KEYS */;
INSERT INTO `accountant` VALUES (1,'Wilson Went','contador@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','5555555');
/*!40000 ALTER TABLE `accountant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (2,'Camilo Luna','fenixagenciaweb@gmail.com','88fc9002d298df125f1acd3a0765dffe82c47fe2'),(1,'Admin','admin@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b'),(5,'Sergio Carrillo','scarillo@medyserv.com','be35afe3286c8814856796267800f7f5fc64b1ff'),(6,'Administrador','administrativo.suba@medyserv.com','bbd23258ef254295c5659e2e8bc85d1f0aece384');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `adverse_event`
--

DROP TABLE IF EXISTS `adverse_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `adverse_event` (
  `adverse_event_id` int(11) NOT NULL AUTO_INCREMENT,
  `clinical_record_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `adverse_event` tinyint(1) NOT NULL,
  `observations` longtext NOT NULL,
  `created` datetime NOT NULL,
  `creator` int(11) NOT NULL,
  `creator_type` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  `modifier` int(11) NOT NULL,
  `modifier_type` varchar(255) NOT NULL,
  PRIMARY KEY (`adverse_event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `adverse_event`
--

LOCK TABLES `adverse_event` WRITE;
/*!40000 ALTER TABLE `adverse_event` DISABLE KEYS */;
INSERT INTO `adverse_event` VALUES (1,1,'2015-11-16 08:16:51',1,'Tendinitis severa','2015-11-16 08:16:51',1,'admin','2015-11-17 04:08:57',1,'admin'),(2,6,'2015-11-16 20:23:21',1,'Se fracturo el otro pie','2015-11-16 20:23:21',1,'admin','2015-11-17 04:12:31',1,'admin'),(3,6,'2015-11-16 21:06:41',1,'Hematoma en el otro pie','2015-11-16 21:06:41',1,'admin','2015-11-17 04:12:57',1,'admin'),(4,7,'2015-11-17 00:03:08',1,'nada','2015-11-17 00:03:08',1,'admin','2015-11-17 00:30:33',1,'admin'),(5,21,'2015-11-17 18:50:11',1,'Se dislocó la otra mano','2015-11-17 18:50:11',1,'admin','2015-11-17 18:50:11',1,'admin'),(6,26,'2015-11-18 11:17:52',1,'Se fractura la otra mano','2015-11-18 11:17:52',1,'admin','2015-11-18 11:17:52',1,'admin'),(7,32,'2015-11-22 05:39:16',0,'','2015-11-22 05:39:16',1,'admin','2015-11-22 05:39:16',1,'admin');
/*!40000 ALTER TABLE `adverse_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointment` (
  `appointment_id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`appointment_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
INSERT INTO `appointment` VALUES (1,'1442588700',1,1,'approved'),(2,'1442347200',1,2,'approved'),(3,'1442847900',3,1,'pending'),(4,'1442415900',1,1,'approved'),(5,'1448236800',1,1,'approved');
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bed`
--

DROP TABLE IF EXISTS `bed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bed` (
  `bed_id` int(11) NOT NULL AUTO_INCREMENT,
  `bed_number` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` longtext NOT NULL COMMENT 'room,cabin,ICU',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0=unalloted;1=alloted',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bed_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bed`
--

LOCK TABLES `bed` WRITE;
/*!40000 ALTER TABLE `bed` DISABLE KEYS */;
INSERT INTO `bed` VALUES (1,'1','Sala de Recuperación',0,''),(2,'2','Sala de Recuperación',0,''),(3,'3','Sala de Recuperación',0,''),(4,'4','Sala de Recuperación',0,''),(5,'5','Sala de Recuperación',0,''),(6,'6','Sala de Recuperación',0,''),(7,'1','UCI',0,''),(8,'2','UCI',0,''),(9,'3','UCI',0,''),(10,'4','UCI',0,''),(11,'5','UCI',0,''),(12,'6','UCI',0,'');
/*!40000 ALTER TABLE `bed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bed_allotment`
--

DROP TABLE IF EXISTS `bed_allotment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bed_allotment` (
  `bed_allotment_id` int(11) NOT NULL AUTO_INCREMENT,
  `bed_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `allotment_timestamp` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `discharge_timestamp` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bed_allotment_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bed_allotment`
--

LOCK TABLES `bed_allotment` WRITE;
/*!40000 ALTER TABLE `bed_allotment` DISABLE KEYS */;
INSERT INTO `bed_allotment` VALUES (1,3,1,'1442293200','1442552400'),(2,4,2,'1442552400','1442811600');
/*!40000 ALTER TABLE `bed_allotment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blood_bank`
--

DROP TABLE IF EXISTS `blood_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blood_bank` (
  `blood_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `blood_group` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`blood_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blood_bank`
--

LOCK TABLES `blood_bank` WRITE;
/*!40000 ALTER TABLE `blood_bank` DISABLE KEYS */;
/*!40000 ALTER TABLE `blood_bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blood_donor`
--

DROP TABLE IF EXISTS `blood_donor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blood_donor` (
  `blood_donor_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `blood_group` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `sex` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_donation_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`blood_donor_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blood_donor`
--

LOCK TABLES `blood_donor` WRITE;
/*!40000 ALTER TABLE `blood_donor` DISABLE KEYS */;
INSERT INTO `blood_donor` VALUES (1,'Michael Bolton','O+','male',25,'5555555','michael@email.com','',1442293200),(2,'Daniel Davis','A+','male',29,'6666666','daniel@email.com','',1442466000);
/*!40000 ALTER TABLE `blood_donor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ci_sessions`
--

DROP TABLE IF EXISTS `ci_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ci_sessions`
--

LOCK TABLES `ci_sessions` WRITE;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` VALUES ('0903334f1d6c0419dd618debddf39dab7391fa0b','190.25.90.191',1447873693,'__ci_last_regenerate|i:1447873619;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('0b6ebe73f613349f094b8d18716ebb11b99381f9','141.101.110.154',1448180059,'__ci_last_regenerate|i:1448179796;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('0ba7d82fb2ac9eaffca9c896e5dddbb45c2cacc9','141.101.110.154',1448173022,'__ci_last_regenerate|i:1448173021;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('0bd06e2442b4306fdec45c8b5ba955edefda700e','181.63.98.12',1447900751,'__ci_last_regenerate|i:1447900705;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:31:\"Notice Info Updated Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('0d7700c0c834cbdfdf2af2058271758ce1ba8448','181.63.98.12',1447980250,'__ci_last_regenerate|i:1447980250;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('0e142b1d5cb71c7d437ad526a799befc54b5ea9c','141.101.110.154',1448165089,'__ci_last_regenerate|i:1448164927;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('0f6c67d3dd34d88e2941bfceab03f72cdab2cf20','181.63.98.12',1447979395,'__ci_last_regenerate|i:1447979154;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('12b5c4c3115026521f528f1d5fc787998cb1290f','181.63.98.12',1447984899,'__ci_last_regenerate|i:1447984732;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:31:\"Notice Info Updated Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('15f617d3ab8819b2c48c02e38cd1ad3c153dd30c','141.101.110.154',1448179450,'__ci_last_regenerate|i:1448179151;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('1825e6dde5c1dbabf3b3ddb0f3b8e1b80a0358ff','141.101.110.154',1448197048,'__ci_last_regenerate|i:1448196934;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";last_page|s:61:\"http://www.medyserv.com/sistema/accountant/invoice_add/create\";'),('18882940a705b2e66e0651409c818996448457dc','190.60.205.104',1447870474,'__ci_last_regenerate|i:1447870297;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('1990a5cb0778acf70609314febbcd6a37048374b','141.101.110.154',1448196163,'__ci_last_regenerate|i:1448195867;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('19db376ee3385e8b06750f85a904fc5295d0dc4c','141.101.110.154',1448188163,'__ci_last_regenerate|i:1448187873;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('1a2b66e361923f2de769222c5620ae0ca94106ff','141.101.110.154',1448163862,'__ci_last_regenerate|i:1448163862;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('1bc235fbf502f984986d0554f2352d7f73d77df8','181.63.98.12',1447890633,'__ci_last_regenerate|i:1447890545;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('1c6dbaab3f51504df60aed5d7486fc935277a576','141.101.110.154',1448085726,'__ci_last_regenerate|i:1448085598;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('1fcb4aa1f46aeeb010f06cd02d66a4a81069e825','190.60.205.104',1447865520,'__ci_last_regenerate|i:1447865520;'),('222a1febd351ecd0684472cbb7a31fd4083b3e07','141.101.110.154',1448186745,'__ci_last_regenerate|i:1448186722;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('2311c1ca72674537df63b4a230f18be4df488bda','141.101.110.154',1448248374,'__ci_last_regenerate|i:1448248374;'),('23156247f21726b126994531436f043185ccc8b2','141.101.110.154',1448169336,'__ci_last_regenerate|i:1448169053;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('23e12517bb4c82080167f982abb12a04866b6ec9','141.101.110.154',1448186382,'__ci_last_regenerate|i:1448186367;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('251ab79579cccc22a3adf82dabcc12e3e121ea53','190.60.205.104',1447871812,'__ci_last_regenerate|i:1447871812;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('258621e453833fb1a2640512ef05496f681534a7','181.63.98.12',1447909997,'__ci_last_regenerate|i:1447909716;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:31:\"Notice Info Updated Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('2628d03f0d717100e8e980e1e62c406437eff267','190.25.90.191',1447867288,'__ci_last_regenerate|i:1447866989;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('263bd94b9103be1032a9e9c5bb9c81e821ecf5c8','141.101.110.154',1448178514,'__ci_last_regenerate|i:1448178502;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('265df75d8a620ea9d87b557ac228f6b5b329572b','190.60.205.104',1447872724,'__ci_last_regenerate|i:1447872724;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('27cc3d910c8196b1614b783f03bb2d38cea76abf','141.101.110.154',1448189629,'__ci_last_regenerate|i:1448189367;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('27d3e6ab49238db4887bd8329014a4ebc72a2c0d','66.249.64.242',1447991719,'__ci_last_regenerate|i:1447991719;'),('2a3d4c5815970dbcfc57f3e852439b65454e7f76','190.60.205.104',1447865259,'__ci_last_regenerate|i:1447864966;patient_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:13:\"Paul Williams\";login_type|s:7:\"patient\";'),('2ace54f674c8ce57c9bebebf17c1f57aaeb0d5b7','181.63.98.12',1448241565,'__ci_last_regenerate|i:1448241330;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('2df97b816178679d20fc2338d9b49b42f0aec84a','198.245.51.90',1447971821,'__ci_last_regenerate|i:1447971821;'),('2e2f485202aabfebb7f807858b6f8aed0308089b','141.101.110.154',1448170019,'__ci_last_regenerate|i:1448169724;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('307380a025aecc368e28a44846aa6b6343e5774f','141.101.110.154',1448185137,'__ci_last_regenerate|i:1448184964;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('30c19b30131d1512f10527bb4f3de76c0c8b9ef8','141.101.110.154',1448182255,'__ci_last_regenerate|i:1448182084;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('31e58044d317a80d17939c3cfad1fbfbb386e1f3','::1',1449015032,'__ci_last_regenerate|i:1449015032;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('340da2f5f10a727cc5a375d0141ea7cba2177d25','141.101.110.154',1448171020,'__ci_last_regenerate|i:1448171020;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('35b4429191870ae9b02d811a23e0c715bf23d0d9','141.101.110.154',1448086038,'__ci_last_regenerate|i:1448086038;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('39746e6dfa082075e4b5ad9d6baeec521942f314','190.60.205.104',1447873209,'__ci_last_regenerate|i:1447873171;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('3a2ed97d38ab328e5b6e3845b4294390917c4d83','181.63.98.12',1447899083,'__ci_last_regenerate|i:1447898859;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:31:\"Notice Info Updated Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('3a8e89c99f5fb91834e1c04995aaca3c94503721','141.101.110.154',1448192169,'__ci_last_regenerate|i:1448191963;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('3a97c25e7ecbe0966ad4c863c2df65609e36177e','190.60.205.104',1447871509,'__ci_last_regenerate|i:1447871509;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('3aa97aa52beb1aea5acd636426b2219e90e856e5','181.63.98.12',1447983414,'__ci_last_regenerate|i:1447983122;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('3bac7ddcdca9357a47ac03f8e60f9ec4be45a9ef','190.60.205.104',1448288519,'__ci_last_regenerate|i:1448288490;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('3f050046b291ab60dda23173c46a6958270efd08','141.101.110.154',1448188860,'__ci_last_regenerate|i:1448188860;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('3f66a14684eb193094bdf2c0ea7abc1dbeecf991','190.60.205.104',1447868183,'__ci_last_regenerate|i:1447868183;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('3faccbd4f864c0a84e024bcbdc5ca0f84fb14b38','190.60.205.104',1447863165,'__ci_last_regenerate|i:1447862933;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('405d99ea9d99d18bb3ba2efbf76e23e96a612d35','141.101.110.154',1448195497,'__ci_last_regenerate|i:1448195221;nurse_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:13:\"Diane Rowling\";login_type|s:5:\"nurse\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"new\";}'),('409d56d7efcd2817110c72264568269550e06b3d','190.60.205.104',1447870902,'__ci_last_regenerate|i:1447870902;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('423fea0e0358120cc0cac6f633caddea96ed0d09','181.63.98.12',1447899337,'__ci_last_regenerate|i:1447899242;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('43d1a1f4a994be1ffb7f0fcdf599a55aac4b1d92','141.101.110.154',1448166035,'__ci_last_regenerate|i:1448165835;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('448d660ec294276fd2f2500feae64eb4edde6b74','181.63.98.12',1448002468,'__ci_last_regenerate|i:1448002216;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('45911cc6d27eac787a052f58c7d64491ad8b0ac5','190.60.205.104',1447867882,'__ci_last_regenerate|i:1447867882;'),('478134ab0d264029ee9d6b8a2135fa5d83df829b','141.101.110.154',1448178846,'__ci_last_regenerate|i:1448178846;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('494d3a731b1f0828ad69918d779663444016c36c','141.101.110.154',1448182945,'__ci_last_regenerate|i:1448182730;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('4c3eccb74b8c15dafecc27480873491c656e691e','181.63.98.12',1448287623,'__ci_last_regenerate|i:1448287622;'),('4df35610f37a75927a6d5b1ea18c683d0e1d2e81','181.63.98.12',1447979468,'__ci_last_regenerate|i:1447979468;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('4e33db99cd8336a7936ebd4df7fe51ac1b736346','141.101.110.154',1448177060,'__ci_last_regenerate|i:1448176847;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('4ef1f477f629be764fa1ffbfe52d934fec2e6ffa','190.60.205.104',1447868485,'__ci_last_regenerate|i:1447868485;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('500fea0c9294e7f92027eb0a3f21bc3bd833c92e','141.101.110.154',1448194846,'__ci_last_regenerate|i:1448194560;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('50a16a689d95e8cf6f2fb8c6bfa7165c2a8cbce2','190.25.90.191',1448043688,'__ci_last_regenerate|i:1448043688;'),('50b8c81cb2f944f9a1375ed9d12a6e48c940f7d7','190.60.205.104',1447868869,'__ci_last_regenerate|i:1447868720;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('50d25347aae4ae0b9171c962cf0f517cd50060aa','141.101.110.154',1448197534,'__ci_last_regenerate|i:1448197239;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";last_page|s:61:\"http://www.medyserv.com/sistema/accountant/invoice_add/create\";'),('5181c140018f5967d7aad651e65205acd5e90320','141.101.110.154',1448166416,'__ci_last_regenerate|i:1448166175;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('518392a0f47b273bf3f343788bb281db507ba1d5','141.101.110.154',1448176398,'__ci_last_regenerate|i:1448176209;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";last_page|s:46:\"http://www.medyserv.com/sistema/doctor/patient\";'),('519b08ad4b294b6c4e94bf32b439df985ebe9214','190.60.205.104',1447872420,'__ci_last_regenerate|i:1447872420;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('51c1f5c7c736b6b80f893d849415994ef063a971','173.245.55.188',1448158886,'__ci_last_regenerate|i:1448158886;'),('5314a5d5c26bfef01079b6c78219d8f3216ed868','181.63.98.12',1447984515,'__ci_last_regenerate|i:1447984246;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('540f6c015d6a7fac02e55499502a965eed37fa0e','64.79.100.48',1448378540,'__ci_last_regenerate|i:1448378539;'),('56c34c1282c48da427995aceb2205202e153e67f','141.101.110.154',1448187820,'__ci_last_regenerate|i:1448187546;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('58454126bf5204e990e57d75ca702325d1ae9a76','141.101.110.154',1448198194,'__ci_last_regenerate|i:1448198194;'),('58c0b35b9ae88a2160e4a27ac6bcbfaee6351f73','181.63.98.12',1448252274,'__ci_last_regenerate|i:1448251997;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('59565f8719e9de90be8e865004bfb0e6e3ab63e0','181.63.98.12',1447983758,'__ci_last_regenerate|i:1447983472;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('59fe55470b9bee4cb8c28e3a049f1237aac23c35','::1',1449014083,'__ci_last_regenerate|i:1449014082;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('5a6e0e9de48bb5421eaa6fa4c05f915ef42c4686','141.101.110.154',1448165449,'__ci_last_regenerate|i:1448165448;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('5b46f86a71365f2a9333b9c13d694a59615172f2','181.63.98.12',1447984185,'__ci_last_regenerate|i:1447983899;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('6038459c37b12a917479e0eafe8756c794741303','64.79.100.48',1448378540,'__ci_last_regenerate|i:1448378540;'),('66a022bfda3746a27b0ea57b75cd3b1702d00930','141.101.110.154',1448163720,'__ci_last_regenerate|i:1448163557;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('679050b43ad10cdb7bb3fa0661acc52ab470006a','141.101.110.154',1448192840,'__ci_last_regenerate|i:1448192578;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('68521c718d862b88a28f7939489309cf0519f4ee','141.101.110.154',1448188466,'__ci_last_regenerate|i:1448188182;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('699faf7c06832e052195615fb9d2e94474bdbbc0','141.101.110.154',1448230149,'__ci_last_regenerate|i:1448229989;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('6a585ddefa5c6c6b3ec98cc67e2ee3e854ce64c8','141.101.110.154',1448196677,'__ci_last_regenerate|i:1448196595;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";last_page|s:61:\"http://www.medyserv.com/sistema/accountant/invoice_add/create\";'),('6bb934d284e3f77ce35dae184506eb74aa2bc76a','181.63.98.12',1447895769,'__ci_last_regenerate|i:1447895692;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('6c942f163cad75d59fdb988d3b3654ee0d931d8a','::1',1449013694,'__ci_last_regenerate|i:1449013693;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('6dd9f1b3f3828a22d845c8fc5c2c546b83c42378','181.63.98.12',1448252923,'__ci_last_regenerate|i:1448252836;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('6fe6d72956df7b7a1ea2c8e0f197c14a021b7596','141.101.110.154',1448086383,'__ci_last_regenerate|i:1448086383;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('716bfafebdf66cb7b1b2d7d36250a9cdc4f4963f','190.60.205.104',1447871206,'__ci_last_regenerate|i:1447871206;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('72dfa6ccc3bfdfa5e1c4852f708fd39d97e2d11e','141.101.110.154',1448170894,'__ci_last_regenerate|i:1448170688;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('72e5d4d698afd01831b9ae3d365cd537f4cc2d05','190.60.205.104',1448290301,'__ci_last_regenerate|i:1448290301;'),('7868ed958ec8f23f5b211077e48a516bcf7d6c98','141.101.110.154',1448178391,'__ci_last_regenerate|i:1448178172;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('79770e272ced304b9d4707d30874569da0b96864','141.101.110.154',1448176834,'__ci_last_regenerate|i:1448176521;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('7a8936315896f67a533cfdd2dd374d51d9f2d5c1','141.101.110.154',1448184754,'__ci_last_regenerate|i:1448184474;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('7dd33b40e54f8b633ecc7bbb03d85e67a2b66b24','190.60.205.104',1447870600,'__ci_last_regenerate|i:1447870600;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('7f1b83a540b6eca0b98a167b432d8f0fd16d618c','190.60.205.104',1447869090,'__ci_last_regenerate|i:1447869090;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('8107e5f66fd827dc4a6522cfe49e2bab02e968b5','141.101.110.154',1448162809,'__ci_last_regenerate|i:1448162529;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('81c7839966d53c945e525ae10ef29fa83707f506','141.101.110.154',1448169660,'__ci_last_regenerate|i:1448169368;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('84019abfd8dbbacb021ee3cf7913f27776f847fe','181.63.98.12',1447890543,'__ci_last_regenerate|i:1447887462;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('867f54949f0c51bdd2edc3499e00ec822d1d2ae1','::1',1449013315,'__ci_last_regenerate|i:1449013305;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('8839b3cb9ae78afe45cf8fcb5361f3b2aa14951e','141.101.110.154',1448160743,'__ci_last_regenerate|i:1448160706;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('88711c204f6c0a4d28bac94f9a933e948ac7e404','141.101.110.154',1448188821,'__ci_last_regenerate|i:1448188533;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('8b2f326f8938f75e0060e25185e9f8697b56b9ec','190.60.205.104',1447861563,'__ci_last_regenerate|i:1447861272;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('8bdb8432a84ae63451e693d5887a57b834019e82','181.63.98.12',1447981556,'__ci_last_regenerate|i:1447981556;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('8bf4902553c68b74452e3e64655e188bc57bdd93','141.101.110.154',1448168001,'__ci_last_regenerate|i:1448167738;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('8c96e963e978cf7cee928b615c49780d3231f6d7','181.63.98.12',1447900622,'__ci_last_regenerate|i:1447900358;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:31:\"Notice Info Updated Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('8d11aed85df97402d966c15ece59093535137558','181.63.98.12',1447884011,'__ci_last_regenerate|i:1447884010;'),('8dbac0e00dcbbf2d21a2a3c653c359a594ca6a7a','141.101.110.154',1448177374,'__ci_last_regenerate|i:1448177183;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('91b6ce6ae03286b79e5a1b69b2bfa7e376f79176','141.101.110.154',1448186235,'__ci_last_regenerate|i:1448185935;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('91e417d9e564ac2474e37489b54f004faa962702','141.101.110.154',1448247835,'__ci_last_regenerate|i:1448247507;pharmacist_login|s:1:\"1\";login_user_id|s:1:\"2\";name|s:12:\"Martha Olsen\";login_type|s:10:\"pharmacist\";'),('9251fcfdc8b43aeeecaa01a9cc2f01c8de734dfe','141.101.110.154',1448163481,'__ci_last_regenerate|i:1448163187;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('9639dac380c8af8c02a99fac9ce3869b1a81ed15','141.101.110.154',1448168324,'__ci_last_regenerate|i:1448168066;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('96e186230bd729b3a9db85964eeabeea72733ea0','181.63.98.12',1447983013,'__ci_last_regenerate|i:1447982801;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('97b933a8dd3e27df6becbee8b1d1e07429a38fcb','::1',1449014638,'__ci_last_regenerate|i:1449014389;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";message|s:31:\"Record Info Deleted Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('99617eeff3906b2ea414bbba1b47f0992d8dcdf8','141.101.110.154',1448181449,'__ci_last_regenerate|i:1448181324;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('99c41f4d4c0318a837813afde3324efae28f2b0c','190.60.205.104',1447868788,'__ci_last_regenerate|i:1447868788;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('99c911a9ec1084dc2780df1d01cde48d6ac91a23','190.60.205.104',1447868615,'__ci_last_regenerate|i:1447868398;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('9b3002b67c5b4605eaa6c9bdbe8aaafea5be2899','190.60.205.104',1447867881,'__ci_last_regenerate|i:1447867880;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('9c930a64899d4b978e8f5c7aea5db4f9772741a8','181.63.98.12',1448137991,'__ci_last_regenerate|i:1448137987;last_page|s:45:\"http://medyserv.com/sistema/admin/invoice_add\";'),('9cbfc4bc5fe83d0ecd75e419d10c7d28f71899cb','181.63.98.12',1448002702,'__ci_last_regenerate|i:1448002702;'),('9d0be9607fa358e13190e4e3e55e17b1ba8ac708','190.60.205.104',1447861870,'__ci_last_regenerate|i:1447861595;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('9e29e031765ff7a59ad736fd25faac6b5632cbfe','190.60.205.104',1447870251,'__ci_last_regenerate|i:1447869995;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('a2187bb3fcff2458f79fc8e9c2e11288bbf2b26e','181.63.98.12',1447891910,'__ci_last_regenerate|i:1447891910;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('a322892b00a1bef93bc54373392c52a3c41b1e56','181.63.98.12',1447982082,'__ci_last_regenerate|i:1447981858;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('a3f2d52d5b3557a282d8c727b91ec68a7f863a63','141.101.110.154',1448160391,'__ci_last_regenerate|i:1448159526;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('a43b0dbb5c8bab937e3fd5af12c08fda42f845bb','141.101.110.154',1448173968,'__ci_last_regenerate|i:1448173680;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('a511f076d7d4caaee35b1939e8a4d40c37d53f35','141.101.110.154',1448247386,'__ci_last_regenerate|i:1448247101;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('a7b0d9a9509904ccf58034ce6d1f0cca87e2c75a','141.101.110.154',1448192541,'__ci_last_regenerate|i:1448192268;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('a7ea48d5e4b13c7ea3a1bdfcd1d2355c581bb1ed','141.101.110.154',1448172798,'__ci_last_regenerate|i:1448172666;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('ab16c9faa463859133d28fd9b1a4a94d511f09bc','141.101.110.154',1448185549,'__ci_last_regenerate|i:1448185321;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('b00f64163f3a705f99355c2aafa3994157196bd0','141.101.110.154',1448182655,'__ci_last_regenerate|i:1448182409;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('b082bcb0332578dbc41704502fabef03fe7fae1f','190.60.205.104',1447867544,'__ci_last_regenerate|i:1447867283;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('b1b42dda1f0ebcdb7af6574560d190220f5252b6','141.101.110.154',1448175377,'__ci_last_regenerate|i:1448175143;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('b26dceedc6a91dbf2271f81b04cc7fe9bfcf5be6','181.63.98.12',1448241276,'__ci_last_regenerate|i:1448241019;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('b3b191bcaea42f00bddbd4f74f62104cea727b04','181.63.98.12',1447981554,'__ci_last_regenerate|i:1447980915;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('b4502866195257bde8fff824306e549dc068c54b','141.101.110.154',1448167059,'__ci_last_regenerate|i:1448167059;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('b71168a68290e532024ec6f64a8b26937a8b6516','141.101.110.154',1448191898,'__ci_last_regenerate|i:1448191653;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('b944a1569db5d5211638171e423dc17ffe3a3c52','141.101.110.154',1448232409,'__ci_last_regenerate|i:1448232397;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('b98ee9fefe7be2f1e4d828cfa452114235114d9b','190.60.205.104',1447863580,'__ci_last_regenerate|i:1447863308;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('bb8257b74418acdf4fa72a8d642d8a6769ee3429','181.63.98.12',1447895073,'__ci_last_regenerate|i:1447894847;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('bbef8a6ff8a98c0e50aa0a10b2b0a8690eb310c9','141.101.110.154',1448170303,'__ci_last_regenerate|i:1448170041;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('bc87fb406ff0bad0748298daf1992700ce080312','141.101.110.154',1448184037,'__ci_last_regenerate|i:1448183804;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('bc9589573843746a6de70da3df18b5addee95433','181.63.98.12',1448250950,'__ci_last_regenerate|i:1448250950;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('bfb02da012b4661e21525a952c265ffe32a08761','190.60.205.104',1447869693,'__ci_last_regenerate|i:1447869693;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('bfd5a553af56918241065a7ffe6fd6da4742eb05','181.63.98.12',1448241828,'__ci_last_regenerate|i:1448241824;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('c046b206efbb572482764138a5062ffa36370cd8','181.63.98.12',1448251970,'__ci_last_regenerate|i:1448251670;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('c0aca7fcb23c9aea81aa3195809cc6d1bcfd08cb','190.60.205.104',1447867274,'__ci_last_regenerate|i:1447866976;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('c0bef973a386818da8b9228e3c417ea0785776b2','181.63.98.12',1447900275,'__ci_last_regenerate|i:1447899889;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('c1e9bb461d6fb666852c0848ddeb380d74cdddd5','141.101.110.154',1448164898,'__ci_last_regenerate|i:1448164626;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('c2cfaf7146d7101e8b0c51c49884daff46972386','181.63.98.12',1447980394,'__ci_last_regenerate|i:1447980268;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('c3ab95d33991878cae71b91925b9a8da2a049151','141.101.110.154',1448178126,'__ci_last_regenerate|i:1448177845;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('c3c814b1a3b981d9590c267be3743e851e37c37c','141.101.110.154',1448193127,'__ci_last_regenerate|i:1448192900;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('c424b8830e1c5bc83066fc15b56908a7979da0cb','141.101.110.154',1448187358,'__ci_last_regenerate|i:1448187101;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('c5f2b8f7fb5a9670e7cdc7ba0a6ad08d56cbe12e','181.63.98.12',1448242345,'__ci_last_regenerate|i:1448242320;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('cb87978eade6fd65792fec3a6387d37b53a3f8fd','141.101.110.154',1448185854,'__ci_last_regenerate|i:1448185630;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('cc3805495579b2323b7a215d06d039a2312aec5e','::1',1449014758,'__ci_last_regenerate|i:1449014717;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('cf32f60380354fd788b4e7292360a34268ee57bb','190.60.205.104',1448288489,'__ci_last_regenerate|i:1448286004;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('cf5d0d447b6b80fcd8cd91f4579322a0054bb773','141.101.110.154',1448168582,'__ci_last_regenerate|i:1448168388;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('d1ec28b54e0690f6cc83687770966fcf13d47dbd','::1',1449015959,'__ci_last_regenerate|i:1449015720;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";message|s:32:\"Registro Eliminado Correctamente\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('d28200b27e9a83205f380b5b827d2b28621c2db6','141.101.110.154',1448241018,'__ci_last_regenerate|i:1448240657;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('d45ff94b89812e57cd54ba034a17fef6aecafb36','190.60.205.104',1448298579,'__ci_last_regenerate|i:1448298579;'),('dabb51ff510573f405a7a64eb69e327162f1d5a2','141.101.110.154',1448184395,'__ci_last_regenerate|i:1448184165;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('db39c7130b9861926946a88fd80ea6c8de122f2f','190.60.205.104',1447869391,'__ci_last_regenerate|i:1447869391;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('dd7e9dfe38dc5815420b164ec766154df40bfbcc','141.101.110.154',1448170625,'__ci_last_regenerate|i:1448170368;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('df29c20e8988c4148e78979b55c38c0c03969e3f','141.101.110.154',1448165647,'__ci_last_regenerate|i:1448165449;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('dfd0944d181eb183be031da77f21281e9d0d57f6','190.60.205.104',1447872117,'__ci_last_regenerate|i:1447872117;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('e07467ce98c0c886aae1e09deb5fc67c70679ba7','141.101.110.154',1448179489,'__ci_last_regenerate|i:1448179473;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('e135af34655159a4671ba613977a728f8252fb5d','181.63.98.12',1448253864,'__ci_last_regenerate|i:1448253670;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";message|s:30:\"Patient Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('e190c613ece9a762e26a30f888ffb9c1edd949ee','141.101.110.154',1448168987,'__ci_last_regenerate|i:1448168716;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('e1dfdca7649f84375357236b1d302f110287e52b','141.101.110.154',1448160398,'__ci_last_regenerate|i:1448160394;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('e2138e265ca252eaa3638865f0416a46331a1e52','190.60.205.104',1447863840,'__ci_last_regenerate|i:1447863631;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('e28a422d91b2908377238ae94a093b6fc28b1ab4','190.60.205.104',1447869102,'__ci_last_regenerate|i:1447869102;'),('e2e0117dc9d2a53b734f774dcea04ae71837419b','141.101.110.154',1448180787,'__ci_last_regenerate|i:1448180538;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('e4968bbdbf6f0451893d31f110276c313ee3e021','181.63.98.12',1447899883,'__ci_last_regenerate|i:1447899584;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('e667708fcc720de3b52f47e72b079a2d347dad6f','181.63.98.12',1447982425,'__ci_last_regenerate|i:1447982387;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:32:\"Invoice Info Updated Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('e6ce61e0b88a0435a7234b18340bfa082d58c956','190.60.205.104',1447868601,'__ci_last_regenerate|i:1447868395;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('e9eb1ac819963ab55d8a38947a8480bdb562f27a','181.63.98.12',1448250752,'__ci_last_regenerate|i:1448250620;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('e9fd89ed248c7b54229a44bb9acb5187f1d4bd5b','141.101.110.154',1448198085,'__ci_last_regenerate|i:1448197806;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('ebd489caddcc0dc5531aeb327abc038f83c47633','141.101.110.154',1448193964,'__ci_last_regenerate|i:1448193921;nurse_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:13:\"Diane Rowling\";login_type|s:5:\"nurse\";'),('eca060fea358cf1c2e1501365c4b90505c8b7ab7','141.101.110.154',1448174151,'__ci_last_regenerate|i:1448173981;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('ed1c441aec30bf1d68ce453eb37ffe55aa48925b','181.63.98.12',1448254121,'__ci_last_regenerate|i:1448253971;admin_login|s:1:\"1\";login_user_id|s:1:\"5\";name|s:15:\"Sergio Carrillo\";login_type|s:5:\"admin\";'),('ee93f95292a6aa70c3334debf5ed7563a460cf43','141.101.110.154',1448171908,'__ci_last_regenerate|i:1448171813;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('f0661f8910c3fde9dcf64659ae9c019c31926eb4','141.101.110.154',1448174911,'__ci_last_regenerate|i:1448174629;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('f07d78a42b5464d6a79e133fa4f3f982f3b9341f','141.101.110.154',1448175737,'__ci_last_regenerate|i:1448175691;doctor_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:10:\"Jhon Smith\";login_type|s:6:\"doctor\";'),('f0bdd73b409da3cd95211ebe3d578df3cb7829d8','141.101.110.154',1448195824,'__ci_last_regenerate|i:1448195546;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('f13a1f8b49f675dd342cb25dc565bb9e33063740','141.101.110.154',1448171467,'__ci_last_regenerate|i:1448171466;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('f3e56e5cc62eb185a47c59c27c6fa49966a9abe7','141.101.110.154',1448180386,'__ci_last_regenerate|i:1448180193;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('f5e00634033a430599f205a684bd5958488b5fd7','190.60.205.104',1447862177,'__ci_last_regenerate|i:1447861925;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('f839ac72158942732c29e860f467580f7ab876db','181.63.98.12',1447985369,'__ci_last_regenerate|i:1447985348;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('fa51f19c60946a580b434a9a24bc4bae47aaf4ce','181.63.98.12',1447908296,'__ci_last_regenerate|i:1447908296;last_page|s:47:\"http://www.medyserv.com/sistema/admin/dashboard\";'),('fb2a3188aac9711fb4846a0f64cb0f47285c515c','181.63.98.12',1447895358,'__ci_last_regenerate|i:1447895150;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('fb7f7471edeadd9af8e36a958f6b46d74acb216d','141.101.110.154',1448193727,'__ci_last_regenerate|i:1448193571;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('fc9322eefd10e88ed2a2618a4b6f641ed5a2e9a2','141.101.110.154',1448163159,'__ci_last_regenerate|i:1448162871;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('fd179125ddb527ed591bdc51de2ba1ad6af86c14','190.25.90.191',1447867521,'__ci_last_regenerate|i:1447867295;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('fddca5ab2efe36c4414bfb6dc646ab1d245467ef','141.101.110.154',1448196561,'__ci_last_regenerate|i:1448196272;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('feadb5c86a4c3c3b08ba62f20810a52648b00d42','190.60.205.104',1447862647,'__ci_last_regenerate|i:1447862416;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";'),('fef3c4b5918295ebcb3542f08157610c2991a9cc','141.101.110.154',1448193453,'__ci_last_regenerate|i:1448193219;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";message|s:29:\"Record Info Saved Successfuly\";__ci_vars|a:1:{s:7:\"message\";s:3:\"old\";}'),('ff054c3d2f23257c77cfaa42b36855666a658679','181.63.98.12',1447910269,'__ci_last_regenerate|i:1447910069;admin_login|s:1:\"1\";login_user_id|s:1:\"1\";name|s:5:\"Admin\";login_type|s:5:\"admin\";');
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinical_record`
--

DROP TABLE IF EXISTS `clinical_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinical_record` (
  `clinical_record_id` int(11) NOT NULL AUTO_INCREMENT,
  `patient_id` int(11) NOT NULL,
  `admission_date` date NOT NULL,
  `diagnosis` longtext NOT NULL,
  `surgery` tinyint(1) DEFAULT NULL,
  `outpatient_procedure` longtext,
  `emergency_procedure_code` varchar(255) DEFAULT NULL,
  `orthopedist` int(11) DEFAULT NULL,
  `osteosynthesis_materials_requested` longtext,
  `hospitalization` tinyint(1) DEFAULT NULL,
  `hospitalization_date` date DEFAULT NULL,
  `bed` longtext,
  `medicine` longtext,
  `paraclinics` longtext,
  `cx_code` varchar(255) DEFAULT NULL,
  `surgical_procedure` longtext,
  `clinical_record_status_id` int(11) NOT NULL,
  `clinical_record_status_modifier` int(11) DEFAULT NULL,
  `clinical_record_status_modified` datetime DEFAULT NULL,
  `clinical_status_modifier_type` varchar(255) NOT NULL,
  `out_observations` longtext NOT NULL,
  `out_date` date NOT NULL,
  `created` datetime NOT NULL,
  `creator` int(11) NOT NULL,
  `creator_type` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  `modifier` int(11) NOT NULL,
  `modifier_type` varchar(255) NOT NULL,
  PRIMARY KEY (`clinical_record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinical_record`
--

LOCK TABLES `clinical_record` WRITE;
/*!40000 ALTER TABLE `clinical_record` DISABLE KEYS */;
INSERT INTO `clinical_record` VALUES (1,3,'2015-11-15','Luxación de codo',0,'Inmovilizar','365147 368745',1,'',0,'2011-07-15','201C','Naproxeno','Inmovilizador','','',3,1,'2015-11-16 23:09:24','admin','','2015-11-17','2015-11-12 21:36:18',1,'admin','2015-11-17 05:25:15',1,'admin'),(7,1,'2015-11-15','Alguno',1,'Aplicar unguento','215478',1,NULL,0,'2015-11-16','204B','','',NULL,NULL,3,1,'2015-11-17 05:21:52','admin','','2015-11-17','2015-11-16 23:49:39',1,'admin','2015-11-17 05:27:15',1,'admin'),(21,13,'2015-11-17','Mano dislocada',0,'Ajustar la mano','541232 587985 254785',3,NULL,0,'2015-11-17','105B','Ibuprofeno','Inmovilizador',NULL,NULL,1,NULL,NULL,'','','0000-00-00','2015-11-17 18:44:36',1,'admin','2015-11-22 01:32:35',1,'admin'),(22,4,'2015-11-17','Fiebre',0,'Tomar agua','325412 365874 369456',1,NULL,1,'2015-11-17','202A','Dolex','Ninguno',NULL,NULL,1,NULL,NULL,'','','0000-00-00','2015-11-17 19:52:35',1,'admin','2015-11-17 19:53:14',1,'admin'),(23,2,'2015-11-17','fractuta de pierna',1,'','',-1,NULL,0,'2015-11-17','401A','','',NULL,NULL,3,1,'2015-11-17 20:09:28','','','2015-11-17','2015-11-17 19:56:03',1,'doctor','2015-11-17 20:09:28',1,'admin'),(24,2,'2015-11-18','Fractura expuesta de cubito dorsal izquierdo',1,'Inmovilización','325421 178563',5,NULL,1,'2015-11-18','101C','Morfina','Inmovilizador',NULL,NULL,3,1,'2015-11-18 01:09:22','','','2015-11-18','2015-11-18 00:47:17',1,'admin','2015-11-18 01:09:22',1,'admin'),(26,14,'2015-11-18','Se fracturo un brazo',1,'Arregllarle el brazo','3254132 874569',3,NULL,1,'2015-11-18','107A','Ibuprofeno','Inmovilizador',NULL,NULL,2,1,'2015-11-18 11:20:49','','','2015-11-20','2015-11-18 11:00:16',1,'admin','2015-11-18 11:20:49',1,'admin'),(27,1,'2015-11-18','Dolor de cabeza',0,'','',-1,NULL,0,'2015-11-18','203B','','',NULL,NULL,1,NULL,NULL,'','','0000-00-00','2015-11-18 12:23:13',1,'admin','2015-11-18 12:23:35',1,'admin'),(28,0,'2015-11-18','Dolor de Garganta',0,'','',-1,NULL,0,'2015-11-18','301A','','',NULL,NULL,1,NULL,NULL,'','','0000-00-00','2015-11-18 12:25:43',1,'admin','2015-11-18 12:25:43',1,'admin'),(29,1,'2015-11-18','Dolor bajito',0,'','',-1,NULL,0,'2015-11-18','105C','','',NULL,NULL,1,NULL,NULL,'','','0000-00-00','2015-11-18 12:43:20',1,'doctor','2015-11-18 12:45:04',1,'doctor'),(30,1,'2015-11-18','Dolor de espalda',0,'','',-1,NULL,0,'2015-11-18','206A','','',NULL,NULL,1,NULL,NULL,'','','0000-00-00','2015-11-18 12:43:34',1,'doctor','2015-11-18 12:45:20',1,'doctor'),(31,12,'2015-11-11','Mano debil',1,'Ninguno','365214',2,NULL,1,'2015-11-11','-1','Dolex','Cuidado',NULL,NULL,2,1,'2015-11-22 01:34:01','','','2015-11-22','2015-11-20 01:55:30',1,'admin','2015-11-22 01:34:01',1,'admin'),(32,13,'2015-11-21','Calor',1,'Tomar agua','654232 254785',1,NULL,1,'2015-11-21','325B','H2O','Tomar dolex',NULL,NULL,2,1,'2015-11-22 07:16:40','','','2015-11-22','2015-11-21 23:17:15',1,'admin','2015-11-22 07:16:40',1,'doctor'),(33,2,'2015-11-22','Esta loca',0,'De psiquiatra','365214',3,NULL,1,'2015-11-22','333A','Xanax','Camisa de Fuerza',NULL,NULL,1,NULL,NULL,'','','0000-00-00','2015-11-22 07:26:17',1,'doctor','2015-11-22 07:26:17',1,'doctor'),(34,1,'2015-11-22','Resfriado chikunguña',1,'Aislar','658749 652369 987587',5,NULL,1,'2015-11-22','111B','Advil','Bajar fiebre',NULL,NULL,1,NULL,NULL,'','','0000-00-00','2015-11-22 07:29:34',1,'nurse','2015-11-22 07:29:34',1,'nurse'),(35,1,'2015-11-22','Resfriado chikunguña',1,'Aislar','658749 652369 987587',5,NULL,1,'2015-11-22','111B','Advil','Bajar fiebre',NULL,NULL,2,1,'2015-11-22 21:55:32','','','2015-11-22','2015-11-22 07:31:37',1,'nurse','2015-11-22 21:55:32',1,'admin'),(36,13,'2015-11-22','Fractura',0,'','',-1,NULL,0,'2015-11-22','','','',NULL,NULL,1,NULL,NULL,'','','0000-00-00','2015-11-22 20:30:33',1,'nurse','2015-11-22 20:30:33',1,'nurse');
/*!40000 ALTER TABLE `clinical_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinical_record_status`
--

DROP TABLE IF EXISTS `clinical_record_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinical_record_status` (
  `clinical_record_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `reference` varchar(255) NOT NULL,
  PRIMARY KEY (`clinical_record_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinical_record_status`
--

LOCK TABLES `clinical_record_status` WRITE;
/*!40000 ALTER TABLE `clinical_record_status` DISABLE KEYS */;
INSERT INTO `clinical_record_status` VALUES (1,'Abierta','opened'),(2,'Cerrada','closed'),(3,'Re-Abierta','re-opened');
/*!40000 ALTER TABLE `clinical_record_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `currency_symbol` longtext COLLATE utf8_unicode_ci NOT NULL,
  `currency_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (9,'Cirugía Maxilofacial',''),(10,'Artroscopia',''),(11,'Consulta',''),(12,'Consulta Externa',''),(6,'Ortopedia',''),(7,'Cirugía',''),(8,'Traumatología','');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destination`
--

DROP TABLE IF EXISTS `destination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destination` (
  `destination_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`destination_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destination`
--

LOCK TABLES `destination` WRITE;
/*!40000 ALTER TABLE `destination` DISABLE KEYS */;
INSERT INTO `destination` VALUES (1,'Hospitalización Ginecología'),(2,'Hospitalización Medicina Interna'),(3,'Hospitalización Pediatría'),(4,'Salida'),(5,'UCI'),(6,'Hospitalización Cirugía');
/*!40000 ALTER TABLE `destination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diagnosis_report`
--

DROP TABLE IF EXISTS `diagnosis_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diagnosis_report` (
  `diagnosis_report_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_type` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'xray,blood test',
  `document_type` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'text,photo',
  `file_name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `prescription_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(11) NOT NULL,
  `laboratorist_id` int(11) NOT NULL,
  PRIMARY KEY (`diagnosis_report_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diagnosis_report`
--

LOCK TABLES `diagnosis_report` WRITE;
/*!40000 ALTER TABLE `diagnosis_report` DISABLE KEYS */;
INSERT INTO `diagnosis_report` VALUES (3,'Rayos X','Imagen','',2,'',1445299500,0),(4,'Rayos X','Otro','',1,'',0,0);
/*!40000 ALTER TABLE `diagnosis_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `doctor_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  `profile` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`doctor_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,'Jhon Smith','doctor@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','5555555',6,''),(2,'Andrew Jhonson','andrew.jhonson@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','3333333',9,''),(3,'Susan Clarkson','susan.clarkson@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','4444444',10,''),(4,'Peter Carter','peter.carter@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','9999999',8,''),(5,'Linda White','linda.white@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','7777777',12,'');
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_template`
--

DROP TABLE IF EXISTS `email_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_template` (
  `email_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `task` longtext COLLATE utf8_unicode_ci NOT NULL,
  `subject` longtext COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `instruction` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`email_template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_template`
--

LOCK TABLES `email_template` WRITE;
/*!40000 ALTER TABLE `email_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form_element`
--

DROP TABLE IF EXISTS `form_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form_element` (
  `form_element_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `html` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`form_element_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form_element`
--

LOCK TABLES `form_element` WRITE;
/*!40000 ALTER TABLE `form_element` DISABLE KEYS */;
/*!40000 ALTER TABLE `form_element` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_number` longtext COLLATE utf8_unicode_ci NOT NULL,
  `patient_id` int(11) NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `invoice_entries` longtext COLLATE utf8_unicode_ci NOT NULL,
  `creation_timestamp` date NOT NULL,
  `due_timestamp` date NOT NULL,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'paid or unpaid',
  `vat_percentage` longtext COLLATE utf8_unicode_ci NOT NULL,
  `discount_amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (1,'20029',0,'Cirugía de Mano a Clare Thompson','[{\"description\":\"Cirug\\u00eda de Mano\",\"amount\":\"850000\"},{\"description\":\"Clavo para mano X 2\",\"amount\":\"75000\"},{\"description\":\"F\\u00e9rula\",\"amount\":\"40000\"}]','2015-11-22','2015-11-25','Seleccionar Estado','16','0'),(2,'75820',1,'Cirugía Maxilofacial','[{\"description\":\"Cirug\\u00eda Maxilofacial\",\"amount\":\"1250000\"},{\"description\":\"Sala de Recuperaci\\u00f3n - 1 d\\u00eda\",\"amount\":\"350000\"}]','0000-00-00','0000-00-00','No Pagada','16','0'),(3,'96604',14,'Cirugía de Mano','[{\"description\":\"Cirugia\",\"amount\":\"$150.000\"}]','2015-11-22','2015-11-27','paid','16%','0'),(4,'74395',3,'Hospitalización y Cirugía','[{\"description\":\"Hospitalizaci\\u00f3n\",\"amount\":\"$250.000\"},{\"description\":\"Cirug\\u00eda\",\"amount\":\"$750.000\"}]','2015-11-22','2015-11-26','unpaid','16%','0');
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laboratorist`
--

DROP TABLE IF EXISTS `laboratorist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laboratorist` (
  `laboratorist_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`laboratorist_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laboratorist`
--

LOCK TABLES `laboratorist` WRITE;
/*!40000 ALTER TABLE `laboratorist` DISABLE KEYS */;
INSERT INTO `laboratorist` VALUES (1,'Rita Tranny','laboratorista@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','2222222'),(2,'Samantha Brend','samantha.brend@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','6666666');
/*!40000 ALTER TABLE `laboratorist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `phrase_id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase` longtext COLLATE utf8_unicode_ci NOT NULL,
  `english` longtext COLLATE utf8_unicode_ci NOT NULL,
  `spanish` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`phrase_id`)
) ENGINE=MyISAM AUTO_INCREMENT=260 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'login','Login','Iniciar Sesión'),(2,'forgot_password','Forgot Password','¿Olvidó su Contraseña?'),(3,'reset_password','Reset Password','Restablecer Contraseña'),(4,'return_to_login_page','Return To Login Page','Volver al Inicio'),(5,'admin_dashboard','Admin Dashboard','Panel de Control de la Administración'),(6,'welcome','Welcome','Hola'),(7,'edit_profile','Edit Profile','Editar Perfil'),(8,'change_password','Change Password','Cambiar Contraseña'),(9,'dashboard','Dashboard','Panel de Control'),(10,'department','Department','Departamento'),(11,'doctor','Doctor','Doctor'),(12,'patient','Patient','Paciente'),(13,'nurse','Nurse','Enfermera'),(14,'pharmacist','Pharmacist','Farmacéuta'),(15,'laboratorist','Laboratorist','Laboratorista'),(16,'accountant','Accountant','Contabilidad'),(17,'monitor_hospital','Monitor Hospital','Reportes de Hospital'),(18,'payment_history','Payment History','Historial de Pagos'),(19,'bed_allotment','Bed Allotment','Asignación de Camas'),(20,'blood_bank','Blood Bank','Banco de Sangre'),(21,'blood_donor','Blood Donor','Donante de Sangre'),(22,'medicine','Medicine','Medicina e Insumos'),(23,'operation_report','Operation Report','Reporte de Operaciones'),(24,'birth_report','Birth Report','Informe de Nacimientos'),(25,'death_report','Death Report','Informe de Decesos'),(26,'settings','Settings','Configuración'),(27,'system_settings','System Settings','Configuración del Sistema'),(28,'language_settings','Language Settings','Configuración de Idioma'),(29,'account','Account','Cuenta'),(30,'payment','Payment','Pagos'),(31,'delete','Delete','Borrar'),(32,'cancel','Cancel','Cancelar'),(33,'add_department','Add Department','Añadir Departamento'),(34,'name','Name','Nombre'),(35,'description','Description','Descripción'),(36,'options','Options','Opciones'),(37,'add_doctor','Add Doctor','Añadir Doctor'),(38,'image','Image','Imagen'),(39,'email','Email','Correo Electrónico'),(40,'address','Address','Dirección'),(41,'phone','Phone','Teléfono'),(42,'profile','Profile','Perfil'),(43,'edit_department','Edit Department','Editar Departamento'),(44,'password','Password','Contraseña'),(45,'select_department','Select Department','Seleccionar Departamento'),(46,'edit_doctor','Edit Doctor','Editar Médico'),(47,'add_patient','Add Patient','Añadir Paciente'),(48,'sex','Sex','Sexo'),(49,'birth_date','Birth Date','Fecha de Nacimiento'),(50,'age','Age','Edad'),(51,'blood_group','Blood Group','Grupo Sanguíneo'),(52,'select_sex','Select Sex','Seleccionar sexo'),(53,'male','Male','Masculino'),(54,'female','Female','Femenino'),(55,'select_blood_group','Select Blood Group','Seleccionar Grupo Sanguíneo'),(56,'edit_patient','Edit Patient','Editar Paciente'),(57,'add_nurse','Add Nurse','Añadir Enfermera'),(58,'edit_nurse','Edit Nurse','Editar Enfermera'),(59,'add_pharmacist','Add Pharmacist','Añadir Farmacéuta'),(60,'edit_pharmacist','Edit Pharmacist','Editar Farmacéuta'),(61,'add_laboratorist','Add Laboratorist','Añadir Laboratorista'),(62,'edit_laboratorist','Edit Laboratorist','Editar de Laboratorista'),(63,'add_accountant','Add Accountant','Añadir Contador'),(64,'edit_accountant','Edit Accountant','Editar Contador'),(65,'invoice_number','Invoice Number','Número de Factura'),(66,'title','Title','Título'),(67,'creation_date','Creation Date','Fecha de Creación'),(68,'due_date','Due Date','Fecha de Vencimiento'),(69,'vat_percentage','Vat Percentage','Porcentaje de IVA'),(70,'discount_amount','Discount Amount','Cantidad de Descuento'),(71,'status','Status','Estado'),(72,'bed_number','Bed Number','Número de Cama'),(73,'bed_type','Bed Type','Tipo de Cama'),(74,'allotment_time','Allotment Time','Fecha de Asignación'),(75,'discharge_time','Discharge Time','Fecha de Alta'),(76,'issue_date','Issue Date','Fecha de Creación'),(77,'payment_to','Payment To','Pagar a'),(78,'bill_to','Bill To','Cobrar a'),(79,'invoice_entries','Invoice Entries','Detalles de Facturación'),(80,'entry','Entry','Item'),(81,'price','Price','Precio'),(82,'sub_total','Sub Total','Sub Total'),(83,'discount','Discount','Descuento'),(84,'grand_total','Grand Total','Gran Total'),(85,'date','Date','Fecha'),(86,'amount','Amount','Cantidad'),(87,'method','Method','Método'),(88,'last_donation_date','Last Donation Date','Última Fecha de Donación'),(89,'medicine_category','Medicine Category','Categoría de Medicinas e Insumos'),(90,'manufacturing_company','Manufacturing Company','Empresa de Fabricación'),(91,'system_name','System Name','Nombre del Sistema'),(92,'system_title','System Title','Título del Sistema'),(93,'paypal_email','Paypal Email','Correo Electrónico de PayPal'),(94,'currency','Currency','Moneda'),(95,'system_email','System Email','Correo Electrónico del Sistema'),(96,'buyer','Buyer','Comprador'),(97,'value_required','Value Required','Valor Obligatorio'),(98,'purchase_code','Purchase Code','Código de Compra'),(99,'language','Language','Idioma'),(100,'text_align','Text Align','Alineación del Texto'),(101,'save','Save','Guardar'),(102,'manage_language','Manage Language','Administrar Idioma'),(103,'phrase_list','Phrase List','Listado de Frases'),(104,'add_phrase','Add Phrase','Añadir Frase'),(105,'add_language','Add Language','Agregar idioma'),(106,'option','Option','Opción'),(107,'edit_phrase','Edit Phrase','Editar Frase'),(108,'delete_language','Delete Language','Eliminar Idioma'),(109,'phrase','Phrase','Frase'),(110,'update_phrase','Update Phrase','Actualizar Frase'),(111,'manage_profile','Manage Profile','Administrar Perfil'),(112,'update_profile','Update Profile','Actualizar Perfil'),(113,'current_password','Current Password','Contraseña Actual'),(114,'new_password','New Password','Nueva Contraseña'),(115,'confirm_new_password','Confirm New Password','Confirmar Nueva Contraseña'),(116,'update_password','Update Password','Actualizar Contraseña'),(117,'profile_info_updated_successfuly','Profile Info Updated Successfuly','Información del Perfil Actualizado Exitosamente'),(118,'doctor_dashboard','Doctor Dashboard','Panel de Control de Doctor'),(119,'appointment','Appointment','Citas'),(120,'prescription','Prescription','Prescripción'),(121,'report','Report','Reporte'),(122,'appointment_schedule','Appointment Schedule','Citas Programadas'),(123,'add_appointment','Add Appointment','Añadir Cita'),(124,'event_schedule','Event Schedule','Calendario de Eventos'),(125,'select_patient','Select Patient','Seleccionar Paciente'),(126,'edit_appointment','Edit Appointment','Editar Cita'),(127,'add_prescription','Add Prescription','Añadir Prescripción'),(128,'case_history','Case History','Historial del Caso'),(129,'medication','Medication','Medicación'),(130,'note','Note','Nota'),(131,'edit_prescription','Edit Prescription','Editar Prescription'),(132,'report_type','Report Type','Tipo de Reporte'),(133,'document_type','Document Type','Tipo de Documento'),(134,'download','Download','Descargar'),(135,'add_diagnosis_report','Add Diagnosis Report','Añadir Informe de Diagnóstico'),(136,'select_report_type','Select Report Type','Seleccionar Tipo de Reporte'),(137,'xray','Xray','Radiografía'),(138,'blood_test','Blood Test','Análisis de Sangre'),(139,'document','Document','Documento'),(140,'select_document_type','Select Document Type','Seleccionar Tipo de Documento'),(141,'doc','Doc','Documento'),(142,'pdf','PDF','PDF'),(143,'excel','Excel','Excel'),(144,'other','Other','Otro'),(145,'add_bed_allotment','Add Bed Allotment','Asignar Una Cama'),(146,'select_bed_number','Select Bed Number','Seleccionar Número de Cama'),(147,'edit_bed_allotment','Edit Bed Allotment','Editar Cama Asignada'),(148,'blood_donor_list','Blood Donor List','Listado de Donantes de Sangre'),(149,'blood_bank_status','Blood Bank Status','Estado del Banco de Sangre'),(150,'add_report','Add Report','Añadir Reporte'),(151,'operation','Operation','Operaciones'),(152,'birth','Birth','Nacimientos'),(153,'death','Death','Decesos'),(154,'type','Type','Tipo'),(155,'select_type','Select Type','Seleccionar Tipo'),(156,'edit_report','Edit Report','Editar Reporte'),(157,'old_password','Old Password','Contraseña Anterior'),(158,'patient_dashboard','Patient Dashboard','Panel de Control de Paciente'),(159,'admit_history','Admit History','Historial de Ingresos'),(160,'operation_history','Operation History','Historial de Operaciones'),(161,'invoice','Invoice','Factura'),(162,'nurse_dashboard','Nurse Dashboard','Panel de Control de Enfermera'),(163,'bed_/_ward','Bed / Ward','Camas / Salas'),(164,'manage_bed','Manage Bed','Administrar Camas'),(165,'manage_blood_bank','Manage Blood Bank','Administrar Banco de Sangre'),(166,'bed','Bed','Cama'),(167,'add_bed','Add Bed','Añadir Cama'),(168,'ward','Ward','Sala de Recuperación'),(169,'cabin','Cabin','Encubadora'),(170,'icu','Icu','UCI'),(171,'edit_bed','Edit Bed','Editar Cama'),(172,'edit_blood_bank','Edit Blood Bank','Editar Banco de Sangre'),(173,'add_blood_donor','Add Blood Donor','Añadir Donante de Sangre'),(174,'edit_blood_donor','Edit Blood Donor','Editar Donante de Sangre'),(175,'select_doctor','Select Doctor','Seleccionar Doctor'),(176,'pharmacist_dashboard','Pharmacist Dashboard','Panel de Control de Farmacéuta'),(177,'add_medicine','Add Medicine','Añadir Insumo'),(178,'select_medicine_category','Select Medicine Category','Seleccionar Categoría de Insumo'),(179,'select_status','Select Status','Seleccionar Estado'),(180,'available','Available','Disponible'),(181,'unavailable','Unavailable','No Disponible'),(182,'edit_medicine','Edit Medicine','Editar Insumo'),(183,'add_medicine_category','Add Medicine Category','Añadir Categoría de Insumo'),(184,'edit_medicine_category','Edit Medicine Category','Editar Categoría de Insumo'),(185,'laboratorist_dashboard','Laboratorist Dashboard','Panel de Control de Laboratorista'),(186,'accountant_dashboard','Accountant Dashboard','Panel de Control de Contador'),(187,'add_invoice','Add Invoice','Añadir Factura'),(188,'manage_invoice','Manage Invoice','Administrar Facturas'),(189,'invoice_title','Invoice Title','Título de la Factura'),(190,'select_a_patient','Select A Patient','Seleccionar Paciente'),(191,'payment_status','Payment Status','Estado del Pago'),(192,'select_a_status','Select A Status','Seleccionar Estado'),(193,'paid','Paid','Pagado'),(194,'unpaid','Unpaid','No Pagado'),(195,'invoice_entry','Invoice Entry','Items de Facturación'),(196,'add_invoice_entry','Add Invoice Entry','Añadir Item de Facturación'),(197,'create_new_invoice','Create New Invoice','Crear Nueva Factura'),(198,'edit_invoice','Edit Invoice','Editar Factura'),(199,'update_invoice','Update Invoice','Actualizar Factura'),(200,'clinical_record','Clinical Record','Ficha Clínica'),(202,'add_record','Add Clinical Record','Crear Ficha Clínica'),(207,'requested_appointments','Requested Appointments','Solicitud de Citas'),(206,'appointment_list','Appointment List','Listado de Citas'),(223,'receptionist_dashboard','Receptionist Dashboard','Panel de Control de Recepcionista'),(208,'requested_appointment','Requested Appointment','Solicitud de Citas'),(213,'add_notice','Add Notice','Añadir Noticia'),(212,'noticeboard','Noticeboard','Tablero de Noticias'),(214,'start_date','Start Date','Fecha de Inicio'),(215,'end_date','End Date','Fecha Final'),(216,'view_all_messages','View All Messages','Ver Todos los Mensajes'),(219,'pending_appointment','Pending Appointment','Solicitudes Pendientes'),(218,'receptionist','Receptionist','Recepcionista'),(220,'pending_appointments','Pending Appointments','Solicitudes Pendientes'),(221,'apply_for_appointment','Apply For Appointment','Solicitar Una Cita'),(222,'message','Message','Mensajes'),(224,'filter_appointments','Filter Appointments','Filtrar Citas'),(226,'number_id','Number ID','Documento de Identidad'),(227,'payer','Payer','Pagador'),(228,'edit','Edit','Editar'),(229,'view_medication_history','View Medication History','Ver Historial Médico'),(230,'search','Search','Buscar'),(231,'patient_search','Search Patient','Buscar Paciente'),(232,'record_patient_identification','Identification Number','Documento Identidad'),(233,'new_clinical_record','New Clinical Record','Nueva Ficha Clínica'),(234,'edit_clinical_record','Edit Clinical Record','Modificar Ficha Clínica'),(235,'payer','Payer','Pagador'),(236,'programming_surgery','Programming Surgery','Cirugías Programadas'),(237,'room','Room','Sala CX'),(238,'cx_code','CX Code','Código CX'),(239,'time','Time','Hora'),(240,'surgical_procedure','Surgical Procedure','Procedimiento Quirúrgico'),(241,'surgeon','Surgeon','Cirujano'),(242,'osteosynthesis_materials_requested','Osteosynthesis Materials Requested','Materiales de Osteosíntesis Solicitados'),(243,'destiny','Destiny','Destino'),(244,'destination','Destination','Destino'),(245,'report_without_giving_high','Without Giving High','Sin Dar De Alta'),(246,'admission_date','Admission Date','Fecha de Ingreso'),(247,'surgery','Surgery','Cirugía'),(248,'diagnosis','Diagnosis','Diagnóstico'),(249,'surgery_count','Surgery Count','Cantidad de Cirugías'),(250,'report_ranking_surgeries','Ranking Surgeries','Ranking De Cirugías'),(251,'adverse_events','Adverse Events','Eventos Adversos'),(252,'no_results_found','No Results Found','No Se Han Encontrado Resultados'),(253,'observations','Observations','Observaciones'),(254,'paraclinics','Paraclinics','Paraclinicos'),(255,'out_date','Out date','Fecha de Egreso'),(256,'census_orthopedics','Census Orthopedics','Censo de Ortopedia'),(257,'cancelations','Cancelations','Cancelaciones'),(258,'cancelation_reason','Cancelation Reason','Razón de Cancelación'),(259,'record_info_deleted_successfuly','Record Deleted Successfuly','Registro Eliminado Correctamente');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicine`
--

DROP TABLE IF EXISTS `medicine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicine` (
  `medicine_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `medicine_category_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `price` longtext COLLATE utf8_unicode_ci NOT NULL,
  `manufacturing_company` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`medicine_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine`
--

LOCK TABLES `medicine` WRITE;
/*!40000 ALTER TABLE `medicine` DISABLE KEYS */;
INSERT INTO `medicine` VALUES (1,'Acetaminofén 800 Mg',1,'Tableta X 10','$ 5.000','MK','Disponible'),(2,'Ibuprofeno 500 Mg',1,'Tableta X 10','$ 2.000','MK','Disponible'),(3,'Diclofenáco 50 Mg',1,'Tableta X 10','$ 1.500','Genfar','No Disponible'),(4,'Placa de Platino',4,'Placa en platino para fracturas de femur','$ 250.000','Empresa X','Disponible'),(5,'Clavos',4,'Clavos 5/8 para fracturas de brazo','$ 70.000','Empresa X','No Disponible'),(6,'Inmovilizador Cuello',5,'Inmovilizador para cuellos','$ 55.000','Empresa X','Disponible');
/*!40000 ALTER TABLE `medicine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicine_category`
--

DROP TABLE IF EXISTS `medicine_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicine_category` (
  `medicine_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`medicine_category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine_category`
--

LOCK TABLES `medicine_category` WRITE;
/*!40000 ALTER TABLE `medicine_category` DISABLE KEYS */;
INSERT INTO `medicine_category` VALUES (1,'Medicina',''),(4,'Material de Osteosíntesis',''),(5,'Paraclínicos ','');
/*!40000 ALTER TABLE `medicine_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_thread_code` longtext NOT NULL,
  `message` longtext NOT NULL,
  `sender` longtext NOT NULL,
  `timestamp` longtext NOT NULL,
  `read_status` int(11) NOT NULL DEFAULT '0' COMMENT '0 unread 1 read',
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
INSERT INTO `message` VALUES (1,'841d52e02083f0c','','patient-1','1442199457',0),(2,'4d728df583e8869','','doctor-1','1442202138',0),(3,'841d52e02083f0c','Test','patient-1','1446438964',0);
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message_thread`
--

DROP TABLE IF EXISTS `message_thread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_thread` (
  `message_thread_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_thread_code` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sender` longtext COLLATE utf8_unicode_ci NOT NULL,
  `reciever` longtext COLLATE utf8_unicode_ci NOT NULL,
  `last_message_timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`message_thread_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message_thread`
--

LOCK TABLES `message_thread` WRITE;
/*!40000 ALTER TABLE `message_thread` DISABLE KEYS */;
INSERT INTO `message_thread` VALUES (1,'841d52e02083f0c','patient-1','',''),(2,'4d728df583e8869','doctor-1','','');
/*!40000 ALTER TABLE `message_thread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_type` longtext COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `color` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp_create` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp_last_update` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note`
--

LOCK TABLES `note` WRITE;
/*!40000 ALTER TABLE `note` DISABLE KEYS */;
/*!40000 ALTER TABLE `note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notice` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `start_timestamp` date NOT NULL,
  `end_timestamp` date NOT NULL,
  PRIMARY KEY (`notice_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice`
--

LOCK TABLES `notice` WRITE;
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
INSERT INTO `notice` VALUES (1,'Reunión de Cirujanos','Reunión de cirujanos para programar fechas','2015-11-22','2015-11-23'),(2,'Visita de auditor','Revista para preparar visita de auditor','2015-11-25','2015-11-27'),(3,'Reunión de Personal','Todo el personal deberemos reunirmos para debatir el tema X','2015-11-19','2015-11-20'),(5,'Evento de Prueba','Descripción de evento','2015-11-19','2015-11-20');
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticeboard`
--

DROP TABLE IF EXISTS `noticeboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticeboard` (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT,
  `notice_title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `notice` longtext COLLATE utf8_unicode_ci NOT NULL,
  `create_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`notice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticeboard`
--

LOCK TABLES `noticeboard` WRITE;
/*!40000 ALTER TABLE `noticeboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `noticeboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nurse`
--

DROP TABLE IF EXISTS `nurse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nurse` (
  `nurse_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`nurse_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nurse`
--

LOCK TABLES `nurse` WRITE;
/*!40000 ALTER TABLE `nurse` DISABLE KEYS */;
INSERT INTO `nurse` VALUES (1,'Diane Rowling','enfermera@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','8888888'),(2,'Amy Carlton','amy.carlton@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','5555555');
/*!40000 ALTER TABLE `nurse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `patient_id` int(11) NOT NULL AUTO_INCREMENT,
  `number_id` longtext COLLATE utf8_unicode_ci NOT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext COLLATE utf8_unicode_ci,
  `password` longtext COLLATE utf8_unicode_ci,
  `address` longtext COLLATE utf8_unicode_ci,
  `phone` longtext COLLATE utf8_unicode_ci,
  `sex` longtext COLLATE utf8_unicode_ci,
  `payer` longtext COLLATE utf8_unicode_ci,
  `age` int(11) DEFAULT NULL,
  `blood_group` longtext COLLATE utf8_unicode_ci,
  `account_opening_timestamp` int(11) NOT NULL,
  PRIMARY KEY (`patient_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'15444666','Paul Williams','paciente@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','7777777','Masculino','Nueva EPS',36,'O+',0),(2,'13888999','Clare Thompson','clare.thompson@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','4444444','Femenino','Caprecom',33,'A+',0),(3,'1129523274','Hansel Ramos','hansell.ramos@gmail.com','f40886f8fdeab0149a6defd01b3ace5aa44a9ba5','Calle 5','3107707149','Masculino','Salud Total E.P.S.',26,'A+',0),(4,'1129536563','Mailen Montenegro','ingmaymontenegro@gmail.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','3002001464','Femenino','Salud Total',26,'O+',0),(12,'11436324','Norelkys Ramos Mesa','norelkys@outlook.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','3812130','Femenino','Salud Total',38,'A+',0),(13,'16934777','Camilo Luna','info@medyserv.com','88fc9002d298df125f1acd3a0765dffe82c47fe2','Calle 5','5555555','Masculino','Coomeva',34,'O+',0),(14,'9555666','Pedro Perez','pepe@email.com','30337e2ca09c3ac93fc9b5458aa752922ce8e20d','','','Masculino','Saud Total',50,'O+',0);
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'income expense',
  `amount` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` longtext COLLATE utf8_unicode_ci NOT NULL,
  `invoice_number` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pharmacist`
--

DROP TABLE IF EXISTS `pharmacist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pharmacist` (
  `pharmacist_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pharmacist_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pharmacist`
--

LOCK TABLES `pharmacist` WRITE;
/*!40000 ALTER TABLE `pharmacist` DISABLE KEYS */;
INSERT INTO `pharmacist` VALUES (1,'Bruce Wegner','bruce.wegner@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','4444444'),(2,'Martha Olsen','farmaceuta@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','3333333');
/*!40000 ALTER TABLE `pharmacist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescription` (
  `prescription_id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `case_history` longtext COLLATE utf8_unicode_ci NOT NULL,
  `medication` longtext COLLATE utf8_unicode_ci NOT NULL,
  `note` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`prescription_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescription`
--

LOCK TABLES `prescription` WRITE;
/*!40000 ALTER TABLE `prescription` DISABLE KEYS */;
INSERT INTO `prescription` VALUES (1,'0',1,2,'Historial del caso','Tomar acetaminofen 500 mg.',''),(2,'1442322000',1,1,'Paciente con fiebre','Ibuprofeno 80 mg. una cada 8 horas','');
/*!40000 ALTER TABLE `prescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receptionist`
--

DROP TABLE IF EXISTS `receptionist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receptionist` (
  `receptionist_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`receptionist_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receptionist`
--

LOCK TABLES `receptionist` WRITE;
/*!40000 ALTER TABLE `receptionist` DISABLE KEYS */;
INSERT INTO `receptionist` VALUES (1,'Katy Obriem','recepcionista@email.com','7c4a8d09ca3762af61e59520943dc26494f8941b','Calle 5','4444444');
/*!40000 ALTER TABLE `receptionist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT 'operation,birth,death',
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` longtext COLLATE utf8_unicode_ci NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
INSERT INTO `report` VALUES (1,'operation','Cirugía de mano izquierda','1442552400',1,2),(2,'operation','Cirugía Maxilofacial','1442552400',1,1);
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `settings_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`settings_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'system_name','MedyServ S.A.S.'),(2,'system_title','MedyServ'),(3,'address','Avenida Ciudad de Cali No. 152-00'),(4,'phone','666 2111  //  Ext. 2101'),(5,'paypal_email','0'),(6,'currency','$'),(7,'system_email','info@medyserv.com'),(8,'buyer','FenixAgenciaWeb'),(9,'purchase_code','1dc44181-b236-402e-9cc6-fff238a28fa6'),(11,'language','spanish'),(12,'text_align','left-to-right'),(13,'system_currency_id','1'),(14,'clickatell_user','fenixagenciaweb'),(15,'clickatell_password','ZPYWMWRHWAKNYc'),(16,'clickatell_api_id','3562110');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surgery`
--

DROP TABLE IF EXISTS `surgery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surgery` (
  `surgery_id` int(11) NOT NULL AUTO_INCREMENT,
  `clinical_record_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `cx_code` longtext NOT NULL,
  `surgical_procedure` longtext NOT NULL,
  `osteosynthesis_materials_requested` longtext NOT NULL,
  `surgery_room_id` int(11) NOT NULL,
  `surgery_status_id` int(11) NOT NULL,
  `surgeon` int(11) NOT NULL,
  `surgeon_assistant` int(11) NOT NULL,
  `surgery_cancelation_reason_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  `surgery_performed_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `creator` int(11) NOT NULL,
  `creator_type` varchar(255) NOT NULL,
  `modified` datetime NOT NULL,
  `modifier` int(11) NOT NULL,
  `modifier_type` varchar(255) NOT NULL,
  PRIMARY KEY (`surgery_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surgery`
--

LOCK TABLES `surgery` WRITE;
/*!40000 ALTER TABLE `surgery` DISABLE KEYS */;
INSERT INTO `surgery` VALUES (1,1,'2015-11-14','18:08:00','597125 254712','Enderezar Codo','',1,2,3,4,4,6,0,'2015-11-14 18:11:50',1,'admin','2015-11-17 05:25:15',1,'admin'),(4,6,'2015-11-17','18:00:00','125687 214301','Arreglar fractura','Placa de platino',2,1,5,2,-1,5,0,'2015-11-16 20:22:56',1,'admin','2015-11-17 16:25:52',1,'admin'),(5,7,'2015-11-16','23:49:00','dzsdfsd','','',2,2,-1,-1,-1,-1,0,'2015-11-16 23:49:49',1,'admin','2015-11-17 00:01:44',1,'admin'),(6,7,'2015-11-17','00:00:00','','','',-1,1,3,2,-1,-1,0,'2015-11-17 00:00:26',1,'admin','2015-11-17 00:03:36',1,'admin'),(7,21,'2015-11-17','19:42:00','325412 854258','Arreglar mano','Tornillo',3,3,2,1,-1,6,0,'2015-11-17 19:44:26',1,'admin','2015-11-17 19:44:26',1,'admin'),(8,21,'2015-11-17','19:44:00','365784','Sobar la mano','Bisturi',3,1,4,5,5,5,0,'2015-11-17 19:48:27',1,'admin','2015-11-22 01:32:35',1,'admin'),(9,20,'2015-11-17','18:42:00','dzdzfdsf','','',1,-1,-1,-1,-1,-1,0,'2015-11-17 19:50:20',1,'admin','2015-11-17 19:50:20',1,'admin'),(10,21,'2015-11-18','19:48:00','698745 524785','Arreglar mano','CLavo',6,5,5,4,1,6,1,'2015-11-17 19:50:31',1,'admin','2015-11-22 00:27:21',1,'admin'),(11,20,'2015-11-17','19:50:00','otra','','',-1,-1,-1,-1,-1,-1,0,'2015-11-17 19:50:31',1,'admin','2015-11-17 19:50:31',1,'admin'),(12,22,'2015-11-17','19:52:00','365412 879658 512365','QUitar fiebre','Nada',1,3,4,1,-1,2,0,'2015-11-17 19:53:14',1,'admin','2015-11-17 19:53:14',1,'admin'),(13,23,'2015-11-17','19:56:00','368524 254785','sanarla','',3,3,3,2,-1,5,1,'2015-11-17 19:56:36',1,'doctor','2015-11-17 20:07:23',1,'admin'),(14,26,'2015-11-19','11:00:00','321452','Meterle el hueso','Clavos de platino',3,5,1,4,-1,6,0,'2015-11-18 11:08:53',1,'admin','2015-11-18 11:12:39',1,'admin'),(15,26,'2015-11-18','11:17:00','3524587','Metre otro hueso','Placa',4,2,3,-1,3,-1,0,'2015-11-18 11:19:40',1,'admin','2015-11-18 11:19:40',1,'admin'),(16,27,'2015-11-18','12:23:00','','','',1,-1,1,-1,-1,-1,0,'2015-11-18 12:23:35',1,'admin','2015-11-18 12:23:35',1,'admin'),(17,29,'2015-11-18','12:43:00','6678 ','hhjhjh','',1,-1,-1,-1,-1,-1,0,'2015-11-18 12:45:04',1,'doctor','2015-11-18 12:45:04',1,'doctor'),(18,30,'2015-11-18','12:43:00','.......','.....','',2,1,1,3,-1,6,1,'2015-11-18 12:45:20',1,'doctor','2015-11-18 12:45:20',1,'doctor'),(19,32,'2015-11-22','04:18:00','3254325','Operar','Clavos de platino',3,3,5,3,-1,5,0,'2015-11-22 04:20:28',1,'admin','2015-11-22 05:14:26',1,'admin');
/*!40000 ALTER TABLE `surgery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surgery_cancelation_reason`
--

DROP TABLE IF EXISTS `surgery_cancelation_reason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surgery_cancelation_reason` (
  `surgery_cancelation_reason_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `reference` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`surgery_cancelation_reason_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surgery_cancelation_reason`
--

LOCK TABLES `surgery_cancelation_reason` WRITE;
/*!40000 ALTER TABLE `surgery_cancelation_reason` DISABLE KEYS */;
INSERT INTO `surgery_cancelation_reason` VALUES (1,'Administrativos','admin'),(2,'Anestesia','anestesia'),(3,'Medicinas e Insumos','medicina'),(4,'Material Osteosíntesis','osteo'),(5,'Urgencias Vitales','vital');
/*!40000 ALTER TABLE `surgery_cancelation_reason` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surgery_room`
--

DROP TABLE IF EXISTS `surgery_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surgery_room` (
  `surgery_room_id` int(11) NOT NULL AUTO_INCREMENT,
  `surgery_room_number` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0=unalloted, 1=alloted',
  `type` longtext NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`surgery_room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surgery_room`
--

LOCK TABLES `surgery_room` WRITE;
/*!40000 ALTER TABLE `surgery_room` DISABLE KEYS */;
INSERT INTO `surgery_room` VALUES (1,'1',0,'',''),(2,'2',0,'',''),(3,'3',0,'',''),(4,'4',0,'',''),(5,'5',0,'',''),(6,'6',0,'','');
/*!40000 ALTER TABLE `surgery_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surgery_status`
--

DROP TABLE IF EXISTS `surgery_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surgery_status` (
  `surgery_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`surgery_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surgery_status`
--

LOCK TABLES `surgery_status` WRITE;
/*!40000 ALTER TABLE `surgery_status` DISABLE KEYS */;
INSERT INTO `surgery_status` VALUES (1,'Cirugía'),(2,'Cancelado'),(3,'Pre-Anestesia'),(4,'Recuperación'),(5,'En Espera');
/*!40000 ALTER TABLE `surgery_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id_type` int(11) NOT NULL AUTO_INCREMENT,
  `family` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (1,'boolean','Si','1',0),(2,'boolean','No','0',1);
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 19:29:06
