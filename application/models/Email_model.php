<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Email_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function account_opening_email($account_type = '', $email = '') {
        $system_name = $this->db->get_where('settings', array('type' => 'system_name'))->row()->description;

        $email_msg = "Bienvenido a " . $system_name . "<br />";
        $email_msg .= "Su tipo de cuenta es : " . $account_type . "<br />";
        $email_msg .= "Su contraseña es : " . $this->db->get_where($account_type, array('email' => $email))->row()->password . "<br />";
        $email_msg .= "Ingresar Aquí : " . base_url() . "<br />";

        $email_sub = "Cuenta email para ingreso al sistema";
        $email_to = $email;

        $this->do_email($email_msg, $email_sub, $email_to);
    }

    function password_reset_email($account_type = '', $email = '') {
        $query = $this->db->get_where($account_type, array('email' => $email));
        if ($query->num_rows() > 0) {
            $password = $query->row()->password;
            $email_msg = "Su tipo de cuenta es : " . $account_type . "<br />";
            $email_msg .= "Su contraseña es : " . $password . "<br />";

            $email_sub = "Solicitud para restablecer contraseña";
            $email_to = $email;
            $this->do_email($email_msg, $email_sub, $email_to);
            return true;
        } else {
            return false;
        }
    }

    /*     * *custom email sender*** */

    function do_email($msg = NULL, $sub = NULL, $to = NULL, $from = NULL) {

        $config = array();
        $config['useragent'] = "CodeIgniter";
        $config['mailpath'] = "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "mail.medyserv.com";
        $config['smtp_port'] = "25";
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = TRUE;

        $this->load->library('email');

        $this->email->initialize($config);

        $system_name = $this->db->get_where('settings', array('type' => 'system_name'))->row()->description;
        if ($from == NULL)
            $from = $this->db->get_where('settings', array('type' => 'system_email'))->row()->description;

        $this->email->from($from, $system_name);
        $this->email->from($from, $system_name);
        $this->email->to($to);
        $this->email->subject($sub);

        $msg = $msg . "<br /><br /><br /><br /><br /><br /><br /><hr /><center><a href=\"http://medyserv.com\">&copy; 2015 MedyServ</a></center>";
        $this->email->message($msg);

        $this->email->send();

        //echo $this->email->print_debugger();
    }

}