<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function clear_cache()
    {
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    function get_type_name_by_id($type, $type_id = '', $field = 'name')
    {
        $this->db->where($type . '_id', $type_id);
        $query = $this->db->get($type);
        $result = $query->result_array();
        foreach ($result as $row)
            return $row[$field];
        //return	$this->db->get_where($type,array($type.'_id'=>$type_id))->row()->$field;	
    }


    // Create a new invoice.
    function create_invoice()
    {
        $data['title'] = $this->input->post('title');
        $data['invoice_number'] = $this->input->post('invoice_number');
        $data['patient_id'] = $this->input->post('patient_id');
        $data['creation_timestamp'] = $this->input->post('creation_timestamp');
        $data['due_timestamp'] = $this->input->post('due_timestamp');
        $data['vat_percentage'] = $this->input->post('vat_percentage');
        $data['discount_amount'] = $this->input->post('discount_amount');
        $data['status'] = $this->input->post('status');

        $invoice_entries = array();
        $descriptions = $this->input->post('entry_description');
        $amounts = $this->input->post('entry_amount');
        $number_of_entries = sizeof($descriptions);

        for ($i = 0; $i < $number_of_entries; $i++) {
            if ($descriptions[$i] != "" && $amounts[$i] != "") {
                $new_entry = array('description' => $descriptions[$i], 'amount' => $amounts[$i]);
                array_push($invoice_entries, $new_entry);
            }
        }
        $data['invoice_entries'] = json_encode($invoice_entries);

        $this->db->insert('invoice', $data);
    }

    function select_invoice_info()
    {
        return $this->db->get('invoice')->result_array();
    }

    function select_invoice_info_by_patient_id()
    {
        $patient_id = $this->session->userdata('login_user_id');
        return $this->db->get_where('invoice', array('patient_id' => $patient_id))->result_array();
    }

    function update_invoice($invoice_id)
    {
        $data['title'] = $this->input->post('title');
        $data['invoice_number'] = $this->input->post('invoice_number');
        $data['patient_id'] = $this->input->post('patient_id');
        $data['creation_timestamp'] = $this->input->post('creation_timestamp');
        $data['due_timestamp'] = $this->input->post('due_timestamp');
        $data['vat_percentage'] = $this->input->post('vat_percentage');
        $data['discount_amount'] = $this->input->post('discount_amount');
        $data['status'] = $this->input->post('status');

        $invoice_entries = array();
        $descriptions = $this->input->post('entry_description');
        $amounts = $this->input->post('entry_amount');
        $number_of_entries = sizeof($descriptions);

        for ($i = 0; $i < $number_of_entries; $i++) {
            if ($descriptions[$i] != "" && $amounts[$i] != "") {
                $new_entry = array('description' => $descriptions[$i], 'amount' => $amounts[$i]);
                array_push($invoice_entries, $new_entry);
            }
        }
        $data['invoice_entries'] = json_encode($invoice_entries);

        $this->db->where('invoice_id', $invoice_id);
        $this->db->update('invoice', $data);
    }

    function delete_invoice($invoice_id)
    {
        $this->db->where('invoice_id', $invoice_id);
        $this->db->delete('invoice');
    }

    function calculate_invoice_total_amount($invoice_number)
    {
        $total_amount = 0;
        $invoice = $this->db->get_where('invoice', array('invoice_number' => $invoice_number))->result_array();
        foreach ($invoice as $row) {
            $invoice_entries = json_decode($row['invoice_entries']);
            foreach ($invoice_entries as $invoice_entry)
                $total_amount += $invoice_entry->amount;

            $vat_amount = $total_amount * $row['vat_percentage'] / 100;
            $grand_total = $total_amount + $vat_amount - $row['discount_amount'];
        }

        return $grand_total;
    }


    //////system settings//////
    function update_system_settings()
    {
        $data['description'] = $this->input->post('system_name');
        $this->db->where('type', 'system_name');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('system_title');
        $this->db->where('type', 'system_title');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('address');
        $this->db->where('type', 'address');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('phone');
        $this->db->where('type', 'phone');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('paypal_email');
        $this->db->where('type', 'paypal_email');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('currency');
        $this->db->where('type', 'currency');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('system_email');
        $this->db->where('type', 'system_email');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('buyer');
        $this->db->where('type', 'buyer');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('system_name');
        $this->db->where('type', 'system_name');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('purchase_code');
        $this->db->where('type', 'purchase_code');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('language');
        $this->db->where('type', 'language');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('text_align');
        $this->db->where('type', 'text_align');
        $this->db->update('settings', $data);
    }

    // SMS settings.
    function update_sms_settings()
    {

        $data['description'] = $this->input->post('clickatell_user');
        $this->db->where('type', 'clickatell_user');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('clickatell_password');
        $this->db->where('type', 'clickatell_password');
        $this->db->update('settings', $data);

        $data['description'] = $this->input->post('clickatell_api_id');
        $this->db->where('type', 'clickatell_api_id');
        $this->db->update('settings', $data);
    }

    /////creates log/////
    function create_log($data)
    {
        $data['timestamp'] = strtotime(date('Y-m-d') . ' ' . date('H:i:s'));
        $data['ip'] = $_SERVER["REMOTE_ADDR"];
        $location = new SimpleXMLElement(file_get_contents('http://freegeoip.net/xml/' . $_SERVER["REMOTE_ADDR"]));
        $data['location'] = $location->City . ' , ' . $location->CountryName;
        $this->db->insert('log', $data);
    }

    ////////BACKUP RESTORE/////////
    function create_backup($type)
    {
        $this->load->dbutil();


        $options = array(
            'format' => 'txt', // gzip, zip, txt
            'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
            'add_insert' => TRUE, // Whether to add INSERT data to backup file
            'newline' => "\n"               // Newline character used in backup file
        );


        if ($type == 'all') {
            $tables = array('');
            $file_name = 'system_backup';
        } else {
            $tables = array('tables' => array($type));
            $file_name = 'backup_' . $type;
        }

        $backup = &$this->dbutil->backup(array_merge($options, $tables));


        $this->load->helper('download');
        force_download($file_name . '.sql', $backup);
    }

    /////////RESTORE TOTAL DB/ DB TABLE FROM UPLOADED BACKUP SQL FILE//////////
    function restore_backup()
    {
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/backup.sql');
        $this->load->dbutil();


        $prefs = array(
            'filepath' => 'uploads/backup.sql',
            'delete_after_upload' => TRUE,
            'delimiter' => ';'
        );
        $restore = &$this->dbutil->restore($prefs);
        unlink($prefs['filepath']);
    }

    /////////DELETE DATA FROM TABLES///////////////
    function truncate($type)
    {
        if ($type == 'all') {
            $this->db->truncate('student');
            $this->db->truncate('mark');
            $this->db->truncate('teacher');
            $this->db->truncate('subject');
            $this->db->truncate('class');
            $this->db->truncate('exam');
            $this->db->truncate('grade');
        } else {
            $this->db->truncate($type);
        }
    }

    ////////IMAGE URL//////////
    function get_image_url($type = '', $id = '')
    {
        if (file_exists('uploads/' . $type . '_image/' . $id . '.jpg'))
            $image_url = base_url() . 'uploads/' . $type . '_image/' . $id . '.jpg';
        else
            $image_url = base_url() . 'uploads/user.jpg';

        return $image_url;
    }

    function save_department_info()
    {
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');

        $this->db->insert('department', $data);
    }

    function select_department_info()
    {
        return $this->db->get('department')->result_array();
    }

    function update_department_info($department_id)
    {
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');

        $this->db->where('department_id', $department_id);
        $this->db->update('department', $data);
    }

    function delete_department_info($department_id)
    {
        $this->db->where('department_id', $department_id);
        $this->db->delete('department');
    }

    function save_doctor_info()
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['password'] = sha1($this->input->post('password'));
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['department_id'] = $this->input->post('department_id');
        $data['profile'] = $this->input->post('profile');

        $this->db->insert('doctor', $data);

        $doctor_id = $this->db->insert_id();
        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/doctor_image/" . $doctor_id . '.jpg');
    }

    function select_doctor_info()
    {
        return $this->db->get('doctor')->result_array();
    }

    function select_doctor_info_by_id($doctor_id){
        return $this->db->get_where('doctor',array('doctor_id'=>$doctor_id))->row();
    }

    function update_doctor_info($doctor_id)
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['department_id'] = $this->input->post('department_id');
        $data['profile'] = $this->input->post('profile');

        $this->db->where('doctor_id', $doctor_id);
        $this->db->update('doctor', $data);

        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/doctor_image/" . $doctor_id . '.jpg');
    }

    function delete_doctor_info($doctor_id)
    {
        $this->db->where('doctor_id', $doctor_id);
        $this->db->delete('doctor');
    }

    function save_patient_info()
    {
        $data['name'] = $this->input->post('name');
        $data['number_id'] = $this->input->post('number_id');
        $data['email'] = $this->input->post('email');
        $data['password'] = sha1($this->input->post('password'));
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['sex'] = $this->input->post('sex');
        $data['payer'] = $this->input->post('payer');
        $data['age'] = $this->input->post('age');
        $data['blood_group'] = $this->input->post('blood_group');

        $this->db->insert('patient', $data);

        $patient_id = $this->db->insert_id();
        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/patient_image/" . $patient_id . '.jpg');
    }

    function select_patient_info()
    {
        return $this->db->get('patient')->result_array();
    }

    function select_patient_info_by_patient_id($patient_id = '')
    {
        return $this->db->get_where('patient', array('patient_id' => $patient_id))->result_array();
    }

    function select_patient_info_by_patient_number_id($patient_number_id = '')
    {
        return $this->db->get_where('patient', array('number_id' => $patient_number_id))->result_array();
    }

    function update_patient_info($patient_id)
    {
        $data['name'] = $this->input->post('name');
        $data['number_id'] = $this->input->post('number_id');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['sex'] = $this->input->post('sex');
        $data['payer'] = $this->input->post('payer');
        $data['age'] = $this->input->post('age');
        $data['blood_group'] = $this->input->post('blood_group');

        $this->db->where('patient_id', $patient_id);
        $this->db->update('patient', $data);

        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/patient_image/" . $patient_id . '.jpg');
    }

    function delete_patient_info($patient_id)
    {
        $this->db->where('patient_id', $patient_id);
        $this->db->delete('patient');
    }

    function save_nurse_info()
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['password'] = sha1($this->input->post('password'));
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->insert('nurse', $data);

        $nurse_id = $this->db->insert_id();
        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/nurse_image/" . $nurse_id . '.jpg');
    }

    function select_nurse_info()
    {
        return $this->db->get('nurse')->result_array();
    }

    function update_nurse_info($nurse_id)
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->where('nurse_id', $nurse_id);
        $this->db->update('nurse', $data);

        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/nurse_image/" . $nurse_id . '.jpg');
    }

    function delete_nurse_info($nurse_id)
    {
        $this->db->where('nurse_id', $nurse_id);
        $this->db->delete('nurse');
    }

    function save_pharmacist_info()
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['password'] = sha1($this->input->post('password'));
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->insert('pharmacist', $data);

        $pharmacist_id = $this->db->insert_id();
        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/pharmacist_image/" . $pharmacist_id . '.jpg');
    }

    function select_pharmacist_info()
    {
        return $this->db->get('pharmacist')->result_array();
    }

    function update_pharmacist_info($pharmacist_id)
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->where('pharmacist_id', $pharmacist_id);
        $this->db->update('pharmacist', $data);

        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/pharmacist_image/" . $pharmacist_id . '.jpg');
    }

    function delete_pharmacist_info($pharmacist_id)
    {
        $this->db->where('pharmacist_id', $pharmacist_id);
        $this->db->delete('pharmacist');
    }

    function save_laboratorist_info()
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['password'] = sha1($this->input->post('password'));
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->insert('laboratorist', $data);

        $laboratorist_id = $this->db->insert_id();
        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/laboratorist_image/" . $laboratorist_id . '.jpg');
    }

    function select_laboratorist_info()
    {
        return $this->db->get('laboratorist')->result_array();
    }

    function update_laboratorist_info($laboratorist_id)
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->where('laboratorist_id', $laboratorist_id);
        $this->db->update('laboratorist', $data);

        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/laboratorist_image/" . $laboratorist_id . '.jpg');
    }

    function delete_laboratorist_info($laboratorist_id)
    {
        $this->db->where('laboratorist_id', $laboratorist_id);
        $this->db->delete('laboratorist');
    }

    function save_accountant_info()
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['password'] = sha1($this->input->post('password'));
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->insert('accountant', $data);

        $accountant_id = $this->db->insert_id();
        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/accountant_image/" . $accountant_id . '.jpg');
    }

    function select_accountant_info()
    {
        return $this->db->get('accountant')->result_array();
    }

    function update_accountant_info($accountant_id)
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->where('accountant_id', $accountant_id);
        $this->db->update('accountant', $data);

        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/accountant_image/" . $accountant_id . '.jpg');
    }

    function delete_accountant_info($accountant_id)
    {
        $this->db->where('accountant_id', $accountant_id);
        $this->db->delete('accountant');
    }

    function save_receptionist_info()
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['password'] = sha1($this->input->post('password'));
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->insert('receptionist', $data);

        $receptionist_id = $this->db->insert_id();
        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/receptionist_image/" . $receptionist_id . '.jpg');
    }

    function select_receptionist_info()
    {
        return $this->db->get('receptionist')->result_array();
    }

    function update_receptionist_info($receptionist_id)
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');

        $this->db->where('receptionist_id', $receptionist_id);
        $this->db->update('receptionist', $data);

        move_uploaded_file($_FILES["image"]["tmp_name"], "uploads/receptionist_image/" . $receptionist_id . '.jpg');
    }

    function delete_receptionist_info($receptionist_id)
    {
        $this->db->where('receptionist_id', $receptionist_id);
        $this->db->delete('receptionist');
    }

    function save_bed_allotment_info()
    {
        $data['bed_id'] = $this->input->post('bed_id');
        $data['patient_id'] = $this->input->post('patient_id');
        $data['allotment_timestamp'] = strtotime($this->input->post('allotment_timestamp'));
        $data['discharge_timestamp'] = strtotime($this->input->post('discharge_timestamp'));

        $this->db->insert('bed_allotment', $data);
    }

    function select_bed_allotment_info()
    {
        return $this->db->get('bed_allotment')->result_array();
    }

    function update_bed_allotment_info($bed_allotment_id)
    {
        $data['bed_id'] = $this->input->post('bed_id');
        $data['patient_id'] = $this->input->post('patient_id');
        $data['allotment_timestamp'] = strtotime($this->input->post('allotment_timestamp'));
        $data['discharge_timestamp'] = strtotime($this->input->post('discharge_timestamp'));

        $this->db->where('bed_allotment_id', $bed_allotment_id);
        $this->db->update('bed_allotment', $data);
    }

    function delete_bed_allotment_info($bed_allotment_id)
    {
        $this->db->where('bed_allotment_id', $bed_allotment_id);
        $this->db->delete('bed_allotment');
    }

    function select_blood_bank_info()
    {
        return $this->db->get('blood_bank')->result_array();
    }

    function update_blood_bank_info($blood_group_id)
    {
        $data['status'] = $this->input->post('status');

        $this->db->where('blood_group_id', $blood_group_id);
        $this->db->update('blood_bank', $data);
    }

    function save_report_info()
    {
        $data['type'] = $this->input->post('type');
        $data['description'] = $this->input->post('description');
        $data['timestamp'] = strtotime($this->input->post('timestamp'));
        $data['patient_id'] = $this->input->post('patient_id');

        $login_type = $this->session->userdata('login_type');
        if ($login_type == 'nurse')
            $data['doctor_id'] = $this->input->post('doctor_id');
        else $data['doctor_id'] = $this->session->userdata('login_user_id');

        $this->db->insert('report', $data);
    }

    function select_report_info()
    {
        return $this->db->get('report')->result_array();
    }

    function update_report_info($report_id)
    {
        $data['type'] = $this->input->post('type');
        $data['description'] = $this->input->post('description');
        $data['timestamp'] = strtotime($this->input->post('timestamp'));
        $data['patient_id'] = $this->input->post('patient_id');

        $login_type = $this->session->userdata('login_type');
        if ($login_type == 'nurse')
            $data['doctor_id'] = $this->input->post('doctor_id');
        else $data['doctor_id'] = $this->session->userdata('login_user_id');

        $this->db->where('report_id', $report_id);
        $this->db->update('report', $data);
    }

    function delete_report_info($report_id)
    {
        $this->db->where('report_id', $report_id);
        $this->db->delete('report');
    }

    function save_bed_info()
    {
        $data['bed_number'] = $this->input->post('bed_number');
        $data['type'] = $this->input->post('type');
        $data['description'] = $this->input->post('description');

        $this->db->insert('bed', $data);
    }

    function select_bed_info()
    {
        return $this->db->get('bed')->result_array();
    }

    function update_bed_info($bed_id)
    {
        $data['bed_number'] = $this->input->post('bed_number');
        $data['type'] = $this->input->post('type');
        $data['description'] = $this->input->post('description');

        $this->db->where('bed_id', $bed_id);
        $this->db->update('bed', $data);
    }

    function delete_bed_info($bed_id)
    {
        $this->db->where('bed_id', $bed_id);
        $this->db->delete('bed');
    }

    function save_blood_donor_info()
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['sex'] = $this->input->post('sex');
        $data['age'] = $this->input->post('age');
        $data['blood_group'] = $this->input->post('blood_group');
        $data['last_donation_timestamp'] = strtotime($this->input->post('last_donation_timestamp'));

        $this->db->insert('blood_donor', $data);
    }

    function select_blood_donor_info()
    {
        return $this->db->get('blood_donor')->result_array();
    }

    function update_blood_donor_info($blood_donor_id)
    {
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['address'] = $this->input->post('address');
        $data['phone'] = $this->input->post('phone');
        $data['sex'] = $this->input->post('sex');
        $data['age'] = $this->input->post('age');
        $data['blood_group'] = $this->input->post('blood_group');
        $data['last_donation_timestamp'] = strtotime($this->input->post('last_donation_timestamp'));

        $this->db->where('blood_donor_id', $blood_donor_id);
        $this->db->update('blood_donor', $data);
    }

    function delete_blood_donor_info($blood_donor_id)
    {
        $this->db->where('blood_donor_id', $blood_donor_id);
        $this->db->delete('blood_donor');
    }

    function save_medicine_category_info()
    {
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');

        $this->db->insert('medicine_category', $data);
    }

    function select_medicine_category_info()
    {
        return $this->db->get('medicine_category')->result_array();
    }

    function update_medicine_category_info($medicine_category_id)
    {
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');

        $this->db->where('medicine_category_id', $medicine_category_id);
        $this->db->update('medicine_category', $data);
    }

    function delete_medicine_category_info($medicine_category_id)
    {
        $this->db->where('medicine_category_id', $medicine_category_id);
        $this->db->delete('medicine_category');
    }

    function save_medicine_info()
    {
        $data['name'] = $this->input->post('name');
        $data['medicine_category_id'] = $this->input->post('medicine_category_id');
        $data['description'] = $this->input->post('description');
        $data['price'] = $this->input->post('price');
        $data['manufacturing_company'] = $this->input->post('manufacturing_company');
        $data['status'] = $this->input->post('status');

        $this->db->insert('medicine', $data);
    }

    function select_medicine_info()
    {
        return $this->db->get('medicine')->result_array();
    }

    function update_medicine_info($medicine_id)
    {
        $data['name'] = $this->input->post('name');
        $data['medicine_category_id'] = $this->input->post('medicine_category_id');
        $data['description'] = $this->input->post('description');
        $data['price'] = $this->input->post('price');
        $data['manufacturing_company'] = $this->input->post('manufacturing_company');
        $data['status'] = $this->input->post('status');

        $this->db->where('medicine_id', $medicine_id);
        $this->db->update('medicine', $data);
    }

    function delete_medicine_info($medicine_id)
    {
        $this->db->where('medicine_id', $medicine_id);
        $this->db->delete('medicine');
    }

    function save_appointment_info()
    {
        $data['timestamp'] = strtotime($this->input->post('date_timestamp') . ' ' . $this->input->post('time_timestamp'));
        $data['status'] = 'approved';
        $data['patient_id'] = $this->input->post('patient_id');

        if ($this->session->userdata('login_type') == 'doctor')
            $data['doctor_id'] = $this->session->userdata('login_user_id');
        else
            $data['doctor_id'] = $this->input->post('doctor_id');

        $this->db->insert('appointment', $data);

        // Notify patient with sms.
        $notify = $this->input->post('notify');
        if ($notify != '') {
            $patient_name = $this->db->get_where('patient',
                array('patient_id' => $data['patient_id']))->row()->name;
            $doctor_name = $this->db->get_where('doctor',
                array('doctor_id' => $data['doctor_id']))->row()->name;
            $date = date('l, d F Y', $data['timestamp']);
            $time = date('g:i a', $data['timestamp']);
            $message = $patient_name . ', you have an appointment with doctor ' . $doctor_name . ' on ' . $date . ' at ' . $time . '.';
            $receiver_phone = $this->db->get_where('patient',
                array('patient_id' => $data['patient_id']))->row()->phone;

            $this->sms_model->send_sms($message, $receiver_phone);
        }
    }

    function save_requested_appointment_info()
    {
        $data['timestamp'] = strtotime($this->input->post('date_timestamp') . ' ' . $this->input->post('time_timestamp'));
        $data['doctor_id'] = $this->input->post('doctor_id');
        $data['patient_id'] = $this->session->userdata('login_user_id');
        $data['status'] = 'pending';

        $this->db->insert('appointment', $data);
    }

    function select_appointment_info_by_doctor_id()
    {
        $doctor_id = $this->session->userdata('login_user_id');
        return $this->db->get_where('appointment', array('doctor_id' => $doctor_id, 'status' => 'approved'))->result_array();
    }

    function select_appointment_info_by_patient_id()
    {
        $patient_id = $this->session->userdata('login_user_id');
        return $this->db->get_where('appointment', array('patient_id' => $patient_id, 'status' => 'approved'))->result_array();
    }

    function select_appointment_info($doctor_id = '', $start_timestamp = '', $end_timestamp = '')
    {
        $response = array();
        if ($doctor_id == 'all') {
            $this->db->order_by('doctor_id', 'asc');
            $this->db->order_by('timestamp', 'desc');
            $appointments = $this->db->get_where('appointment', array('status' => 'approved'))->result_array();
            foreach ($appointments as $row) {
                if ($row['timestamp'] >= $start_timestamp && $row['timestamp'] <= $end_timestamp)
                    array_push($response, $row);
            }
        } else {
            $this->db->order_by('timestamp', 'desc');
            $appointments = $this->db->get_where('appointment', array('doctor_id' => $doctor_id, 'status' => 'approved'))->result_array();
            foreach ($appointments as $row) {
                if ($row['timestamp'] >= $start_timestamp && $row['timestamp'] <= $end_timestamp)
                    array_push($response, $row);
            }
        }
        return $response;
    }

    function select_pending_appointment_info_by_patient_id()
    {
        $patient_id = $this->session->userdata('login_user_id');
        return $this->db->get_where('appointment', array('patient_id' => $patient_id, 'status' => 'pending'))->result_array();
    }

    function select_requested_appointment_info_by_doctor_id()
    {
        $doctor_id = $this->session->userdata('login_user_id');
        return $this->db->get_where('appointment', array('doctor_id' => $doctor_id, 'status' => 'pending'))->result_array();
    }

    function select_requested_appointment_info()
    {
        $this->db->order_by('doctor_id', 'asc');
        return $this->db->get_where('appointment', array('status' => 'pending'))->result_array();
    }

    function select_patient_info_by_doctor_id()
    {
        $doctor_id = $this->session->userdata('login_user_id');

        $this->db->group_by('patient_id');
        return $this->db->get_where('appointment', array('doctor_id' => $doctor_id, 'status' => 'approved'))->result_array();
    }

    function select_appointments_between_loggedin_patient_and_doctor()
    {
        $patient_id = $this->session->userdata('login_user_id');

        $this->db->group_by('doctor_id');
        return $this->db->get_where('appointment', array('patient_id' => $patient_id, 'status' => 'approved'))->result_array();
    }

    function update_appointment_info($appointment_id)
    {
        $data['timestamp'] = strtotime($this->input->post('date_timestamp') . ' ' . $this->input->post('time_timestamp'));
        $data['patient_id'] = $this->input->post('patient_id');

        $this->db->where('appointment_id', $appointment_id);
        $this->db->update('appointment', $data);

        // Notify patient with sms.
        $notify = $this->input->post('notify');
        if ($notify != '') {
            $doctor_id = $this->session->userdata('login_user_id');
            $patient_name = $this->db->get_where('patient',
                array('patient_id' => $data['patient_id']))->row()->name;
            $doctor_name = $this->db->get_where('doctor',
                array('doctor_id' => $doctor_id))->row()->name;
            $date = date('l, d F Y', $data['timestamp']);
            $time = date('g:i a', $data['timestamp']);
            $message = $patient_name . ', your appointment with doctor ' . $doctor_name . ' has been updated to ' . $date . ' at ' . $time . '.';
            $receiver_phone = $this->db->get_where('patient',
                array('patient_id' => $data['patient_id']))->row()->phone;

            $this->sms_model->send_sms($message, $receiver_phone);
        }
    }

    function approve_appointment_info($appointment_id)
    {
        $data['timestamp'] = strtotime($this->input->post('date_timestamp') . ' ' . $this->input->post('time_timestamp'));
        $data['status'] = 'approved';

        if ($this->session->userdata('login_type') == 'receptionist')
            $data['doctor_id'] = $this->input->post('doctor_id');

        $this->db->where('appointment_id', $appointment_id);
        $this->db->update('appointment', $data);

        // Notify patient with sms.
        $notify = $this->input->post('notify');
        if ($notify != '') {
            $doctor_id = $this->db->get_where('appointment',
                array('appointment_id' => $appointment_id))->row()->doctor_id;
            $patient_id = $this->db->get_where('appointment',
                array('appointment_id' => $appointment_id))->row()->patient_id;
            $patient_name = $this->db->get_where('patient',
                array('patient_id' => $patient_id))->row()->name;
            $doctor_name = $this->db->get_where('doctor',
                array('doctor_id' => $doctor_id))->row()->name;
            $date = date('l, d F Y', $data['timestamp']);
            $time = date('g:i a', $data['timestamp']);
            $message = $patient_name . ', your requested appointment with doctor ' . $doctor_name . ' on ' . $date . ' at ' . $time . ' has been approved.';
            $receiver_phone = $this->db->get_where('patient',
                array('patient_id' => $patient_id))->row()->phone;

            $this->sms_model->send_sms($message, $receiver_phone);
        }
    }

    function delete_appointment_info($appointment_id)
    {
        $this->db->where('appointment_id', $appointment_id);
        $this->db->delete('appointment');
    }

    function save_prescription_info()
    {
        $data['timestamp'] = strtotime($this->input->post('date_timestamp') . ' ' . $this->input->post('time_timestamp'));
        $data['patient_id'] = $this->input->post('patient_id');
        $data['case_history'] = $this->input->post('case_history');
        $data['medication'] = $this->input->post('medication');
        $data['note'] = $this->input->post('note');
        $data['doctor_id'] = $this->session->userdata('login_user_id');

        $this->db->insert('prescription', $data);
    }

    function select_prescription_info_by_doctor_id()
    {
        $doctor_id = $this->session->userdata('login_user_id');
        return $this->db->get_where('prescription', array('doctor_id' => $doctor_id))->result_array();
    }

    function select_medication_history($patient_id = '')
    {
        return $this->db->get_where('prescription', array('patient_id' => $patient_id))->result_array();
    }

    function select_prescription_info_by_patient_id()
    {
        $patient_id = $this->session->userdata('login_user_id');
        return $this->db->get_where('prescription', array('patient_id' => $patient_id))->result_array();
    }

    function update_prescription_info($prescription_id)
    {
        $data['timestamp'] = strtotime($this->input->post('date_timestamp') . ' ' . $this->input->post('time_timestamp'));
        $data['patient_id'] = $this->input->post('patient_id');
        $data['case_history'] = $this->input->post('case_history');
        $data['medication'] = $this->input->post('medication');
        $data['note'] = $this->input->post('note');
        $data['doctor_id'] = $this->session->userdata('login_user_id');

        $this->db->where('prescription_id', $prescription_id);
        $this->db->update('prescription', $data);
    }

    function delete_prescription_info($prescription_id)
    {
        $this->db->where('prescription_id', $prescription_id);
        $this->db->delete('prescription');
    }

    function save_diagnosis_report_info()
    {
        $data['timestamp'] = strtotime($this->input->post('date_timestamp') . ' ' . $this->input->post('time_timestamp'));
        $data['report_type'] = $this->input->post('report_type');
        $data['file_name'] = $_FILES["file_name"]["name"];
        $data['document_type'] = $this->input->post('document_type');
        $data['description'] = $this->input->post('description');
        $data['prescription_id'] = $this->input->post('prescription_id');

        $this->db->insert('diagnosis_report', $data);

        $diagnosis_report_id = $this->db->insert_id();
        move_uploaded_file($_FILES["file_name"]["tmp_name"], "uploads/diagnosis_report/" . $_FILES["file_name"]["name"]);
    }

    function select_diagnosis_report_info()
    {
        return $this->db->get('diagnosis_report')->result_array();
    }

    function delete_diagnosis_report_info($diagnosis_report_id)
    {
        $this->db->where('diagnosis_report_id', $diagnosis_report_id);
        $this->db->delete('diagnosis_report');
    }

    function save_notice_info()
    { 
        $data['title'] = $this->input->post('title');
        $data['description'] = $this->input->post('description');
        if ($this->input->post('start_timestamp') != '')
            $data['start_timestamp'] = ($this->input->post('start_timestamp'));
        else
            $data['start_timestamp'] = '';
        if ($this->input->post('end_timestamp') != '')
            $data['end_timestamp'] = ($this->input->post('end_timestamp'));
        else
            $data['end_timestamp'] = $data['start_timestamp'];

        $this->db->insert('notice', $data);
    }

    function select_notice_info()
    {
        return $this->db->get('notice')->result_array();
    }

    function update_notice_info($notice_id)
    { 
        $data['title'] = $this->input->post('title');
        $data['description'] = $this->input->post('description');
        if ($this->input->post('start_timestamp') != '')
            $data['start_timestamp'] = ($this->input->post('start_timestamp'));
        else
            $data['start_timestamp'] = '';
        if ($this->input->post('end_timestamp') != '')
            $data['end_timestamp'] = ($this->input->post('end_timestamp'));
        else
            $data['end_timestamp'] = $data['start_timestamp'];

        $this->db->where('notice_id', $notice_id);
        $this->db->update('notice', $data);
    }

    function delete_notice_info($notice_id)
    {
        $this->db->where('notice_id', $notice_id);
        $this->db->delete('notice');
    }

    ////////private message//////
    function send_new_private_message()
    {
        $message = $this->input->post('message');
        $timestamp = strtotime(date("Y-m-d H:i:s"));

        $reciever = $this->input->post('reciever');
        $sender = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');

        //check if the thread between those 2 users exists, if not create new thread
        $num1 = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->num_rows();
        $num2 = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->num_rows();

        if ($num1 == 0 && $num2 == 0) {
            $message_thread_code = substr(md5(rand(100000000, 20000000000)), 0, 15);
            $data_message_thread['message_thread_code'] = $message_thread_code;
            $data_message_thread['sender'] = $sender;
            $data_message_thread['reciever'] = $reciever;
            $this->db->insert('message_thread', $data_message_thread);
        }
        if ($num1 > 0)
            $message_thread_code = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->row()->message_thread_code;
        if ($num2 > 0)
            $message_thread_code = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->row()->message_thread_code;


        $data_message['message_thread_code'] = $message_thread_code;
        $data_message['message'] = $message;
        $data_message['sender'] = $sender;
        $data_message['timestamp'] = $timestamp;
        $this->db->insert('message', $data_message);

        return $message_thread_code;
    }

    function send_reply_message($message_thread_code)
    {
        $message = $this->input->post('message');
        $timestamp = strtotime(date("Y-m-d H:i:s"));
        $sender = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');


        $data_message['message_thread_code'] = $message_thread_code;
        $data_message['message'] = $message;
        $data_message['sender'] = $sender;
        $data_message['timestamp'] = $timestamp;
        $this->db->insert('message', $data_message);
    }

    function mark_thread_messages_read($message_thread_code)
    {
        // mark read only the oponnent messages of this thread, not currently logged in user's sent messages
        $current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
        $this->db->where('sender !=', $current_user);
        $this->db->where('message_thread_code', $message_thread_code);
        $this->db->update('message', array('read_status' => 1));
    }

    function count_unread_message_of_thread($message_thread_code)
    {
        $unread_message_counter = 0;
        $current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
        $messages = $this->db->get_where('message', array('message_thread_code' => $message_thread_code))->result_array();
        foreach ($messages as $row) {
            if ($row['sender'] != $current_user && $row['read_status'] == '0')
                $unread_message_counter++;
        }
        return $unread_message_counter;
    }

    function select_surgery_room_info()
    {
        return $this->db->get('surgery_room')->result_array();
    }

    function select_surgery_status_info()
    {
        return $this->db->get('surgery_status')->result_array();
    }

    function select_surgery_cancelation_reason_info()
    {
        return $this->db->get('surgery_cancelation_reason')->result_array();
    }

    function select_destination_info()
    {
        return $this->db->get('destination')->result_array();
    }

    function select_boolean_info()
    {
        $this->db->where('family', 'boolean');
        $this->db->order_by('default', 'DESC');
        return $this->db->get('type')->result_array();
    }

    function create_record()
    {

        $data['patient_id'] = $this->input->post('patient_id');
        $data['admission_date'] = $this->input->post('patient_admission_date');
        $data['diagnosis'] = $this->input->post('patient_diagnosis');
        $data['surgery'] = $this->input->post('patient_surgery');
        $data['outpatient_procedure'] = $this->input->post('patient_outpatient_procedure');
        $data['orthopedist'] = $this->input->post('patient_orthopedist');
        $data['emergency_procedure_code'] = $this->input->post('patient_emergency_procedure_code');
        $data['hospitalization'] = $this->input->post('patient_hospitalization');
        $data['clinical_record_status_id'] = $this->db->get_where('clinical_record_status',array('reference'=>'opened'))->row()->clinical_record_status_id;

        $data['hospitalization_date'] = $this->input->post('patient_hospitalization_date');
        $data['bed'] = $this->input->post('patient_hospitalization_bed');
        $data['medicine'] = $this->input->post('patient_medicine');
        $data['paraclinics'] = $this->input->post('patient_paraclinics');

        $data['created'] = (new DateTime())->format('Y-m-d H:i:s');
        $data['creator'] = $this->session->userdata('login_user_id');
        $data['creator_type'] = $this->session->userdata('login_type');
        $data['modified'] = (new DateTime())->format('Y-m-d H:i:s');
        $data['modifier'] = $this->session->userdata('login_user_id');
        $data['modifier_type'] = $this->session->userdata('login_type');

        $this->db->insert('clinical_record', $data);

        $data['clinical_record_id'] = $this->db->insert_id();

        return $data;
    }

    function select_clinical_record_info_by_id($clinical_record_id)
    {
        return $this->db->get_where('clinical_record', array('clinical_record_id' => $clinical_record_id))->result_array();
    }

    function select_adverse_events_by_clinical_record_id($clinical_record_id)
    {
        return $this->db->get_where('adverse_event', array('clinical_record_id' => $clinical_record_id))->result_array();
    }

    function select_clinical_record_status_by_reference($reference='opened'){
        return $this->db->get_where('clinical_record_status',array('reference'=>$reference))->row() ;
    }

    function select_surgeries_by_clinical_record_id($clinical_record_id){
        $this->db->select('s.surgery_id, s.date, s.time, s.cx_code, s.surgical_procedure, s.osteosynthesis_materials_requested,
        s.surgery_room_id, s.surgery_status_id, d.doctor_id surgeon_id, d.name surgeon, d.doctor_id surgeon_assistant_id,
        a.name surgeon_assistant, s.surgery_performed_id, s.surgery_cancelation_reason_id, ca.name surgery_cancelation_reason,
        s.destination_id, de.name destination, sr.surgery_room_id, sr.surgery_room_number surgery_room, ss.surgery_status_id,
        ss.name surgery_status
        ');
        $this->db->from('surgery s');
        $this->db->join('doctor d','d.doctor_id = s.surgeon','left');
        $this->db->join('doctor a','a.doctor_id = s.surgeon_assistant','left');
        $this->db->join('surgery_cancelation_reason ca','ca.surgery_cancelation_reason_id = s.surgery_cancelation_reason_id','left');
        $this->db->join('destination de','de.destination_id = s.destination_id','left');
        $this->db->join('surgery_room sr','sr.surgery_room_id = s.surgery_room_id','left');
        $this->db->join('surgery_status ss','ss.surgery_status_id = s.surgery_status_id','left');
        $this->db->where(array('s.clinical_record_id'=>$clinical_record_id));
        return $this->db->get()->result_array();
    }

    function update_clinical_record($clinical_record_id)
    {
        //emergency section
        if (null !== $this->input->post('patient_admission_date', null)) {
            $data['admission_date'] = $this->input->post('patient_admission_date');
        }
        if (null !== $this->input->post('patient_diagnosis', null)) {
            $data['diagnosis'] = $this->input->post('patient_diagnosis');
        }
        if (null !== $this->input->post('patient_surgery', null)) {
            $data['surgery'] = $this->input->post('patient_surgery');
        }
        if (null !== $this->input->post('patient_outpatient_procedure', null)) {
            $data['outpatient_procedure'] = $this->input->post('patient_outpatient_procedure');
        }
        if (null !== $this->input->post('patient_emergency_procedure_code', null)) {
            $data['emergency_procedure_code'] = $this->input->post('patient_emergency_procedure_code');
        }
        if (null !== $this->input->post('patient_orthopedist', null)) {
            $data['orthopedist'] = $this->input->post('patient_orthopedist');
        }
        if (null !== $this->input->post('patient_hospitalization', null)) {
            $data['hospitalization'] = $this->input->post('patient_hospitalization');
        }

        //hospitalization date
        if (null !== $this->input->post('patient_hospitalization_date', null)) {
            $data['hospitalization_date'] = $this->input->post('patient_hospitalization_date');
        }
        if (null !== $this->input->post('patient_hospitalization_bed', null)) {
            $data['bed'] = $this->input->post('patient_hospitalization_bed');
        }
        if (null !== $this->input->post('patient_medicine', null)) {
            $data['medicine'] = $this->input->post('patient_medicine');
        }
        if (null !== $this->input->post('patient_paraclinics', null)) {
            $data['paraclinics'] = $this->input->post('patient_paraclinics');
        }

        if (null !== $this->input->post('patient_add_surgery', null)) {
            //surgey section
            if (null !== $this->input->post('patient_cx_code', null)) {
                $data['cx_code'] = $this->input->post('patient_cx_code');
            }
            if (null !== $this->input->post('patient_surgical_procedure', null)) {
                $data['surgical_procedure'] = $this->input->post('patient_surgical_procedure');
            }
            if (null !== $this->input->post('patient_osteosynthesis_materials_requested', null)) {
                $data['osteosynthesis_materials_requested'] = $this->input->post('patient_osteosynthesis_materials_requested');
            }
            if (null !== $this->input->post('patient_cx_date', null)) {
                $data['date'] = $this->input->post('patient_cx_date');
            }
            if (null !== $this->input->post('patient_cx_time', null)) {
                $data['time'] = $this->input->post('patient_cx_time');
            }
            if (null !== $this->input->post('patient_cx_room', null)) {
                $data['surgery_room_id'] = $this->input->post('patient_cx_room');
            }
            if (null !== $this->input->post('patient_cx_status', null)) {
                $data['surgery_status_id'] = $this->input->post('patient_cx_status');
            }
            if (null !== $this->input->post('patient_surgeon', null)) {
                $data['surgeon'] = $this->input->post('patient_surgeon');
            }
            if (null !== $this->input->post('patient_surgeon_assistant', null)) {
                $data['surgeon_assistant'] = $this->input->post('patient_surgeon_assistant');
            }
            if (null !== $this->input->post('patient_cancelation_reason', null)) {
                $data['surgery_cancelation_reason_id'] = $this->input->post('patient_cancelation_reason');
            }
            if (null !== $this->input->post('patient_destination', null)) {
                $data['destination_id'] = $this->input->post('patient_destination');
            }
            if (null !== $this->input->post('patient_surgery_performed', null)) {
                $data['surgery_performed_id'] = $this->input->post('patient_surgery_performed');
            }

            if (null !== $this->input->post('patient_surgery_id', null)) {
                //insert a new surgey
                $data['modified'] = (new DateTime())->format('Y-m-d H:i:s');
                $data['modifier'] = $this->session->userdata('login_user_id');
                $data['modifier_type'] = $this->session->userdata('login_type');
                $this->db->where('surgery_id', $this->input->post('patient_surgery_id'));
                $this->db->update('surgery', $data);
                unset($data);
            } else {
                //update existent surgey
                $data['modified'] = (new DateTime())->format('Y-m-d H:i:s');
                $data['modifier'] = $this->session->userdata('login_user_id');
                $data['modifier_type'] = $this->session->userdata('login_type');
                $data['created'] = (new DateTime())->format('Y-m-d H:i:s');
                $data['creator'] = $this->session->userdata('login_user_id');
                $data['creator_type'] = $this->session->userdata('login_type');
                $data['clinical_record_id'] = $clinical_record_id;
                $this->db->insert('surgery', $data);
                $data['surgery_id'] = $this->db->insert_id();
                unset($data);
            }
        }


        if (null !== $this->input->post('patient_add_adverse_event', null)) {
            //adverse events section
            if (null !== $this->input->post('patient_adverse_event', null)) {
                $data['adverse_event'] = $this->input->post('patient_adverse_event');
            }
            if (null !== $this->input->post('patient_adverse_event_observations', null)) {
                $data['observations'] = $this->input->post('patient_adverse_event_observations');
            }
            if (null !== $this->input->post('patient_adverse_event_id', null)) {
                //update existent adverse event
                $data['modified'] = (new DateTime())->format('Y-m-d H:i:s');
                $data['modifier'] = $this->session->userdata('login_user_id');
                $data['modifier_type'] = $this->session->userdata('login_type');
                $this->db->where('adverse_event_id', $this->input->post('patient_adverse_event_id'));
                $this->db->update('adverse_event', $data);
                unset($data);
            } else {
                //insert a new adverse event
                $data['clinical_record_id'] = $this->input->post('clinical_record_id');
                $data['date'] = (new DateTime())->format('Y-m-d H:i:s');
                $data['modified'] = (new DateTime())->format('Y-m-d H:i:s');
                $data['modifier'] = $this->session->userdata('login_user_id');
                $data['modifier_type'] = $this->session->userdata('login_type');
                $data['created'] = (new DateTime())->format('Y-m-d H:i:s');
                $data['creator'] = $this->session->userdata('login_user_id');
                $data['creator_type'] = $this->session->userdata('login_type');
                $this->db->insert('adverse_event', $data);
                $data['adverse_event_id'] = $this->db->insert_id();
                unset($data);
            }
        }

        if (null !== $this->input->post('patient_out', null)) {
            $data['clinical_record_status_id'] = $this->db->get_where('clinical_record_status',array('reference'=>'closed'))->row()->clinical_record_status_id;
            $data['clinical_record_status_modified'] = (new DateTime())->format('Y-m-d H:i:s');
            $data['clinical_record_status_modifier'] = $this->session->userdata('login_user_id');
            if (null !== $this->input->post('patient_out_observations', null)) {
                $data['out_observations'] = $this->input->post('patient_out_observations');
            }
            if (null !== $this->input->post('patient_out_date', null)) {
                $data['out_date'] = $this->input->post('patient_out_date')." ".(new DateTime())->format('H:i:s');
            }

        }

        if (null !== $this->input->post('clinical_record_reopened', null)) {
            $data['clinical_record_status_id'] = $this->db->get_where('clinical_record_status',array('reference'=>'re-opened'))->row()->clinical_record_status_id;
            $data['clinical_record_status_modified'] = (new DateTime())->format('Y-m-d H:i:s');
            $data['clinical_record_status_modifier'] = $this->session->userdata('login_user_id');
        }


        $data['modified'] = (new DateTime())->format('Y-m-d H:i:s');
        $data['modifier'] = $this->session->userdata('login_user_id');
        $data['modifier_type'] = $this->session->userdata('login_type');

        $this->db->where('clinical_record_id', $clinical_record_id);
        $this->db->update('clinical_record', $data);

        $data['clinical_record_id'] = $clinical_record_id;
        return $data;
    }

    function search_clinical_record($search){
        $this->db->select('cr.patient_id clinical_record_patient_id, s.name clinical_record_status, cr. *, p.*');
        $this->db->from('patient p');
        $this->db->join('clinical_record cr','p.patient_id = cr.patient_id','left');
        $this->db->join('clinical_record_status s','s.clinical_record_status_id = cr.clinical_record_status_id');
        $this->db->where("p.number_id LIKE '$search%'");
        $this->db->or_where("p.name LIKE '$search%'");
        $r =  $this->db->get()->result_array();
        //var_dump($this->db->last_query());die;
        return $r;
    }

    function select_report_ranking_surgeries($from, $to)
    {
        $this->db->select('doctor.name, COUNT(surgery.surgery_id) surgery_count');
        $this->db->from('surgery');
        $this->db->join('doctor','doctor.doctor_id = surgery.surgeon');
        $this->db->where("surgery.date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        $this->db->group_by("doctor.name");
        return $this->db->get()->result_array();
    }

    function select_report_ranking_surgeries_count($from, $to)
    {
        $this->db->select('doctor.name, COUNT(surgery.surgery_id) surgery_count');
        $this->db->from('surgery');
        $this->db->join('doctor','doctor.doctor_id = surgery.surgeon');
        $this->db->where("surgery.date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        $this->db->group_by("doctor.name");
        return $this->db->count_all_results();
    }

    function select_report_census_orthopedics($from, $to)
    {
        $this->db->select('clinical_record.clinical_record_id, patient.name, patient.number_id, destination.name destiny,
        patient.age, patient.payer, clinical_record.bed, clinical_record.admission_date, clinical_record.diagnosis,
        clinical_record.medicine, clinical_record.paraclinics, clinical_record.admission_date, clinical_record.out_date,');
        $this->db->from('clinical_record');
		$this->db->join('surgery','clinical_record.clinical_record_id = surgery.clinical_record_id');
		$this->db->join('destination','destination.destination_id = surgery.destination_id');
        $this->db->join('patient','clinical_record.patient_id = patient.patient_id');
        $this->db->join('clinical_record_status','clinical_record.clinical_record_status_id = clinical_record_status.clinical_record_status_id');
        $this->db->where('clinical_record_status.reference <> "closed"');
		$this->db->where("clinical_record.admission_date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        return  $this->db->get()->result_array();
    }

    function select_report_census_orthopedics_count($from, $to)
    {
        $this->db->select('clinical_record.clinical_record_id, patient.name, patient.number_id, destination.name destiny,
        patient.age, patient.payer, clinical_record.bed, clinical_record.admission_date, clinical_record.diagnosis,
        clinical_record.medicine, clinical_record.paraclinics, clinical_record.admission_date, clinical_record.out_date,');
        $this->db->from('clinical_record');
		$this->db->join('surgery','clinical_record.clinical_record_id = surgery.clinical_record_id');
		$this->db->join('destination','destination.destination_id = surgery.destination_id');
        $this->db->join('patient','clinical_record.patient_id = patient.patient_id');
        $this->db->join('clinical_record_status','clinical_record.clinical_record_status_id = clinical_record_status.clinical_record_status_id');
        $this->db->where('clinical_record_status.reference','opened');
        $this->db->where("clinical_record.admission_date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        return $this->db->count_all_results();
    }
	
	function select_report_whitout_giving_high($from, $to)
    {
        $this->db->select('clinical_record.clinical_record_id,patient.number_id, patient.name,
        patient.age, patient.payer, clinical_record.bed, clinical_record.admission_date, clinical_record.diagnosis,
        clinical_record.medicine, IF(clinical_record.surgery=0,"NO","SI") surgery');
        $this->db->from('clinical_record');
        $this->db->join('patient','clinical_record.patient_id = patient.patient_id');
        $this->db->join('clinical_record_status','clinical_record.clinical_record_status_id = clinical_record_status.clinical_record_status_id');
        $this->db->where('clinical_record_status.reference <> "closed"');
		$this->db->where("clinical_record.admission_date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        return  $this->db->get()->result_array();
    }

    function select_report_whitout_giving_high_count($from, $to)
    {
        $this->db->select('clinical_record.clinical_record_id,patient.number_id, patient.name,
        patient.age, patient.payer, clinical_record.admission_date, clinical_record.diagnosis,
        clinical_record.medicine, IF(clinical_record.surgery=0,"NO","SI") surgery');
        $this->db->from('clinical_record');
        $this->db->join('patient','clinical_record.patient_id = patient.patient_id');
        $this->db->join('clinical_record_status','clinical_record.clinical_record_status_id = clinical_record_status.clinical_record_status_id');
        $this->db->where('clinical_record_status.reference','opened');
        $this->db->where("clinical_record.admission_date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        return $this->db->count_all_results();
    }

    function select_report_programming_surgery($from, $to){
        $this->db->select('s.date, s.time, s.surgery_room_id, sr.surgery_room_number room, p.name, p.number_id, p.age, s.cx_code,
        s.surgical_procedure, s.osteosynthesis_materials_requested, d.name surgeon');
        $this->db->from('clinical_record cr');
        $this->db->join('patient p','cr.patient_id = p.patient_id');
        $this->db->join('surgery s','cr.clinical_record_id = s.clinical_record_id');
        $this->db->join('surgery_status ss','ss.surgery_status_id = s.surgery_status_id','left');
        $this->db->join('surgery_room sr','sr.surgery_room_id = s.surgery_room_id','left');
        $this->db->join('doctor d','d.doctor_id = s.surgeon','left');
        $this->db->where("s.date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        $this->db->where("ss.name = 'En Espera'");
        return $this->db->get()->result_array();
    }

    function select_report_programming_surgery_count($from, $to){
        $this->db->select('s.date, s.time, s.surgery_room_id, sr.surgery_room_number room, p.name, p.number_id, p.age, s.cx_code,
        s.surgical_procedure, s.osteosynthesis_materials_requested, s.surgeon, s.surgeon_assistant');
        $this->db->from('clinical_record cr');
        $this->db->join('patient p','cr.patient_id = p.patient_id');
        $this->db->join('surgery s','cr.clinical_record_id = s.clinical_record_id');
        $this->db->join('surgery_status ss','ss.surgery_status_id = s.surgery_status_id','left');
        $this->db->join('surgery_room sr','sr.surgery_room_id = s.surgery_room_id','left');
        $this->db->join('doctor d','d.doctor_id = s.surgeon','left');
        $this->db->where("s.date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        $this->db->where("ss.name = 'En Espera'");
        return $this->db->count_all_results();
    }
    
	function select_report_room($from){
        $this->db->select('s.surgery_room_id, sr.surgery_room_number room, p.name, p.number_id, d.name surgeon,
        ss.name status, de.name destiny, s.date');
        $this->db->from('clinical_record cr');
        $this->db->join('patient p','cr.patient_id = p.patient_id');
        $this->db->join('surgery s','cr.clinical_record_id = s.clinical_record_id');
        $this->db->join('surgery_status ss','ss.surgery_status_id = s.surgery_status_id');
        $this->db->join('surgery_room sr','sr.surgery_room_id = s.surgery_room_id');
        $this->db->join('doctor d','d.doctor_id = s.surgeon');
        $this->db->join('destination de','de.destination_id = s.destination_id');
        $this->db->where("s.date BETWEEN '$from 00:00:00' AND '$from 23:59:59'");
        return  $this->db->get()->result_array();
    }

    function select_report_room_count($from){
        $this->db->select('s.surgery_room_id, sr.surgery_room_number room, p.name, p.number_id, d.name surgeon,
        ss.name status, de.name destiny, s.date');
        $this->db->from('clinical_record cr');
        $this->db->join('patient p','cr.patient_id = p.patient_id');
        $this->db->join('surgery s','cr.clinical_record_id = s.clinical_record_id');
        $this->db->join('surgery_status ss','ss.surgery_status_id = s.surgery_status_id');
        $this->db->join('surgery_room sr','sr.surgery_room_id = s.surgery_room_id');
        $this->db->join('doctor d','d.doctor_id = s.surgeon');
        $this->db->join('destination de','de.destination_id = s.destination_id');
        $this->db->where("s.date BETWEEN '$from 00:00:00' AND '$from 23:59:59'");
        return $this->db->count_all_results();
    }

    function select_report_cancelations($from, $to){
        $this->db->select('s.date, sr.surgery_room_number room, p.name, p.number_id,
        scr.name reason, d.name surgeon');
        $this->db->from('clinical_record cr');
        $this->db->join('patient p','cr.patient_id = p.patient_id');
        $this->db->join('surgery s','cr.clinical_record_id = s.clinical_record_id');
        $this->db->join('surgery_cancelation_reason scr','scr.surgery_cancelation_reason_id = s.surgery_cancelation_reason_id','left');
        $this->db->join('surgery_room sr','sr.surgery_room_id = s.surgery_room_id','left');
        $this->db->join('doctor d','d.doctor_id = s.surgeon','left');
        $this->db->where("s.date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
		$names = array('Administrativos', 'Anestesia', 'Medicinas e Insumos', 'Material Osteosíntesis', 'Urgencias Vitales');
        $this->db->where_in('scr.name', $names);
        return $this->db->get()->result_array();
    }

    function select_report_cancelations_count($from, $to){
        $this->db->select('s.date, sr.surgery_room_number room, p.name, p.number_id,
        scr.name reason, d.name surgeon');
        $this->db->from('clinical_record cr');
        $this->db->join('patient p','cr.patient_id = p.patient_id');
        $this->db->join('surgery s','cr.clinical_record_id = s.clinical_record_id');
        $this->db->join('surgery_cancelation_reason scr','scr.surgery_cancelation_reason_id = s.surgery_cancelation_reason_id','left');
        $this->db->join('surgery_room sr','sr.surgery_room_id = s.surgery_room_id','left');
        $this->db->join('doctor d','d.doctor_id = s.surgeon','left');
        $this->db->where("s.date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
		$names = array('Administrativos', 'Anestesia', 'Medicinas e Insumos', 'Material Osteosíntesis', 'Urgencias Vitales');
        $this->db->where_in('scr.name', $names);
        return $this->db->count_all_results();
    }
	
	function select_report_adverse_events($from, $to){
        $this->db->select('p.name, p.number_id, ae.date, ae.observations');
        $this->db->from('clinical_record cr');
        $this->db->join('patient p','cr.patient_id = p.patient_id');
        $this->db->join('adverse_event ae','ae.clinical_record_id = cr.clinical_record_id');
        $this->db->where("ae.date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        return  $this->db->get()->result_array();
    }

    function select_report_adverse_events_count($from, $to){
        $this->db->select('p.name, p.number_id, ae.date, ae.observations');
        $this->db->from('clinical_record cr');
        $this->db->join('patient p','cr.patient_id = p.patient_id');
        $this->db->join('adverse_event ae','ae.clinical_record_id = cr.clinical_record_id');
        $this->db->where("ae.date BETWEEN '$from 00:00:00' AND '$to 23:59:59'");
        return $this->db->count_all_results();
    }

    function select_surgery_info($surgery_id){
        return $this->db->get_where('surgery',array('surgery_id'=>$surgery_id))->row();
    }

    function select_adverse_event_info($adverse_event_id){
        return $this->db->get_where('adverse_event',array('adverse_event_id'=>$adverse_event_id))->row();
    }

    function select_user_info($type, $id){
        //var_dump(array($type, $id));die;
        return $this->db->get_where($type, array("{$type}_id"=>$id))->row();
    }
    function delete_clinical_record($clinical_record_id){
        $this->db->where('clinical_record_id', $clinical_record_id);
        $this->db->delete('clinical_record');
    }
    function delete_surgery($surgery_id){
        $this->db->where('surgery_id', $surgery_id);
        $this->db->delete('surgery');
    }
    function delete_adverse_event($adverse_event_id){
        $this->db->where('adverse_event_id', $adverse_event_id);
        $this->db->delete('adverse_event');
    }
}
