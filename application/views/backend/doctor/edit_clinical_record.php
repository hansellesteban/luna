<?php $clinical_record_closed = $clinical_record_closed_status->clinical_record_status_id!=$clinical_record['clinical_record_status_id']; ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('edit_clinical_record'); ?>
                </div>
            </div>
            <div class="panel-body">
                <fieldset>
					<legend>Datos Generales</legend>
                    
					<div class="form-group row">
						<label style="text-align: right;" for="patient_identification" class="control-label col-sm-2 ">
							<?php echo get_phrase('number_id'); ?>
						</label>

						<div class="col-sm-3">
							<input type="text" class="form-control" name="patient_identification" id="patient_identification" data-validate="required"
								   data-message-required="<?php echo get_phrase('value_required'); ?>" value="<? echo isset($patient)?$patient['number_id']:"" ?>" readonly>
						</div>
						
					</div>
					<div class="form-group row">
						<label style="text-align: right;" for="patient_fullname" class="col-sm-2 control-label">
							<?php echo get_phrase('name'); ?>
						</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="patient_fullname" id="patient_fullname" value="<?= isset($patient)?$patient['name']:"" ?>" readonly>
						</div>
						<label style="text-align: right;" for="patient_blood_group" class="col-sm-2 control-label">
							<?php echo get_phrase('blood_group'); ?>
						</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" name="patient_blood_group" id="patient_blood_group" value="<?= isset($patient)?$patient['blood_group']:"" ?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label style="text-align: right;" for="patient_payer" class="col-sm-2 control-label">
							<?php echo get_phrase('payer'); ?>
						</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="patient_payer" id="patient_payer" value="<?= isset($patient)?$patient['payer']:"" ?>" readonly>
						</div>
                        <label style="text-align: right;" for="patient_age" class="col-sm-2 control-label">
							<?php echo get_phrase('age'); ?>
						</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" name="patient_age" id="patient_age" value="<?= isset($patient)?$patient['age']:"" ?>" readonly>
						</div>
					</div>
                    <?= form_close(); ?>
				</fieldset>
                
                <div style="clear:both;">&nbsp;</div>
                
                <fieldset>
                    <legend>Urgencias</legend>
                    <?php if($clinical_record_closed): ?>
                    <?= form_open(base_url().'index.php?doctor/record/update/'.$clinical_record['clinical_record_id'], array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data')); ?>
                    <div class="form-group">

                        <label for="patient_admission_date" class="col-sm-2 control-label">Fecha de Ingreso</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                <input type="date" class="form-control" name="patient_admission_date" id="patient_admission_date"
                                       data-validate="required"
                                       data-message-required="<?php echo get_phrase('value_required'); ?>"
                                       value="<?= $clinical_record['admission_date']; ?>" >
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="patient_diagnosis" class="col-sm-2 control-label">Diagnóstico</label>

                        <div class="col-sm-4">
                            <textarea class="form-control" id="patient_diagnosis" name="patient_diagnosis" data-validate="required"
                                      data-message-required="<?php echo get_phrase('value_required'); ?>"><?= $clinical_record['diagnosis']; ?></textarea>
                        </div>

                        <label for="patient_surgery" class="col-sm-1 control-label col-sm-offset-1">Cirugía</label>

                        <div class="col-sm-2">
                            <select name="patient_surgery" class="form-control" id="patient_surgery" data-validate="required"
                                    data-message-required="<?php echo get_phrase('value_required'); ?>" >
                                <?php foreach($boolean_list AS $boolean): ?>
                                    <option <?php if($clinical_record['surgery']==$boolean['value']): ?>selected<?php endif; ?> value="<?= $boolean['value']; ?>" label="<?= $boolean['name']; ?>"><?= $boolean['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="patient_outpatient_procedure" class="col-sm-2 control-label">Procedimiento Pte Ambulatorio</label>

                        <div class="col-sm-4">
                            <textarea class="form-control" id="patient_outpatient_procedure" name="patient_outpatient_procedure"><?= $clinical_record['outpatient_procedure']; ?></textarea>
                        </div>
                        <label for="patient_emergency_procedure_code" class="col-sm-2 control-label">Código Procedimiento Emergencias</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="Códigos separados por espacios" id="patient_emergency_procedure_code" name="patient_emergency_procedure_code"
                                   value="<?= $clinical_record['emergency_procedure_code']; ?>" >
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="patient_orthopedist" class="col-sm-1 control-label col-sm-offset-1">Ortopedista</label>
                        <div class="col-sm-4">
                            <select name="patient_orthopedist" class="form-control" id="patient_orthopedist">
                                <option value="-1">Seleccione...</option>
                                <?php foreach($doctor_list AS $doctor): ?>
                                    <option <?php if($clinical_record['orthopedist']==$doctor['doctor_id']): ?>selected<?php endif; ?> value="<?= $doctor['doctor_id']; ?>" label="<?= $doctor['name']; ?>"><?= $doctor['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <label for="patient_hospitalization" class="col-sm-2 control-label">Hospitalización</label>
                        <div class="col-sm-2">
                            <select name="patient_hospitalization" class="form-control" id="patient_hospitalization">
                                <?php foreach($boolean_list AS $boolean): ?>
                                <option <?php if($clinical_record['hospitalization']==$boolean['value']): ?>selected<?php endif; ?> value="<?= $boolean['value']; ?>" label="<?= $boolean['name']; ?>"><?= $boolean['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-primary pull-right" type="submit">
                        <i class="fa fa-save"></i>
                        Guardar Urgencias
                    </button>
                    <?= form_close(); ?>
                    <?php else: ?>
                        
                      <div class="form-group row">
                        <label style="text-align: right;" class="col-sm-2 control-label">
							Fecha de Ingreso
						</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="admission_date" id="admission_date" value="<?= $clinical_record['admission_date']; ?>" readonly>
						</div>
                        
                        <label style="text-align: right;" class="col-sm-2 control-label">
							Diagnóstico
						</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="diagnosis" id="diagnosis" value="<?= $clinical_record['diagnosis']; ?>" readonly>
						</div>
                     </div>
                     
                     <div class="form-group row">                        
                        <label style="text-align: right;" class="col-sm-2 control-label">
							Cirugía
						</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="surgery" id="surgery" value="<?= $clinical_record['surgery']==1?"Si":"No"; ?>" readonly>
						</div>
                        
                        <label style="text-align: right;" class="col-sm-2 control-label">
							Peocedimiento Pte Ambulatorio
						</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="outpatient_procedure" id="outpatient_procedure" value="<?= $clinical_record['outpatient_procedure']; ?>" readonly>
						</div>
                      </div>
                      
                      <div class="form-group row">                       
                        <label style="text-align: right;" class="col-sm-2 control-label">
							Ortopedista
						</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="orthopedist_detail" id="orthopedist_detail" value="<?= $clinical_record['orthopedist_detail']->name; ?>" readonly>
						</div>
                        
                        <label style="text-align: right;" class="col-sm-2 control-label">
							Hospitalización
						</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" name="hospitalization" id="hospitalization" value="<?= $clinical_record['hospitalization']==1?"Si":"No"; ?>" readonly>
						</div>
                      </div>
                            
                    <?php endif; ?>
                    
                </fieldset>
                
                <div style="clear:both;">&nbsp;</div>
                
                <fieldset>
                    <legend>Hospitalización</legend>
                    <?php if($clinical_record_closed): ?>
                    <?= form_open(base_url().'index.php?doctor/record/update/'.$clinical_record['clinical_record_id'], array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data')); ?>
                    <div class="form-group">
                        <label for="patient_hospitalization_date" class="col-sm-2 control-label">Fecha Hospitalización/FIP</label>

                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                <input type="date" class="form-control" name="patient_hospitalization_date"
                                       value="<?= $clinical_record['hospitalization_date'] ?>" id="patient_hospitalization_date" >
                            </div>
                        </div>

                        <label for="patient_hospitalization_bed" class="col-sm-2 control-label">Cama</label>

                        <div class="col-sm-3">
                                <input type="text" class="form-control" name="patient_hospitalization_bed"
                                       value="<?= $clinical_record['bed']; ?>" id="patient_hospitalization_bed" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="patient_medicine" class="col-sm-2 control-label">Medicina</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="patient_medicine"
                                   value="<?= $clinical_record['medicine']; ?>" id="patient_medicine" >
                        </div>

                        <label for="patient_paraclinics" class="col-sm-2 control-label">Paraclínicos</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="patient_paraclinics" id="patient_paraclinics"
                                   value="<?= $clinical_record['paraclinics']; ?>">
                        </div>
                    </div>
                    <button class="btn btn-primary pull-right" type="submit">
                        <i class="fa fa-save"></i>
                        Guardar Hospitalización
                    </button>
                    <?= form_close(); ?>
                    <?php else: ?>

                          <div class="form-group row">
                            <label style="text-align: right;" class="col-sm-2 control-label">
							Fecha de Hospitalización
						    </label>
						    <div class="col-sm-4">
							    <input type="text" class="form-control" name="hospitalization_date" id="hospitalization_date" value="<?= $clinical_record['hospitalization_date']; ?>" readonly>
						    </div>
                            
                            <label style="text-align: right;" class="col-sm-2 control-label">
							Cama
						    </label>
						    <div class="col-sm-4">
							    <input type="text" class="form-control" name="bed" id="bed" value="<?= $clinical_record['bed']; ?>" readonly>
						    </div>
                          </div>
                            
                          <div class="form-group row">
                            <label style="text-align: right;" class="col-sm-2 control-label">
							Medicina
						    </label>
						    <div class="col-sm-4">
							    <input type="text" class="form-control" name="medicine" id="medicine" value="<?= $clinical_record['medicine']; ?>" readonly>
						    </div>
                            
                            <label style="text-align: right;" class="col-sm-2 control-label">
							Paraclínicos
						    </label>
						    <div class="col-sm-4">
							    <input type="textarea" row="2" class="form-control" name="paraclinics" id="paraclinics" value="<?= $clinical_record['paraclinics']; ?>" readonly>
						    </div>
                          </div>
                        </div>
                    <?php endif; ?>
                </fieldset>
                <?php if($clinical_record_closed): ?>
                
                <div style="clear:both;">&nbsp;</div>
                
                <fieldset>
                    <legend>Cirugía  <span style="font-size: 14px; color: #AFAFAF;"><em>(En ésta sección se crean <b>Nuevas Cirugías</b>. Para editar alguna de clic en el botón <b>Editar</b> abajo.)</em></span></legend>
                    <?php echo form_open(base_url().'index.php?doctor/record/update/'.$clinical_record['clinical_record_id'].'/surgery/'.($surgery_edit?'edit/'.$surgery->surgery_id:'add'), array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data')); ?>
                    <input type="hidden" name="patient_add_surgery" id="patient_add_surgery" value="1">
                    <?php if($surgery_edit): ?>
                    <input type="hidden" name="patient_surgery_id" id="patient_surgery_id" value="<?= $surgery->surgery_id; ?>">
                    <?php else: ?>
                    <input type="hidden" name="clinical_record_id" id="clinical_record_id" value="<?= $clinical_record['clinical_record_id']; ?>">
                    <?php endif; ?>
                    <div class="form-group">
                        <label for="patient_cx_date" class="col-sm-2 control-label">Fecha de CX</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                <input type="date" class="form-control" name="patient_cx_date"
                                       value="<?= $surgery_edit?$surgery->date:date("Y-m-d"); ?>" id="patient_cx_date" >
                            </div>
                        </div>

                        <label for="patient_cx_time" class="col-sm-2 control-label">Hora de CX</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-clock"></i></span>
                                <input type="time" class="form-control" name="patient_cx_time"
                                       value="<?= $surgery_edit?$surgery->time:date("H:i"); ?>" id="patient_cx_time" >
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="patient_cx_code" class="col-sm-2 control-label">Código CX</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="Códigos separados por espacios" id="patient_cx_code" name="patient_cx_code" value="<?= $surgery_edit?$surgery->cx_code:""; ?>">
                        </div>

                        <label for="patient_surgical_procedure" class="col-sm-2 control-label">Procedimiento Quirúrgico</label>
                        <div class="col-sm-4">
                            <textarea class="form-control" id="patient_surgical_procedure" name="patient_surgical_procedure"><?= $surgery_edit?$surgery->surgical_procedure:"" ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="patient_cx_room" class="col-sm-2 control-label">Sala de CX</label>
                        <div class="col-sm-4">
                            <select name="patient_cx_room" class="form-control" id="patient_cx_room">
                                <option value="-1">Seleccione...</option>
                                <?php foreach($surgery_room_list AS $room): ?>
                                    <option <?php if($surgery_edit && $surgery->surgery_room_id==$room['surgery_room_id']): ?>selected<?php endif; ?> value="<?= $room['surgery_room_id']; ?>" label="<?= $room['surgery_room_number']; ?>"><?= $room['surgery_room_number']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <label for="patient_cx_status" class="col-sm-2 control-label">Estado</label>
                        <div class="col-sm-4">
                            <select name="patient_cx_status" class="form-control" id="patient_cx_status">
                                <option value="-1">Seleccione...</option>
                                <?php foreach($surgery_status_list AS $surgery_status): ?>
                                    <option <?php if($surgery_edit && $surgery->surgery_status_id==$surgery_status['surgery_status_id']): ?>selected<?php endif; ?>  value="<?= $surgery_status['surgery_status_id']; ?>" label="<?= $surgery_status['name']; ?>"><?= $surgery_status['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                    </div>
                    <div class="form-group">

                        <label for="patient_surgeon" class="col-sm-2 control-label">Cirujano</label>
                        <div class="col-sm-4">
                            <select name="patient_surgeon" class="form-control" id="patient_surgeon">
                                <option value="-1">Seleccione...</option>
                                <?php foreach($doctor_list AS $doctor): ?>
                                    <option <?php if($surgery_edit && $surgery->surgeon==$doctor['doctor_id']): ?>selected<?php endif; ?> value="<?= $doctor['doctor_id']; ?>" label="<?= $doctor['name']; ?>"><?= $doctor['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <label for="patient_surgeon_assistant" class="col-sm-2 control-label">Ayudante</label>
                        <div class="col-sm-4">
                            <select name="patient_surgeon_assistant" class="form-control" id="patient_surgeon_assistant">
                                <option value="-1">Seleccione...</option>
                                <?php foreach($doctor_list AS $doctor): ?>
                                    <option
									<?php if($surgery_edit && $surgery->surgeon_assistant==$doctor['doctor_id']): ?>selected<?php endif; ?> value="<?= $doctor['doctor_id']; ?>" label="<?= $doctor['name']; ?>"><?= $doctor['name']; ?>
                                    </option>
                                <?php endforeach; ?>
                                <?php foreach($nurse_list AS $nurse): ?>
                                    <option value="<?= $nurse['nurse_id']; ?>" label="<?= $nurse['name']; ?>"><?= $nurse['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="patient_osteosynthesis_materials_requested" class="col-sm-2 control-label">Material Osteosíntesis Solicitado</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="patient_osteosynthesis_materials_requested"
                                   id="patient_osteosynthesis_materials_requested" value="<?= $surgery_edit?$surgery->osteosynthesis_materials_requested:"" ?>">
                        </div>

                        <label for="patient_cancelation_reason" class="col-sm-2 control-label">Motivo de Cancelación</label>
                        <div class="col-sm-4">
                            <select name="patient_cancelation_reason" class="form-control" id="patient_cancelation_reason">
                                <option value="-1">Seleccione...</option>
                                <?php foreach($surgery_cancelation_reason_list AS $surgery_cancelation_reason): ?>
                                    <option <?php if($surgery_edit && $surgery->surgery_cancelation_reason_id==$surgery_cancelation_reason['surgery_cancelation_reason_id']): ?>selected<?php endif; ?> value="<?= $surgery_cancelation_reason['surgery_cancelation_reason_id']; ?>" label="<?= $surgery_cancelation_reason['name']; ?>"><?= $surgery_cancelation_reason['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="patient_destination" class="col-sm-2 control-label">Destino</label>
                        <div class="col-sm-4">
                            <select name="patient_destination" class="form-control" id="patient_destination">
                                <option value="-1">Seleccione...</option>
                                <?php foreach($destination_list AS $destination): ?>
                                    <option <?php if($surgery_edit && $surgery->destination_id==$destination['destination_id']): ?>selected<?php endif; ?> value="<?= $destination['destination_id']; ?>" label="<?= $destination['name']; ?>"><?= $destination['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <label for="patient_surgery_performed" class="col-sm-2 control-label">Cirugía Realizada</label>
                        <div class="col-sm-4">
                            <select name="patient_surgery_performed" class="form-control" id="patient_surgery_performed">
                                <?php foreach($boolean_list AS $boolean): ?>
                                    <option <?php if($surgery_edit && $surgery->surgery_performed_id==$boolean['value']): ?>selected<?php endif; ?> value="<?= $boolean['value']; ?>" label="<?= $boolean['name']; ?>"><?= $boolean['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-primary pull-right" type="submit">
                        <i class="fa fa-plus"></i>
                        Guardar Cirugía
                    </button>
                    <?= form_close(); ?>
                </fieldset>
                <?php endif; ?>
                
                <div style="clear:both;"></div>
                
                <fieldset>
                    <legend>Historial de Cirugías  <span style="font-size: 14px; color: #AFAFAF;"><em>(Desde aquí se editan las <b>Cirugías</b> dando clic en el botón <b>Editar</b>.)</em></span></legend>
                    <div style="overflow-x: scroll;">

                    <table class="table table-bordered table-striped datatable" id="table-2">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Hora</th>
                                <th>Código CX</th>
                                <th>Procedimiento Quirúrgico</th>
                                <th>Sala CX</th>
                                <th>Estado</th>
                                <th>Material Osteosíntesis</th>
                                <th>Cirujano</th>
                                <th>Ayudante</th>
                                <th>Cirugía Realizada</th>
                                <th>Destino</th>
                                <th>Motivo Cancelación</th>
                                <?php if($clinical_record_closed): ?>
                                <th>Acción</th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($surgeries AS $surgery):?>
                                <tr>

                                    <td><?= $surgery['date']; ?></td>
                                    <td><?= $surgery['time']; ?></td>
                                    <td><?= $surgery['cx_code']; ?></td>
                                    <td><?= $surgery['surgical_procedure']; ?></td>
                                    <td><?= $surgery['surgery_room']; ?></td>
                                    <td><?= $surgery['surgery_status']; ?></td>
                                    <td><?= $surgery['osteosynthesis_materials_requested']; ?></td>
                                    <td><?= $surgery['surgeon']; ?></td>
                                    <td><?= $surgery['surgeon_assistant']; ?></td>
                                    <td><?= $surgery['surgery_performed_id']=="1"?"Si":"No"; ?></td>
                                    <td><?= $surgery['destination']; ?></td>
                                    <td><?= $surgery['surgery_cancelation_reason']; ?></td>
                                    <?php if($clinical_record_closed): ?>
                                    <td>
                                        <a href="<?= base_url()."index.php?doctor/record/edit/{$clinical_record['clinical_record_id']}/surgery/edit/{$surgery['surgery_id']}"; ?>" class="btn btn-success btn-sm btn-icon icon-left">
                            <i class="entypo-pencil"></i>
                            Editar</a>
                                        <button type="button" class="btn btn-warning btn-sm btn-icon icon-left m-md" onclick="
                                            if(confirm('Está seguro de querer eliminar esta cirugía?')){var f = document.createElement('form');
                                            f.action = '<?= base_url()."index.php?doctor/record/delete/surgery/{$surgery['surgery_id']}"; ?>';f.method = 'post';
                                            var i = document.createElement('input');i.type = 'hidden';i.name = 'surgery_id';i.value = '<?= $surgery['surgery_id']; ?>';f.appendChild(i);
                                            var c = document.createElement('input');c.type = 'hidden';c.name = 'clinical_record_id';c.value = '<?= $clinical_record['clinical_record_id']; ?>';f.appendChild(c);
                                            f.submit();
                                            }else{return false;}">
                                            <i class="entypo-erase"></i>
                                            Eliminar</button>
                                    </td>
                                </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    </div>
                </fieldset>
                <?php if($clinical_record_closed): ?>
                
                <div style="clear:both;">&nbsp;</div>
                <div style="clear:both;">&nbsp;</div>
                
                <?php echo form_open(base_url().'index.php?doctor/record/update/'.$clinical_record['clinical_record_id'].'/adverse/event/'.($adverse_event_edit?'edit/'.$adverse_event->adverse_event_id:'add'), array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data')); ?>
                <fieldset>
                    <input type="hidden" name="patient_add_adverse_event" id="patient_add_adverse_event" value="1">
                    <?php if($adverse_event_edit): ?>
                        <input type="hidden" name="patient_adverse_event_id" id="patient_adverse_event_id" value="<?= $adverse_event->adverse_event_id; ?>">
                    <?php else: ?>
                        <input type="hidden" name="clinical_record_id" id="clinical_record_id" value="<?= $clinical_record['clinical_record_id']; ?>">
                    <?php endif; ?>               
                    
                    
                    <legend>Evento Adverso  <span style="font-size: 14px; color: #AFAFAF;"><em>(En ésta sección se crean <b>Nuevos Eventos Adversos</b>. Para editor alguno de clic en el botón <b>Editar</b> abajo.)</em></legend>
                    <div class="form-group">
                        <label for="patient_adverse_event" class="col-sm-2 control-label">Evento Adverso</label>
                        <div class="col-sm-2">
                            <select name="patient_adverse_event" class="form-control" id="patient_adverse_event">
                                <?php foreach($boolean_list AS $boolean): ?>
                                    <option <?php if($adverse_event_edit && $adverse_event->adverse_event==$boolean['value']): ?>selected<?php endif; ?> value="<?= $boolean['value']; ?>" label="<?= $boolean['name']; ?>"><?= $boolean['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <label for="patient_adverse_event_observations" class="col-sm-2 control-label">Observaciones Evento Adverso</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" id="patient_adverse_event_observations" name="patient_adverse_event_observations"><?= $adverse_event_edit?$adverse_event->observations:"" ?></textarea>
                        </div>
                    </div>
                    <button class="btn btn-primary pull-right" type="submit">
                        <i class="fa fa-plus"></i>
                        Guardar Evento Adverso
                    </button>
                </fieldset>
                
				<?= form_close(); ?>
                <?php endif; ?>
                
                <div style="clear:both;">&nbsp;</div>
                <div style="clear:both;">&nbsp;</div>
                
                <fieldset>
                    
                    <legend>Historial de Eventos Adversos  <span style="font-size: 14px; color: #AFAFAF;"><em>(Desde aquí se editan los <b>Eventos Adversos</b> dando clic en el botón <b>Editar</b>.)</em></span></legend>
                    <table class="table table-bordered table-striped datatable" id="table-2">
                        <thead>
                            <tr>
                                <th>Fecha y Hora</th>
                                <th>Evento Adverso</th>
                                <th>Observaciones</th>
                                <?php if($clinical_record_closed): ?>
                                <th>Acción</th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($adverse_events AS $adverse_event):?>
                            <tr>

                                <td><?= $adverse_event['date']; ?></td>
                                <td><?= $adverse_event['adverse_event']=="1"?"Si":"No"; ?></td>
                                <td><?= $adverse_event['observations']; ?></td>
                                <?php if($clinical_record_closed): ?>
                                <td>
                                    <a href="<?= base_url()."index.php?doctor/record/edit/{$clinical_record['clinical_record_id']}/adverse/event/edit/{$adverse_event['adverse_event_id']}"; ?>" class="btn btn-success btn-sm btn-icon icon-left">
                            <i class="entypo-pencil"></i>
                            Editar</a>
                                    <button type="button" class="btn btn-warning btn-sm btn-icon icon-left m-md" onclick="
                                        if(confirm('Está seguro de querer eliminar este evento adverso?')){var f = document.createElement('form');
                                        f.action = '<?= base_url()."index.php?doctor/record/delete/adverse_event/{$adverse_event['adverse_event_id']}"; ?>';f.method = 'post';
                                        var i = document.createElement('input');i.type = 'hidden';i.name = 'adverse_event_id';i.value = '<?= $adverse_event['adverse_event_id']; ?>';f.appendChild(i);
                                        var c = document.createElement('input');c.type = 'hidden';c.name = 'clinical_record_id';c.value = '<?= $clinical_record['clinical_record_id']; ?>';f.appendChild(c);
                                        f.submit();
                                        }else{return false;}">
                                        <i class="entypo-erase"></i>
                                        Eliminar</button>
                                </td>
                            </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </fieldset>
                
                <div style="clear:both;">&nbsp;</div>
                
                <fieldset>
                    <legend>Salida del Paciente</legend>
                    <?php if ($clinical_record_closed): ?>
                    <?php echo form_open(base_url().'index.php?doctor/record/update/'.$clinical_record['clinical_record_id'], array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data',
                        'onsubmit'=>"return confirm('Está seguro que desea cerrar esta ficha clínica?');")); ?>
                    <input type="hidden" name="clinical_record_id" id="clinical_record_id" value="<?= $clinical_record['clinical_record_id']; ?>">
                    <input type="hidden" name="patient_out" id="patient_out" value="1">
                    <div class="">

                        <label for="patient_out_observations" class="col-sm-2 control-label">Observaciones</label>
                        <div class="col-sm-4">
                            <textarea class="form-control" id="patient_out_observations" name="patient_out_observations"><?= $clinical_record['out_observations']; ?></textarea>
                        </div>

                        <label for="patient_out_date" class="col-sm-2 control-label">Fecha de Egreso</label>
                        <div class="form-group col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-clock"></i></span>
                                <input type="date" class="form-control" name="patient_out_date" id="patient_out_date"
                                       data-validate="required"
                                       data-message-required="<?php echo get_phrase('value_required'); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="control-label col-sm-12"><b>Última Modificación por:</b> <?= $clinical_record['modifier']->name; ?></label><br />
                        <label class="control-label col-sm-12"><b>Fecha y Hora:</b> <?= $clinical_record['modified']; ?></label>
                    </div>

                    <button class="btn btn-danger pull-right" type="submit">
                        <i class="fa fa-save">&nbsp;</i>
                        Salida de Paciente - Cerrar Ficha
                    </button>
                    <?= form_close(); ?>
                    <?php else: ?>
                        <div class="form-group">

                            <label class="control-label col-sm-12">
                                <span class="Bold"><b>Fecha de Cierre:</b></span> <?= $clinical_record['out_date']; ?>
                            </label>
                            <label class="control-label col-sm-12">
                                <span class="Bold"><b>Última Modificación por:</b> <?= $clinical_record['modifier']->name; ?>
                            </label>
                            <label class="control-label col-sm-12">
                                <span class="Bold"><b>Observaciones:</b></span> <?= $clinical_record['out_observations']; ?>
                            </label>
                        </div>
                        <?= form_open(base_url().'index.php?doctor/record/update/'.$clinical_record['clinical_record_id'], array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data',
                            'onsubmit'=>"return confirm('Está seguro que desea re-abrir esta ficha clínica?');")); ?>
                            <input type="hidden" name="clinical_record_reopened" id="clinical_record_reopened" value="1">
                            <button class="btn btn-success pull-right" type="submit">
                                <i class="fa fa-folder-open">&nbsp;</i>
                                Reabrir Ficha Clínica
                            </button>
                            
                            <div style="clear:both;">&nbsp;</div>
                            
                        <?= form_close(); ?>
                    <?php endif; ?>
                </fieldset>
            </div>
        </div>
    </div>
</div>