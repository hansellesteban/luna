<?php
if(isset($patient)){
    $patient = $patient[0];
}
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title" >
					<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('add_record'); ?>
				</div>
			</div>
			<div class="panel-body">
				<fieldset>
					<legend>Datos Generales</legend>
                    <?= form_open(base_url().'index.php?doctor/record/new/search/patient', array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data')); ?>
					<div class="form-group row">
						<label for="patient_identification" class="control-label col-sm-2 ">
							<?php echo get_phrase('number_id'); ?>
						</label>

						<div class="col-sm-3">
							<input type="text" class="form-control" name="patient_identification" id="patient_identification" data-validate="required"
								   data-message-required="<?php echo get_phrase('value_required'); ?>" value="<? echo isset($patient)?$patient['number_id']:"" ?>">
						</div>
						<div class="col-sm-3">
							<button class="btn btn-primary" type="submit">
								<i class="fa fa-search"></i>
								<?php echo get_phrase('patient_search'); ?>
							</button>
						</div>
						<div class="col-sm-3">
							<button onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/add_patient/from_record');"
									class="btn btn-primary" type="button">
								<i class="fa fa-plus"></i>
								<?php echo get_phrase('add_patient'); ?>
							</button>
						</div>
					</div>
					<div class="form-group row">
						<label style="text-align: right;" for="patient_fullname" class="col-sm-2 control-label">
							<?php echo get_phrase('name'); ?>
						</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="patient_fullname" id="patient_fullname" value="<?= isset($patient)?$patient['name']:"" ?>" readonly>
						</div>
						<label style="text-align: right;" for="patient_blood_group" class="col-sm-2 control-label">
							<?php echo get_phrase('blood_group'); ?>
						</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" name="patient_blood_group" id="patient_blood_group" value="<?= isset($patient)?$patient['blood_group']:"" ?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label style="text-align: right;" for="patient_payer" class="col-sm-2 control-label">
							<?php echo get_phrase('payer'); ?>
						</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="patient_payer" id="patient_payer" value="<?= isset($patient)?$patient['payer']:"" ?>" readonly>
						</div>
                        <label style="text-align: right;" for="patient_age" class="col-sm-2 control-label">
							<?php echo get_phrase('age'); ?>
						</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" name="patient_age" id="patient_age" value="<?= isset($patient)?$patient['age']:"" ?>" readonly>
						</div>
					</div>
                    <?= form_close(); ?>
                    
				</fieldset>
                
                <?php echo form_open(base_url().'index.php?doctor/record/create', array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data')); ?>
                <input type="hidden" name="patient_id" value="<?= isset($patient)?$patient['patient_id']:""?>" >
                <fieldset>
					<legend>Urgencias</legend>
                    <div class="form-group">

                        <label for="patient_admission_date" class="col-sm-2 control-label">Fecha de Ingreso</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                <input type="date" class="form-control" name="patient_admission_date" id="patient_admission_date" data-validate="required"
                                       data-message-required="<?php echo get_phrase('value_required'); ?>"
                                       value="<?php echo date("Y-m-d"); ?>" >
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="patient_diagnosis" class="col-sm-2 control-label">Diagnóstico</label>

                        <div class="col-sm-4">
                            <textarea class="form-control" id="patient_diagnosis" name="patient_diagnosis" data-validate="required"
                                   data-message-required="<?php echo get_phrase('value_required'); ?>"></textarea>
                        </div>

                        <label for="patient_surgery" class="col-sm-1 control-label col-sm-offset-1">Cirugía</label>

                        <div class="col-sm-2">
                            <select name="patient_surgery" class="form-control" id="patient_surgery">
                                <?php foreach($boolean_list AS $boolean): ?>
                                    <option value="<?= $boolean['value']; ?>" label="<?= $boolean['name']; ?>"><?= $boolean['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="patient_outpatient_procedure" class="col-sm-2 control-label">Procedimiento Pte Ambulatorio</label>

                        <div class="col-sm-4">
                            <textarea class="form-control" id="patient_outpatient_procedure" name="patient_outpatient_procedure"></textarea>
                        </div>
                        <label for="patient_emergency_procedure_code" class="col-sm-2 control-label">Código Procedimiento Emergencias</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control" placeholder="Códigos separados por espacios" id="patient_emergency_procedure_code" name="patient_emergency_procedure_code" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="patient_orthopedist" class="col-sm-1 control-label col-sm-offset-1">Ortopedista</label>

                        <div class="col-sm-4">
                            <select name="patient_orthopedist" class="form-control" id="patient_orthopedist">
                                <option value="-1">Seleccione...</option>
                                <?php foreach($doctor_list AS $doctor): ?>
                                <option value="<?= $doctor['doctor_id']; ?>" label="<?= $doctor['name']; ?>"><?= $doctor['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <label for="patient_hospitalization" class="col-sm-2 control-label">Hospitalización</label>

                        <div class="col-sm-2">
                            <select name="patient_hospitalization" class="form-control" id="patient_hospitalization">
                                <?php foreach($boolean_list AS $boolean): ?>
                                    <option value="<?= $boolean['value']; ?>" label="<?= $boolean['name']; ?>"><?= $boolean['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

				</fieldset>
				<fieldset>
					<legend>Hospitalización</legend>
                    <div class="form-group">
                        <label for="patient_hospitalization_date" class="col-sm-2 control-label">Fecha Hospitalización/FIP</label>

                        <div class="col-sm-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                <input type="date" class="form-control" name="patient_hospitalization_date"
                                       value="<?php echo date("Y-m-d"); ?>" id="patient_hospitalization_date">
                            </div>
                        </div>

                        <label for="patient_hospitalization_bed" class="col-sm-2 control-label">Cama</label>

                        <div class="col-sm-3">
                                <input type="text" class="form-control" name="patient_hospitalization_bed"
                                       value="<?= $clinical_record['bed']; ?>" id="patient_hospitalization_bed" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="patient_medicine" class="col-sm-2 control-label">Medicina</label>

                        <div class="col-sm-4">
                                <input type="text" class="form-control" name="patient_medicine"
                                       value="" id="patient_medicine" >
                        </div>

                        <label for="patient_paraclinics" class="col-sm-2 control-label">Paraclínicos</label>

                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="patient_paraclinics"
                                   value="" id="patient_paraclinics" >
                        </div>
                    </div>
				</fieldset>
                <button class="btn btn-success pull-right" type="submit">
                    <i class="fa fa-save">&nbsp;</i>
                    Guardar Ficha Clínica y Continuar
                </button>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>