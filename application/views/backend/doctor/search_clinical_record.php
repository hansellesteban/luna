<div class="row">
    <div class="form-group col-sm-12">
        <?= form_open(base_url().'index.php?doctor/record/search',array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data')); ?>
        <div class="input-group col-sm-4">
            <input type="search" name="search" class="form-control" id="search" data-validate="required"
                   data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?= $search; ?>">
        </div>
        <button class="btn btn-primary col-sm-2" type="submit">
            <i class="fa fa-search"></i>
            Buscar Ficha Clínica
        </button>
    </div>
</div>
<br>
<?php if($records_count>0): ?>
    <table class="table table-bordered table-striped datatable" id="table-2">
        <thead>
        <tr>
            <th>Identificación</th>
            <th>Nombre</th>
            <th>Sexo</th>
            <th>Payer</th>
            <th>Grupo Sanguineo</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($patients as $patient) { ?>
            <tr>
                <td><?= $patient['number_id']; ?></td>
                <td><?= $patient['name']; ?></td>
                <td><?= $patient['sex']; ?></td>
                <td><?= $patient['payer']; ?></td>
                <td><?= $patient['blood_group']; ?></td>
            </tr>
            <?php if($patient['clinical_record_patient_id']): ?>
                <tr>
                    <td colspan="5" style="padding-left: 10px;">
                        <table class="table table-bordered table-striped datatable">
                            <tr>
                                <td style="width: 15%;">No. Ficha Clínica</td>
                                <td style="width: 15%;">Fecha de Ingreso</td>
                                <td style="width: 40%;">Diagnostico</td>
                                <td style="width: 20%;">Estado</td>
                                <td style="width: 10%;"></td>
                            </tr>
                            <?php foreach($records AS $record2): ?>
                                <?php if($patient['patient_id']===$record2['clinical_record_patient_id']): ?>
                                    <tr style="margin-left: 10px;">
                                        <td><?= $record2['clinical_record_id']; ?></td>
                                        <td><?= $record2['admission_date']; ?></td>
                                        <td><?= $record2['diagnosis']; ?></td>
                                        <td><?= $record2['clinical_record_status']; ?></td>
                                        <td>
                                            <a href="<?= base_url().'index.php?doctor/record/edit/'.$record2['clinical_record_id']; ?>" class="btn btn-success btn-sm btn-icon icon-left">
                                                <i class="entypo-pencil"></i>
                                                Editar</a>
                                            <button type="button" class="btn btn-warning btn-sm btn-icon icon-left m-md" onclick="
                                                if(confirm('Está seguro de querer eliminar esta ficha clínica?')){var f = document.createElement('form');
                                                f.action = '<?= base_url().'index.php?doctor/record/delete'; ?>';f.method = 'post';
                                                var i = document.createElement('input');i.type = 'hidden';i.name = 'clinical_record_id';i.value = '<?= $record2['clinical_record_id']; ?>';f.appendChild(i);
                                                var s = document.createElement('input');s.type = 'hidden';s.name = 'search';s.value = '<?= $search; ?>';f.appendChild(s);
                                                f.submit();
                                                }else{return false;}">
                                                <i class="entypo-erase"></i>
                                                Eliminar</button>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </table>
                    </td>
                </tr>
            <?php endif; ?>
        <?php } ?>
        </tbody>
    </table>
<?php elseif($search != ""): ?>
    No se han encontrado resultados
<?php endif; ?>