<div style="clear:both;"></div>
<br>
<div class="pull-left">
    <?= form_open(base_url().'index.php?nurse/reports/'.$report_url, array('class' => 'form-horizontal form-groups validate record-add', 'enctype' => 'multipart/form-data','id'=>'form')); ?>
    <?php if(isset($from)): ?>
    <div class="form-group col-sm-4">
        <label for="from" class="col-sm-3 control-label">Desde</label>
        <div class="col-sm-9">
            <div class="input-group">
                <span class="input-group-addon"><i class="entypo-clock"></i></span>
                <input type="date" class="form-control" name="from" id="from" value="<?= $from; ?>" >
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if(isset($to)): ?>
    <div class="form-group col-sm-4">
        <label for="hasta" class="col-sm-3 control-label">Hasta</label>
        <div class="col-sm-9">
            <div class="input-group">
                <span class="input-group-addon"><i class="entypo-clock"></i></span>
                <input type="date" class="form-control" name="hasta" id="hasta" value="<?= $to; ?>" >
            </div>
        </div>
    </div>
    <?php endif; ?>
    <button class="btn btn-primary pull-right" type="submit">
        <i class="fa fa-search">&nbsp;</i>
        Buscar
    </button>
    <?= form_close(); ?>
</div>
<?php if($report_count>0): ?>
<table class="table table-bordered table-striped datatable" id="table-1">
    <thead>
        <tr>
            <?php foreach($report_map AS $key => $title): ?>
            <th><?= $title ?></th>
            <?php endforeach; ?>
        </tr>
    </thead>

    <tbody>
        <?php foreach ($report as $row) { ?>
            <?php if($row['name']): ?>
            <tr>
                <?php foreach($report_map AS $key => $title): ?>
                <td><?php echo $row[$key]?></td>
                <?php endforeach; ?>
            </tr>
            <?php endif; ?>
        <?php } ?>
    </tbody>
</table>
<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-1").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
        <?php if(isset($autorefresh)): ?>
        setInterval(function(){
            $("#form").submit();
        },<?= $autorefresh; ?>000);
        <?php endif; ?>
    });
</script>
<?php else: ?>
    <?= get_phrase('no_results_found'); ?>
<?php endif; ?>

