<head>
<meta http-equiv="refresh" content="300" > 
</head>

<body>

<div class="row"> 

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/patient">
            <div class="tile-stats tile-white-red">
                <div class="icon"><i class="fa fa-user"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('patient'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3>Paciente</h3>
            </div>
        </a>
    </div>
    
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/doctor">
            <div class="tile-stats tile-white tile-white-primary">
                <div class="icon"><i class="fa fa-user-md"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('doctor'); ?>"
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('doctor') ?></h3>
            </div>
        </a>
    </div>
    
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/nurse">
            <div class="tile-stats tile-white-aqua">
                <div class="icon"><i class="fa fa-plus-square"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('nurse'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('nurse') ?></h3>
            </div>
        </a>
    </div>
    
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/report/programming/surgery">
            <div class="tile-stats tile-white-green">
                <div class="icon"><i class="fa fa-wheelchair"></i></div>
                <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('surgery', array('surgery_status_id' => '5'))->result_array());?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3>Cirugías Programadas</h3>
            </div>
        </a>
    </div>
 </div> 

<div class="row"> 
    
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/record/search">
            <div class="tile-stats tile-white-plum">
                <div class="icon"><i class="fa fa-file-text"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('clinical_record'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('Ficha Clínica') ?></h3>
            </div>
        </a>
    </div> 

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/payment_history">
            <div class="tile-stats tile-white-pink">
                <div class="icon"><i class="fa fa-list-alt"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('invoice'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('payment') ?></h3>
            </div>
        </a>
    </div>
    
    <!--
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/pharmacist">
            <div class="tile-stats tile-white-blue">
                <div class="icon"><i class="fa fa-medkit"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('pharmacist'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('pharmacist') ?></h3>
            </div>
        </a>
    </div>
    
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/laboratorist">
            <div class="tile-stats tile-white-cyan">
                <div class="icon"><i class="fa fa-flask"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('laboratorist'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('laboratorist') ?></h3>
            </div>
        </a>
    </div>
    -->

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/accountant">
            <div class="tile-stats tile-white-purple">
                <div class="icon"><i class="fa fa-money"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('accountant'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('accountant') ?></h3>
            </div>
        </a>
    </div>

    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/medicine">
            <div class="tile-stats tile-white-orange">
                <div class="icon"><i class="fa fa-heart"></i></div>
                <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('medicine'); ?>" 
                     data-duration="1500" data-delay="0">0</div>
                <h3><?php echo get_phrase('medicine') ?></h3>
            </div>
        </a>
    </div>

    <!--
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/birth_report">
            <div class="tile-stats tile-white-brown">
                <div class="icon"><i class="fa fa-github-alt"></i></div>
                <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('report', array('type' => 'birth'))->result_array());?>" 
                     data-duration="1500" data-delay="0"></div>
                <h3><?php echo get_phrase('birth_report') ?></h3>
            </div>
        </a>
    </div>
    -->

    <!--
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/death_report">
            <div class="tile-stats tile-white-plum">
                <div class="icon"><i class="fa fa-ban"></i></div>
                <div class="num" data-start="0" data-end="<?php echo count($this->db->get_where('report', array('type' => 'death'))->result_array());?>" 
                     data-duration="1500" data-delay="0"></div>
                <h3><?php echo get_phrase('death_report') ?></h3>
            </div>
        </a>
    </div>
    -->

    <!--
    <div class="col-sm-3">
        <a href="<?php echo base_url(); ?>index.php?admin/system_settings">
            <div class="tile-stats tile-white-gray">
                <div class="icon"><i class="fa fa-h-square"></i></div>
                <div class="num">&nbsp;</div>
                <h3><?php echo get_phrase('settings') ?></h3>
            </div>
        </a>
    </div>
    -->
</div>



<div class="row">
    <!-- CALENDAR-->
    
    <!--
    <div class="col-md-12 col-xs-12">    
        <div class="panel panel-primary " data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-calendar"></i>
                    <?php echo get_phrase('event_schedule'); ?>
                </div>
            </div>
            <div class="panel-body" style="padding:0px;">
                <div class="calendar-env">
                    <div class="calendar-body">
                        <div id="notice_calendar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->


<script type="text/javascript">
    
    $(document).ready(function()
    {
        var calendar = $('#notice_calendar');
				
        $('#notice_calendar').fullCalendar
        ({
            header:
            {
                left: 'title',
                right: 'month,agendaWeek,agendaDay today prev,next'
            },

            editable: false,
            firstDay: 1,
            height: 530,
            droppable: false,
			ignoreTimezone: true,

            events:
            [
                <?php
                $notices   = $this->db->get('notice')->result_array();
                foreach ($notices as $row):
                ?>
                    {
                        title   :   "<?php echo $title = $this->db->get_where('notice' , 
                                        array('notice_id' => $row['notice_id'] ))->row()->title;?>",
                        start   :   new Date("<?=$row['start_timestamp']?>"),
                        end     :   new Date("<?=$row['end_timestamp']?>"),
                        allDay: true
                    },
                <?php endforeach ?>
            ]
        });
    });
</script>

<script type="text/javascript">
    jQuery(window).load(function ()
    {
        var $ = jQuery;

        $("#table-2").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>"
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });

        // Highlighted rows
        $("#table-2 tbody input[type=checkbox]").each(function (i, el)
        {
            var $this = $(el),
                    $p = $this.closest('tr');

            $(el).on('change', function ()
            {
                var is_checked = $this.is(':checked');

                $p[is_checked ? 'addClass' : 'removeClass']('highlight');
            });
        });

        // Replace Checboxes
        $(".pagination a").click(function (ev)
        {
            replaceCheckboxes();
        });
    });
</script>

</body>