<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Hansel Ramos Osorio <hansell.ramos@gmail.com>
 * Date: 11/9/15
 * Time: 1:23 PM
 */

class Record extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    public function add(){
        $this->load->view('backend/record/add');
    }

}