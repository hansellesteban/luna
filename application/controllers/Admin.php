<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Joyonto Roy
 * 	date	: 1 August, 2014
 * 	http://codecanyon.net/user/Creativeitem
 * 	http://creativeitem.com
 */

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
		$this->load->library('session');

        /* cache control */
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    /*     * *default function, redirects to login page if no admin logged in yet** */

    public function index() {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');
        if ($this->session->userdata('admin_login') == 1)
            redirect(base_url() . 'index.php?admin/dashboard', 'refresh');
    }

    /*     * *ADMIN DASHBOARD** */

    function dashboard() {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }
        $page_data['page_name'] = 'dashboard';
        $page_data['page_title'] = get_phrase('admin_dashboard');
        $this->load->view('backend/index', $page_data);
    }

    /*     * ***LANGUAGE SETTINGS******** */

    function manage_language($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'edit_phrase') {
            $page_data['edit_profile'] = $param2;
        }
        if ($param1 == 'update_phrase') {
            $language = $param2;
            $total_phrase = $this->input->post('total_phrase');
            for ($i = 1; $i < $total_phrase; $i++) {
                //$data[$language]	=	$this->input->post('phrase').$i;
                $this->db->where('phrase_id', $i);
                $this->db->update('language', array($language => $this->input->post('phrase' . $i)));
            }
            redirect(base_url() . 'index.php?admin/manage_language/edit_phrase/' . $language, 'refresh');
        }
        if ($param1 == 'do_update') {
            $language = $this->input->post('language');
            $data[$language] = $this->input->post('phrase');
            $this->db->where('phrase_id', $param2);
            $this->db->update('language', $data);
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
        }
        if ($param1 == 'add_phrase') {
            $data['phrase'] = $this->input->post('phrase');
            $this->db->insert('language', $data);
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
        }
        if ($param1 == 'add_language') {
            $language = $this->input->post('language');
            $this->load->dbforge();
            $fields = array(
                $language => array(
                    'type' => 'LONGTEXT'
                )
            );
            $this->dbforge->add_column('language', $fields);

            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
        }
        if ($param1 == 'delete_language') {
            $language = $param2;
            $this->load->dbforge();
            $this->dbforge->drop_column('language', $language);
            $this->session->set_flashdata('message', get_phrase('settings_updated'));

            redirect(base_url() . 'index.php?admin/manage_language/', 'refresh');
        }
        $page_data['page_name'] = 'manage_language';
        $page_data['page_title'] = get_phrase('manage_language');
        //$page_data['language_phrases'] = $this->db->get('language')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    /*     * ***SITE/SYSTEM SETTINGS******** */

    function system_settings($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'do_update') {
            $this->crud_model->update_system_settings();
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
        }
        if ($param1 == 'upload_logo') {
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/system_settings/', 'refresh');
        }
        $page_data['page_name'] = 'system_settings';
        $page_data['page_title'] = get_phrase('system_settings');
        $page_data['settings'] = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    // SMS settings.
    function sms_settings($param1 = '') {

        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'do_update') {
            $this->crud_model->update_sms_settings();
            $this->session->set_flashdata('message', get_phrase('settings_updated'));
            redirect(base_url() . 'index.php?admin/sms_settings/', 'refresh');
        }

        $page_data['page_name'] = 'sms_settings';
        $page_data['page_title'] = get_phrase('sms_settings');
        $this->load->view('backend/index', $page_data);
    }

    /*     * ****MANAGE OWN PROFILE AND CHANGE PASSWORD** */

    function manage_profile($param1 = '', $param2 = '', $param3 = '') {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url() . 'index.php?login', 'refresh');

        if ($param1 == 'update_profile_info') {
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');

            $this->db->where('admin_id', $this->session->userdata('login_user_id'));
            $this->db->update('admin', $data);

            $this->session->set_flashdata('message', get_phrase('profile_info_updated_successfuly'));
            redirect('index.php?admin/manage_profile');
        }
        if ($param1 == 'change_password') {
            $current_password_input = sha1($this->input->post('password'));
            $new_password = sha1($this->input->post('new_password'));
            $confirm_new_password = sha1($this->input->post('confirm_new_password'));

            $current_password_db = $this->db->get_where('admin', array('admin_id' =>
                        $this->session->userdata('login_user_id')))->row()->password;

            if ($current_password_db == $current_password_input && $new_password == $confirm_new_password) {
                $this->db->where('admin_id', $this->session->userdata('login_user_id'));
                $this->db->update('admin', array('password' => $new_password));

                $this->session->set_flashdata('message', get_phrase('password_info_updated_successfuly'));
                redirect('index.php?admin/manage_profile');
            } else {
                $this->session->set_flashdata('message', get_phrase('password_update_failed'));
                redirect('index.php?admin/manage_profile');
            }
        }
        $page_data['page_name'] = 'manage_profile';
        $page_data['page_title'] = get_phrase('manage_profile');
        $page_data['edit_data'] = $this->db->get_where('admin', array('admin_id' => $this->session->userdata('login_user_id')))->result_array();
        $this->load->view('backend/index', $page_data);
    }

    function department($task = "", $department_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_department_info();
            $this->session->set_flashdata('message', get_phrase('department_info_saved_successfuly'));
            redirect('index.php?admin/department');
        }

        if ($task == "update") {
            $this->crud_model->update_department_info($department_id);
            $this->session->set_flashdata('message', get_phrase('department_info_updated_successfuly'));
            redirect('index.php?admin/department');
        }

        if ($task == "delete") {
            $this->crud_model->delete_department_info($department_id);
            redirect('index.php?admin/department');
        }

        $data['department_info'] = $this->crud_model->select_department_info();
        $data['page_name'] = 'manage_department';
        $data['page_title'] = get_phrase('department');
        $this->load->view('backend/index', $data);
    }

    function doctor($task = "", $doctor_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_doctor_info();
            $this->session->set_flashdata('message', get_phrase('doctor_info_saved_successfuly'));
            redirect('index.php?admin/doctor');
        }

        if ($task == "update") {
            $this->crud_model->update_doctor_info($doctor_id);
            $this->session->set_flashdata('message', get_phrase('doctor_info_updated_successfuly'));
            redirect('index.php?admin/doctor');
        }

        if ($task == "delete") {
            $this->crud_model->delete_doctor_info($doctor_id);
            redirect('index.php?admin/doctor');
        }

        $data['doctor_info'] = $this->crud_model->select_doctor_info();
        $data['page_name'] = 'manage_doctor';
        $data['page_title'] = get_phrase('doctor');
        $this->load->view('backend/index', $data);
    }

    function patient($task = "", $patient_id = "", $from_record = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_patient_info();
            $this->session->set_flashdata('message', get_phrase('patient_info_saved_successfuly'));
            redirect(
                !empty($from_record)?
                    base_url().'index.php?admin/record/new/patient/'.$this->input->post('number_id'):
                    base_url().'index.php?admin/patient');
        }

        if ($task == "update") {
            $this->crud_model->update_patient_info($patient_id);
            $this->session->set_flashdata('message', get_phrase('patient_info_updated_successfuly'));
            redirect(base_url().'index.php?admin/patient');
        }

        if ($task == "delete") {
            $this->crud_model->delete_patient_info($patient_id);
            redirect(base_url().'index.php?admin/patient');
        }

        $data['patient_info'] = $this->crud_model->select_patient_info();
        $data['page_name'] = 'manage_patient';
        $data['page_title'] = get_phrase('patient');
        $this->load->view('backend/index', $data);
    }
	
	function medication_history($param1 = "", $prescription_id = "")
    {
        if ($this->session->userdata('admin_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        $patient_name               =   $this->db->get_where('patient' ,
                                        array('patient_id' => $param1 ))->row()->name; // $param1 = $patient_id
        $data['prescription_info']  =   $this->crud_model->select_medication_history($param1); // $param1 = $patient_id
        $data['menu_check']         =   'from_patient';
        $data['page_name']          =   'manage_prescription';
        $data['page_title']         =   get_phrase('medication_history_of_:_') . $patient_name;
        $this->load->view('backend/index', $data);
    }
	
	function prescription($task = "", $prescription_id = "", $menu_check = '', $patient_id = '')
    {
        if ($this->session->userdata('admin_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        if ($task == "create")
        {
            $this->crud_model->save_prescription_info();
            $this->session->set_flashdata('message' , get_phrase('prescription_info_saved_successfuly'));
            redirect('index.php?admin/prescription');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_prescription_info($prescription_id);
            $this->session->set_flashdata('message' , get_phrase('prescription_info_updated_successfuly'));
            if($menu_check == 'from_prescription')
                redirect('index.php?admin/prescription');
            else
                redirect('index.php?admin/medication_history/'.$patient_id);
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_prescription_info($prescription_id);
            if($menu_check == 'from_prescription')
                redirect('index.php?admin/prescription');
            else
                redirect('index.php?admin/medication_history/'.$patient_id);
        }
        
        $data['prescription_info']  = $this->crud_model->select_prescription_info_by_doctor_id();
        $data['menu_check']         = 'from_prescription';
        $data['page_name']          = 'manage_prescription';
        $data['page_title']         = get_phrase('prescription');
        $this->load->view('backend/index', $data);
    }
    
    function diagnosis_report($task = "", $diagnosis_report_id = "")
    {
        if ($this->session->userdata('admin_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        if ($task == "create")
        {
            $this->crud_model->save_diagnosis_report_info();
            $this->session->set_flashdata('message' , get_phrase('diagnosis_report_info_saved_successfuly'));
            redirect('index.php?admin/prescription');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_diagnosis_report_info($diagnosis_report_id);
            $this->session->set_flashdata('message' , get_phrase('diagnosis_report_info_deleted_successfuly'));
            redirect('index.php?admin/prescription');
        }
    }
	
	function nurse($task = "", $nurse_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_nurse_info();
            $this->session->set_flashdata('message', get_phrase('nurse_info_saved_successfuly'));
            redirect('index.php?admin/nurse');
        }

        if ($task == "update") {
            $this->crud_model->update_nurse_info($nurse_id);
            $this->session->set_flashdata('message', get_phrase('nurse_info_updated_successfuly'));
            redirect('index.php?admin/nurse');
        }

        if ($task == "delete") {
            $this->crud_model->delete_nurse_info($nurse_id);
            redirect('index.php?admin/nurse');
        }

        $data['nurse_info'] = $this->crud_model->select_nurse_info();
        $data['page_name'] = 'manage_nurse';
        $data['page_title'] = get_phrase('nurse');
        $this->load->view('backend/index', $data);
    }

    function pharmacist($task = "", $pharmacist_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_pharmacist_info();
            $this->session->set_flashdata('message', get_phrase('pharmacist_info_saved_successfuly'));
            redirect('index.php?admin/pharmacist');
        }

        if ($task == "update") {
            $this->crud_model->update_pharmacist_info($pharmacist_id);
            $this->session->set_flashdata('message', get_phrase('pharmacist_info_updated_successfuly'));
            redirect('index.php?admin/pharmacist');
        }

        if ($task == "delete") {
            $this->crud_model->delete_pharmacist_info($pharmacist_id);
            redirect('index.php?admin/pharmacist');
        }

        $data['pharmacist_info'] = $this->crud_model->select_pharmacist_info();
        $data['page_name'] = 'manage_pharmacist';
        $data['page_title'] = get_phrase('pharmacist');
        $this->load->view('backend/index', $data);
    }

    function laboratorist($task = "", $laboratorist_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_laboratorist_info();
            $this->session->set_flashdata('message', get_phrase('laboratorist_info_saved_successfuly'));
            redirect('index.php?admin/laboratorist');
        }

        if ($task == "update") {
            $this->crud_model->update_laboratorist_info($laboratorist_id);
            $this->session->set_flashdata('message', get_phrase('laboratorist_info_updated_successfuly'));
            redirect('index.php?admin/laboratorist');
        }

        if ($task == "delete") {
            $this->crud_model->delete_laboratorist_info($laboratorist_id);
            redirect('index.php?admin/laboratorist');
        }

        $data['laboratorist_info'] = $this->crud_model->select_laboratorist_info();
        $data['page_name'] = 'manage_laboratorist';
        $data['page_title'] = get_phrase('laboratorist');
        $this->load->view('backend/index', $data);
    }

    function accountant($task = "", $accountant_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_accountant_info();
            $this->session->set_flashdata('message', get_phrase('accountant_info_saved_successfuly'));
            redirect('index.php?admin/accountant');
        }

        if ($task == "update") {
            $this->crud_model->update_accountant_info($accountant_id);
            $this->session->set_flashdata('message', get_phrase('accountant_info_updated_successfuly'));
            redirect('index.php?admin/accountant');
        }

        if ($task == "delete") {
            $this->crud_model->delete_accountant_info($accountant_id);
            redirect('index.php?admin/accountant');
        }

        $data['accountant_info'] = $this->crud_model->select_accountant_info();
        $data['page_name'] = 'manage_accountant';
        $data['page_title'] = get_phrase('accountant');
        $this->load->view('backend/index', $data);
    }

    function invoice_add($task = "") 
    {
        if ($this->session->userdata('admin_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        if ($task == "create")
        {
            $this->crud_model->create_invoice();
            $this->session->set_flashdata('message' , get_phrase('invoice_info_saved_successfuly'));
            redirect('index.php?admin/invoice_add');
        }
        
        $data['page_name']      = 'add_invoice';
        $data['page_title']     = get_phrase('invoice');
        $this->load->view('backend/index', $data);
    }
    
    function invoice_manage($task = "", $invoice_id = "") 
    {
        if ($this->session->userdata('admin_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_invoice($invoice_id);
            $this->session->set_flashdata('message' , get_phrase('invoice_info_updated_successfuly'));
            redirect('index.php?admin/invoice_manage');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_invoice($invoice_id);
            redirect('index.php?admin/invoice_manage');
        }
        
        $data['invoice_info']   = $this->crud_model->select_invoice_info();
        $data['page_name']      = 'manage_invoice';
        $data['page_title']     = get_phrase('invoice');
        $this->load->view('backend/index', $data);
    }
	
	function receptionist($task = "", $receptionist_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_receptionist_info();
            $this->session->set_flashdata('message', get_phrase('receptionist_info_saved_successfuly'));
            redirect('index.php?admin/receptionist');
        }

        if ($task == "update") {
            $this->crud_model->update_receptionist_info($receptionist_id);
            $this->session->set_flashdata('message', get_phrase('receptionist_info_updated_successfuly'));
            redirect('index.php?admin/receptionist');
        }

        if ($task == "delete") {
            $this->crud_model->delete_receptionist_info($receptionist_id);
            redirect('index.php?admin/receptionist');
        }

        $data['receptionist_info'] = $this->crud_model->select_receptionist_info();
        $data['page_name'] = 'manage_receptionist';
        $data['page_title'] = get_phrase('receptionist');
        $this->load->view('backend/index', $data);
    }

    function payment_history($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['invoice_info'] = $this->crud_model->select_invoice_info();
        $data['page_name'] = 'show_payment_history';
        $data['page_title'] = get_phrase('payment_history');
        $this->load->view('backend/index', $data);
    }

    function bed_allotment($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['bed_allotment_info'] = $this->crud_model->select_bed_allotment_info();
        $data['page_name'] = 'show_bed_allotment';
        $data['page_title'] = get_phrase('bed_allotment');
        $this->load->view('backend/index', $data);
    }

    function blood_bank($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['blood_bank_info'] = $this->crud_model->select_blood_bank_info();
        $data['page_name'] = 'show_blood_bank';
        $data['page_title'] = get_phrase('blood_bank');
        $this->load->view('backend/index', $data);
    }

    function blood_donor($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['blood_donor_info'] = $this->crud_model->select_blood_donor_info();
        $data['page_name'] = 'show_blood_donor';
        $data['page_title'] = get_phrase('blood_donor');
        $this->load->view('backend/index', $data);
    }

    function medicine($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['medicine_info'] = $this->crud_model->select_medicine_info();
        $data['page_name'] = 'show_medicine';
        $data['page_title'] = get_phrase('medicine');
        $this->load->view('backend/index', $data);
    }

    function operation_report($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'show_operation_report';
        $data['page_title'] = get_phrase('operation_report');
        $this->load->view('backend/index', $data);
    }

    function birth_report($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'show_birth_report';
        $data['page_title'] = get_phrase('birth_report');
        $this->load->view('backend/index', $data);
    }

    function death_report($task = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $data['page_name'] = 'show_death_report';
        $data['page_title'] = get_phrase('death_report');
        $this->load->view('backend/index', $data);
    }

    function notice($task = "", $notice_id = "") {
        if ($this->session->userdata('admin_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_notice_info();
            $this->session->set_flashdata('message', get_phrase('notice_info_saved_successfuly'));
            redirect('index.php?admin/notice');
        }

        if ($task == "update") {
            $this->crud_model->update_notice_info($notice_id);
            $this->session->set_flashdata('message', get_phrase('notice_info_updated_successfuly'));
            redirect('index.php?admin/notice');
        }

        if ($task == "delete") {
            $this->crud_model->delete_notice_info($notice_id);
            redirect('index.php?admin/notice');
        }

        $data['notice_info'] = $this->crud_model->select_notice_info();
        $data['page_name'] = 'manage_notice';
        $data['page_title'] = get_phrase('noticeboard');
        $this->load->view('backend/index', $data);
    }

    function record($task = "", $param01 = "", $param02 = "", $param03 = "", $param04 = "", $param05 = ""){
        //var_dump(array($task, $param01, $param02, $param03, $param04, $param05));die;
        if ($this->session->userdata('admin_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }

        if($param01 != "" && $param01 == "search" && $param02 == "patient"){
            $patient_number_id = $this->input->post('patient_identification');
            $patient = $this->crud_model->select_patient_info_by_patient_number_id($patient_number_id);
            if(!empty($patient)){
                redirect(base_url()."index.php?admin/record/new/patient/{$patient[0]['number_id']}");
            }
        }else if($param01 != "" && $param01 == "patient"){
            $patient = $this->crud_model->select_patient_info_by_patient_number_id($param02);
            if(!empty($patient)){
                $patient_number_id = $patient[0]['number_id'];
            }
        }

        if($task=="new" || $task=="edit"){
            $data['doctor_list'] = $this->crud_model->select_doctor_info();
            $data['nurse_list'] = $this->crud_model->select_nurse_info();
            $data['bed_list'] = $this->crud_model->select_bed_info();
            $data['surgery_room_list'] = $this->crud_model->select_surgery_room_info();
            $data['surgery_status_list'] = $this->crud_model->select_surgery_status_info();
            $data['surgery_cancelation_reason_list'] = $this->crud_model->select_surgery_cancelation_reason_info();
            $data['destination_list'] = $this->crud_model->select_destination_info();
            $data['boolean_list'] = $this->crud_model->select_boolean_info();
            $data['clinical_record_closed_status'] = $this->crud_model->select_clinical_record_status_by_reference('closed');
            $data['clinical_record_reopened_status'] = $this->crud_model->select_clinical_record_status_by_reference('reopened');
        }

        if($task == "new"){
            $data['page_name']      = 'add_clinical_record';

            if(!empty($patient_number_id)){
                $data['patient'] = $this->crud_model->select_patient_info_by_patient_number_id($patient_number_id);
            }
        }

        if($task == "edit" && $param01!=""){
            $data['page_name'] = 'edit_clinical_record';
            $data['clinical_record'] = $this->crud_model->select_clinical_record_info_by_id($param01)[0];
            $data['clinical_record']['orthopedist_detail'] = $this->crud_model->select_doctor_info_by_id($data['clinical_record']['orthopedist']);
            $data['clinical_record']['modifier'] = $this->crud_model->select_user_info($data['clinical_record']['modifier_type'], $data['clinical_record']['modifier']);
            $data['surgeries'] = $this->crud_model->select_surgeries_by_clinical_record_id($param01);
            $data['adverse_events'] = $this->crud_model->select_adverse_events_by_clinical_record_id($param01);
            $data['patient'] = $this->crud_model->select_patient_info_by_patient_id($data['clinical_record']['patient_id'])[0];
            $data['surgery_edit'] = false;
            $data['adverse_event_edit'] = false;
            if($param02=="surgery" && $param03=="edit" && $param04!=""){
                $data['surgery'] = $this->crud_model->select_surgery_info($param04);
                $data['surgery_edit'] = true;
            }
            if($param02=="adverse" && $param03=="event" && $param04=="edit" && $param05 != ""){
                $data['adverse_event'] = $this->crud_model->select_adverse_event_info($param05);
                $data['adverse_event_edit'] = true;
            }
        }

        if($task == "update" && $param01!=""){
            $data = $this->crud_model->update_clinical_record($param01);
            $this->session->set_flashdata('message' , get_phrase('record_info_saved_successfuly'));
            redirect(base_url().'index.php?admin/record/edit/'.$data['clinical_record_id']);
        }

        if ($task == "create"){
            $data = $this->crud_model->create_record();
            $this->session->set_flashdata('message' , get_phrase('record_info_saved_successfuly'));
            redirect(base_url().'index.php?admin/record/edit/'.$data['clinical_record_id']);
        }

        if($task == "search"){
            $data['page_name'] = 'search_clinical_record';
            $data['search'] = $param01;
            $data['records_count'] = 0;
            if($param01 == "" && null !== $this->input->post('search',null)){
                redirect(base_url()."index.php?admin/record/search/".$this->input->post('search'));
            }else if($param01!=""){
                $data['records'] = $this->crud_model->search_clinical_record($param01);
                $data['patients'] = array();
                foreach($data['records'] as $record){
                    if(!isset($data['patients'][$record['patient_id']])){
                        $data['patients'][$record['patient_id']] = array(
                            'patient_id'=>$record['patient_id'],
                            'number_id'=>$record['number_id'],
                            'name'=>$record['name'],
                            'sex'=>$record['sex'],
                            'payer'=>$record['payer'],
                            'blood_group'=>$record['blood_group'],
                            'clinical_record_patient_id'=>$record['clinical_record_patient_id']
                        );
                    }
                }
                $data['records_count'] = count($data['records']);
            }
            //var_dump($data['records']);die;
        }

        if($task == "delete"){
            $this->session->set_flashdata('message', get_phrase('record_info_deleted_successfuly'));
            if($param01=="surgery"){
                $this->crud_model->delete_surgery($this->input->post('surgery_id'));
                redirect(base_url() . 'index.php?admin/record/edit/' . $this->input->post('clinical_record_id'));
            }else if($param01=="adverse_event"){
                $this->crud_model->delete_adverse_event($this->input->post('adverse_event_id'));
                redirect(base_url() . 'index.php?admin/record/edit/' . $this->input->post('clinical_record_id'));
            }else if($param01=="") {
                $this->crud_model->delete_clinical_record($this->input->post('clinical_record_id'));
                redirect(base_url() . 'index.php?admin/record/search/' . $this->input->post('search'));
            }
        }

        $data['page_title']     = get_phrase('clinical_record');
        $this->load->view('backend/index', $data);
    }

    function report($param01 = "", $param02 = "", $param03 = "", $param04 = ""){
        $data['page_name']      = 'report';

        if($param01 == "ranking" && $param02 == "surgeries"){
            $data['page_title']     = get_phrase('report_ranking_surgeries');
            $data['report_map'] = array('surgery_count'=>get_phrase('surgery_count'), 'name'=>get_phrase('name'));
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from')?$this->input->post('from'):$borders['from'];
            $data['to'] = $this->input->post('to')?$this->input->post('to'):$borders['to'];
            $data['report'] = $this->crud_model->select_report_ranking_surgeries($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_ranking_surgeries_count($data['from'],$data['to']);
            $data['report_url'] = "$param01/$param02";
        }
        		
		if($param01 == "without" && $param02 == "giving" &&  $param03 == "high") {
            $data['page_title'] = get_phrase('report_without_giving_high');
            $data['report_map'] = array(
                'number_id'=>get_phrase('number_id'),
                'name'=>get_phrase('name'),
                'age'=>get_phrase('age'),
                'payer'=>get_phrase('payer'),
                'admission_date'=>get_phrase('admission_date'),
                'diagnosis'=>get_phrase('diagnosis'),
                'medicine'=>get_phrase('medicine'),
                'surgery'=>get_phrase('surgery'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_whitout_giving_high($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_whitout_giving_high_count($data['from'],$data['to']);
            $data['report_url'] = "$param01/$param02/$param03";
        }

        if($param01 == "programming" && $param02 =="surgery")
        {
            $data['page_title'] = get_phrase('programming_surgery');
            $data['report_map'] = array(
                'date'=>get_phrase('date'),
                'time'=>get_phrase('time'),
                'room'=>get_phrase('room'),
                'name'=>get_phrase('name'),
                'number_id'=>get_phrase('number_id'),
                'age'=>get_phrase('age'),
                'cx_code'=>get_phrase('cx_code'),
                'surgical_procedure'=>get_phrase('surgical_procedure'),
                'osteosynthesis_materials_requested'=>get_phrase('osteosynthesis_materials_requested'),
                'surgeon'=>get_phrase('surgeon'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_programming_surgery($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_programming_surgery_count($data['from'],$data['to']);
            $data['report_url'] = "$param01/$param02";
        }
		
		if($param01 == "census" && $param02 == "orthopedics") {
            $data['page_title'] = get_phrase('census_orthopedics');
            $data['report_map'] = array(
                'admission_date'=>get_phrase('admission_date'),
				'bed'=>get_phrase('bed'),
                'name'=>get_phrase('name'),
				'number_id'=>get_phrase('number_id'),
                'age'=>get_phrase('age'),
                'payer'=>get_phrase('payer'),
                'diagnosis'=>get_phrase('diagnosis'),                
                'paraclinics'=>get_phrase('paraclinics'),
				'medicine'=>get_phrase('medicine'),
				'destiny'=>get_phrase('destiny'),
				'out_date'=>get_phrase('out_date'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_census_orthopedics($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_census_orthopedics_count($data['from'],$data['to']);
            $data['report_url'] = "$param01/$param02";
        }
		
		if($param01 == "cancelations")
        {
            $data['page_title'] = get_phrase('cancelations');
            $data['report_map'] = array(
                'date'=>get_phrase('date'),
                'room'=>get_phrase('room'),
                'name'=>get_phrase('name'),
                'number_id'=>get_phrase('number_id'),
                'surgeon'=>get_phrase('surgeon'),
                'reason'=>get_phrase('cancelation_reason'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_cancelations($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_cancelations_count($data['from'],$data['to']);
            $data['report_url'] = "$param01";
        }

        if($param01 == "room"){
            $data['page_title'] = get_phrase('room');
            $data['report_map'] = array(
                'room'=>get_phrase('room'),
                'date'=>get_phrase('date'),
                'name'=>get_phrase('name'),
                'number_id'=>get_phrase('number_id'),
                'room'=>get_phrase('room'),
                'surgeon'=>get_phrase('surgeon'),
                'status'=>get_phrase('status'),
                'destiny'=>get_phrase('destiny'),
            );

            $data['from'] = $this->input->post('from') ? $this->input->post('from') : (new DateTime())->format('Y-m-d');
            $data['autorefresh']= 60;
            //$data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_room($data['from']);
            $data['report_count'] = $this->crud_model->select_report_room_count($data['from']);
            $data['report_url'] = "$param01";
        }        
		
		if($param01 == "adverse" && $param02 =="events"){
            $data['page_title'] = get_phrase('adverse_events');
            $data['report_map'] = array(
                'name'=>get_phrase('name'),
                'number_id'=>get_phrase('number_id'),
                'date'=>get_phrase('date'),
                'observations'=>get_phrase('observations'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_adverse_events($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_adverse_events_count($data['from'],$data['to']);
            $data['report_url'] = "$param01/$param02";
        }

        $this->load->view('backend/index', $data);
    }





    private function getMonthBordersDays($date = false){
        if($date==false){
            $date = new DateTime();
        }
        $to = new DateTime($date->format('Y-m-1'));
        $from = new DateTime($date->format('Y-m-').'1');
        return array("from"=>$from->format('Y-m-d'), "to"=>$to->format('Y-m-t'));
    }

}