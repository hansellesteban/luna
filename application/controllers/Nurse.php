<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Nurse extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->library('session');
    }
    
    function index() 
    {
        if ($this->session->userdata('nurse_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        $data['page_name']      = 'dashboard';
        $data['page_title']     = get_phrase('nurse_dashboard');
        $this->load->view('backend/index', $data);
    }
    
    function patient($task = "", $patient_id = "", $from_record = "") {
        if ($this->session->userdata('nurse_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_patient_info();
            $this->session->set_flashdata('message', get_phrase('patient_info_saved_successfuly'));
            redirect(
                !empty($from_record)?
                    base_url().'index.php?nurse/record/new/patient/'.$this->input->post('number_id'):
                    base_url().'index.php?nurse/patient');
        }

        if ($task == "update") {
            $this->crud_model->update_patient_info($patient_id);
            $this->session->set_flashdata('message', get_phrase('patient_info_updated_successfuly'));
            redirect(base_url().'index.php?nurse/patient');
        }

        if ($task == "delete") {
            $this->crud_model->delete_patient_info($patient_id);
            redirect(base_url().'index.php?nurse/patient');
        }

        $data['patient_info'] = $this->crud_model->select_patient_info();
        $data['page_name'] = 'manage_patient';
        $data['page_title'] = get_phrase('patient');
        $this->load->view('backend/index', $data);
    }
    
    function bed($task = "", $bed_id = "")
    {
        if ($this->session->userdata('nurse_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        if ($task == "create")
        {
            $this->crud_model->save_bed_info();
            $this->session->set_flashdata('message' , get_phrase('bed_info_saved_successfuly'));
            redirect('index.php?nurse/bed');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_bed_info($bed_id);
            $this->session->set_flashdata('message' , get_phrase('bed_info_updated_successfuly'));
            redirect('index.php?nurse/bed');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_bed_info($bed_id);
            redirect('index.php?nurse/bed');
        }
        
        $data['bed_info']       = $this->crud_model->select_bed_info();
        $data['page_name']      = 'manage_bed';
        $data['page_title']     = get_phrase('bed');
        $this->load->view('backend/index', $data);
    }
    
    function bed_allotment($task = "", $bed_allotment_id = "")
    {
        if ($this->session->userdata('nurse_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        if ($task == "create")
        {
            $this->crud_model->save_bed_allotment_info();
            $this->session->set_flashdata('message' , get_phrase('bed_allotment_info_saved_successfuly'));
            redirect('index.php?nurse/bed_allotment');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_bed_allotment_info($bed_allotment_id);
            $this->session->set_flashdata('message' , get_phrase('bed_allotment_info_updated_successfuly'));
            redirect('index.php?nurse/bed_allotment');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_bed_allotment_info($bed_allotment_id);
            redirect('index.php?nurse/bed_allotment');
        }
        
        $data['bed_allotment_info'] = $this->crud_model->select_bed_allotment_info();
        $data['page_name']          = 'manage_bed_allotment';
        $data['page_title']         = get_phrase('bed_allotment');
        $this->load->view('backend/index', $data);
    }
    
    function blood_bank($task = "", $blood_group_id = "")
    {
        if ($this->session->userdata('nurse_login') != 1)
        {
                $this->session->set_userdata('last_page' , current_url());
                redirect(base_url(), 'refresh');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_blood_bank_info($blood_group_id);
            $this->session->set_flashdata('message' , get_phrase('blood_bank_info_updated_successfuly'));
            redirect('index.php?nurse/blood_bank');
        }
        
        $data['blood_bank_info']    = $this->crud_model->select_blood_bank_info();
        $data['page_name']          = 'manage_blood_bank';
        $data['page_title']         = get_phrase('blood_bank');
        $this->load->view('backend/index', $data);
    }
    
    function blood_donor($task = "", $blood_donor_id = "")
    {
        if ($this->session->userdata('nurse_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        if ($task == "create")
        {
            $this->crud_model->save_blood_donor_info();
            $this->session->set_flashdata('message' , get_phrase('blood_donor_info_saved_successfuly'));
            redirect('index.php?nurse/blood_donor');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_blood_donor_info($blood_donor_id);
            $this->session->set_flashdata('message' , get_phrase('blood_donor_info_updated_successfuly'));
            redirect('index.php?nurse/blood_donor');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_blood_donor_info($blood_donor_id);
            redirect('index.php?nurse/blood_donor');
        }
        
        $data['blood_donor_info']   = $this->crud_model->select_blood_donor_info();
        $data['page_name']          = 'manage_blood_donor';
        $data['page_title']         = get_phrase('blood_donor');
        $this->load->view('backend/index', $data);
    }
    
    function report($task = "", $report_id = "")
    {
        if ($this->session->userdata('nurse_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        if ($task == "create")
        {
            $this->crud_model->save_report_info();
            $this->session->set_flashdata('message' , get_phrase('report_info_saved_successfuly'));
            redirect('index.php?nurse/report');
        }
        
        if ($task == "update")
        {
            $this->crud_model->update_report_info($report_id);
            $this->session->set_flashdata('message' , get_phrase('report_info_updated_successfuly'));
            redirect('index.php?nurse/report');
        }
        
        if ($task == "delete")
        {
            $this->crud_model->delete_report_info($report_id);
            redirect('index.php?nurse/report');
        }
        
        $data['page_name']      = 'manage_report';
        $data['page_title']     = get_phrase('report');
        $this->load->view('backend/index', $data);
    }
    
    function profile($task = "")
    {
        if ($this->session->userdata('nurse_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }
        
        $nurse_id      = $this->session->userdata('login_user_id');
        if ($task == "update")
        {
            $this->crud_model->update_nurse_info($nurse_id);
            $this->session->set_flashdata('message' , get_phrase('profile_info_updated_successfuly'));
            redirect('index.php?nurse/profile');
        }
        
        if ($task == "change_password")
        {
            $password               = $this->db->get_where('nurse', array('nurse_id' => $nurse_id))->row()->password;
            $old_password           = sha1($this->input->post('old_password'));
            $new_password           = $this->input->post('new_password');
            $confirm_new_password   = $this->input->post('confirm_new_password');
            
            if($password==$old_password && $new_password==$confirm_new_password)
            {
                $data['password'] = sha1($new_password);
                
                $this->db->where('nurse_id',$nurse_id);
                $this->db->update('nurse',$data);
                
                $this->session->set_flashdata('message' , get_phrase('password_info_updated_successfuly'));
                redirect('index.php?nurse/profile');
            }
            else
            {
                $this->session->set_flashdata('message' , get_phrase('password_update_failed'));
                redirect('index.php?nurse/profile');
            }
        }
        
        $data['page_name']      = 'edit_profile';
        $data['page_title']     = get_phrase('profile');
        $this->load->view('backend/index', $data);
    }



    function record($task = "", $param01 = "", $param02 = "", $param03 = "", $param04 = "", $param05 = ""){
        //var_dump(array($task, $param01, $param02, $param03, $param04, $param05));die;
        if ($this->session->userdata('nurse_login') != 1)
        {
            $this->session->set_userdata('last_page' , current_url());
            redirect(base_url(), 'refresh');
        }

        if($param01 != "" && $param01 == "search" && $param02 == "patient"){
            $patient_number_id = $this->input->post('patient_identification');
            $patient = $this->crud_model->select_patient_info_by_patient_number_id($patient_number_id);
            if(!empty($patient)){
                redirect(base_url()."index.php?nurse/record/new/patient/{$patient[0]['number_id']}");
            }
        }else if($param01 != "" && $param01 == "patient"){
            $patient = $this->crud_model->select_patient_info_by_patient_number_id($param02);
            if(!empty($patient)){
                $patient_number_id = $patient[0]['number_id'];
            }
        }

        if($task=="new" || $task=="edit"){
            $data['doctor_list'] = $this->crud_model->select_doctor_info();
            $data['nurse_list'] = $this->crud_model->select_nurse_info();
            $data['bed_list'] = $this->crud_model->select_bed_info();
            $data['surgery_room_list'] = $this->crud_model->select_surgery_room_info();
            $data['surgery_status_list'] = $this->crud_model->select_surgery_status_info();
            $data['surgery_cancelation_reason_list'] = $this->crud_model->select_surgery_cancelation_reason_info();
            $data['destination_list'] = $this->crud_model->select_destination_info();
            $data['boolean_list'] = $this->crud_model->select_boolean_info();
            $data['clinical_record_closed_status'] = $this->crud_model->select_clinical_record_status_by_reference('closed');
            $data['clinical_record_reopened_status'] = $this->crud_model->select_clinical_record_status_by_reference('reopened');
        }

        if($task == "new"){
            $data['page_name']      = 'add_clinical_record';

            if(!empty($patient_number_id)){
                $data['patient'] = $this->crud_model->select_patient_info_by_patient_number_id($patient_number_id);
            }
        }

        if($task == "edit" && $param01!=""){
            $data['page_name'] = 'edit_clinical_record';
            $data['clinical_record'] = $this->crud_model->select_clinical_record_info_by_id($param01)[0];
            $data['clinical_record']['orthopedist_detail'] = $this->crud_model->select_doctor_info_by_id($data['clinical_record']['orthopedist']);
            //var_dump(array($data['clinical_record']['modifier_type'], $data['clinical_record']['modifier']));die;
            $data['clinical_record']['modifier'] = $this->crud_model->select_user_info($data['clinical_record']['modifier_type'], $data['clinical_record']['modifier']);
            $data['surgeries'] = $this->crud_model->select_surgeries_by_clinical_record_id($param01);
            $data['adverse_events'] = $this->crud_model->select_adverse_events_by_clinical_record_id($param01);
            $data['patient'] = $this->crud_model->select_patient_info_by_patient_id($data['clinical_record']['patient_id'])[0];
            //var_dump($data['patient']);die;
            $data['surgery_edit'] = false;
            $data['adverse_event_edit'] = false;
            if($param02=="surgery" && $param03=="edit" && $param04!=""){
                $data['surgery'] = $this->crud_model->select_surgery_info($param04);
                $data['surgery_edit'] = true;
            }
            if($param02=="adverse" && $param03=="event" && $param04=="edit" && $param05 != ""){
                $data['adverse_event'] = $this->crud_model->select_adverse_event_info($param05);
                $data['adverse_event_edit'] = true;
            }
        }

        if($task == "update" && $param01!=""){
            $data = $this->crud_model->update_clinical_record($param01);
            $this->session->set_flashdata('message' , get_phrase('record_info_saved_successfuly'));
            redirect(base_url().'index.php?nurse/record/edit/'.$data['clinical_record_id']);
        }

        if ($task == "create"){
            $data = $this->crud_model->create_record();
            $this->session->set_flashdata('message' , get_phrase('record_info_saved_successfuly'));
            redirect(base_url().'index.php?nurse/record/edit/'.$data['clinical_record_id']);
        }

        if($task == "search"){
            $data['page_name'] = 'search_clinical_record';
            $data['search'] = $param01;
            $data['records_count'] = 0;
            if($param01 == "" && null !== $this->input->post('search',null)){
                redirect(base_url()."index.php?nurse/record/search/".$this->input->post('search'));
            }else if($param01!=""){
                $data['records'] = $this->crud_model->search_clinical_record($param01);
                $data['patients'] = array();
                foreach($data['records'] as $record){
                    if(!isset($data['patients'][$record['patient_id']])){
                        $data['patients'][$record['patient_id']] = array(
                            'patient_id'=>$record['patient_id'],
                            'number_id'=>$record['number_id'],
                            'name'=>$record['name'],
                            'sex'=>$record['sex'],
                            'payer'=>$record['payer'],
                            'blood_group'=>$record['blood_group'],
                            'clinical_record_patient_id'=>$record['clinical_record_patient_id']
                        );
                    }
                }
                $data['records_count'] = count($data['records']);
            }
            //var_dump($data['records']);die;
        }

        if($task == "delete"){
            $this->session->set_flashdata('message', get_phrase('record_info_deleted_successfuly'));
            if($param01=="surgery"){
                $this->crud_model->delete_surgery($this->input->post('surgery_id'));
                redirect(base_url() . 'index.php?nurse/record/edit/' . $this->input->post('clinical_record_id'));
            }else if($param01=="adverse_event"){
                $this->crud_model->delete_adverse_event($this->input->post('adverse_event_id'));
                redirect(base_url() . 'index.php?nurse/record/edit/' . $this->input->post('clinical_record_id'));
            }else if($param01=="") {
                $this->crud_model->delete_clinical_record($this->input->post('clinical_record_id'));
                redirect(base_url() . 'index.php?nurse/record/search/' . $this->input->post('search'));
            }
        }

        $data['page_title']     = get_phrase('clinical_record');
        $this->load->view('backend/index', $data);
    }

    function reports($param01 = "", $param02 = "", $param03 = "", $param04 = ""){
        $data['page_name']      = 'report';

        if($param01 == "without" && $param02 == "giving" &&  $param03 == "high") {
            $data['page_title'] = get_phrase('report_without_giving_high');
            $data['report_map'] = array(
                'number_id'=>get_phrase('number_id'),
                'name'=>get_phrase('name'),
                'age'=>get_phrase('age'),
                'payer'=>get_phrase('payer'),
                'admission_date'=>get_phrase('admission_date'),
                'diagnosis'=>get_phrase('diagnosis'),
                'medicine'=>get_phrase('medicine'),
                'surgery'=>get_phrase('surgery'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_whitout_giving_high($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_whitout_giving_high_count($data['from'],$data['to']);
            $data['report_url'] = "$param01/$param02/$param03";
        }

        if($param01 == "programming" && $param02 =="surgery")
        {
            $data['page_title'] = get_phrase('programming_surgery');
            $data['report_map'] = array(
                'date'=>get_phrase('date'),
                'time'=>get_phrase('time'),
                'room'=>get_phrase('room'),
                'name'=>get_phrase('name'),
                'number_id'=>get_phrase('number_id'),
                'age'=>get_phrase('age'),
                'cx_code'=>get_phrase('cx_code'),
                'surgical_procedure'=>get_phrase('surgical_procedure'),
                'osteosynthesis_materials_requested'=>get_phrase('osteosynthesis_materials_requested'),
                'surgeon'=>get_phrase('surgeon'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_programming_surgery($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_programming_surgery_count($data['from'],$data['to']);
            $data['report_url'] = "$param01/$param02";
        }
		
		if($param01 == "census" && $param02 == "orthopedics") {
            $data['page_title'] = get_phrase('census_orthopedics');
            $data['report_map'] = array(
                'admission_date'=>get_phrase('admission_date'),
				'bed'=>get_phrase('bed'),
                'name'=>get_phrase('name'),
				'number_id'=>get_phrase('number_id'),
                'age'=>get_phrase('age'),
                'payer'=>get_phrase('payer'),
                'diagnosis'=>get_phrase('diagnosis'),                
                'paraclinics'=>get_phrase('paraclinics'),
				'medicine'=>get_phrase('medicine'),
				'destiny'=>get_phrase('destiny'),
				'out_date'=>get_phrase('out_date'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_census_orthopedics($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_census_orthopedics_count($data['from'],$data['to']);
            $data['report_url'] = "$param01/$param02";
        }
		
		if($param01 == "cancelations")
        {
            $data['page_title'] = get_phrase('cancelations');
            $data['report_map'] = array(
                'date'=>get_phrase('date'),
                'room'=>get_phrase('room'),
                'name'=>get_phrase('name'),
                'number_id'=>get_phrase('number_id'),
                'surgeon'=>get_phrase('surgeon'),
                'reason'=>get_phrase('cancelation_reason'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_cancelations($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_cancelations_count($data['from'],$data['to']);
            $data['report_url'] = "$param01";
        }

        if($param01 == "room"){
            $data['page_title'] = get_phrase('room');
            $data['report_map'] = array(
                'room'=>get_phrase('room'),
                'date'=>get_phrase('date'),
                'name'=>get_phrase('name'),
                'number_id'=>get_phrase('number_id'),
                'room'=>get_phrase('room'),
                'surgeon'=>get_phrase('surgeon'),
                'status'=>get_phrase('status'),
                'destiny'=>get_phrase('destiny'),
            );

            $data['from'] = $this->input->post('from') ? $this->input->post('from') : (new DateTime())->format('Y-m-d');
            $data['autorefresh']= 60;
            //$data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_room($data['from']);
            $data['report_count'] = $this->crud_model->select_report_room_count($data['from']);
            $data['report_url'] = "$param01";
        }

        if($param01 == "adverse" && $param02 =="events"){
            $data['page_title'] = get_phrase('adverse_events');
            $data['report_map'] = array(
                'name'=>get_phrase('name'),
                'number_id'=>get_phrase('number_id'),
                'date'=>get_phrase('date'),
                'observations'=>get_phrase('observations'),
            );
            $borders = $this->getMonthBordersDays();
            $data['from'] = $this->input->post('from') ? $this->input->post('from') : $borders['from'];
            $data['to'] = $this->input->post('to') ? $this->input->post('to') : $borders['to'];
            $data['report'] = $this->crud_model->select_report_adverse_events($data['from'],$data['to']);
            $data['report_count'] = $this->crud_model->select_report_adverse_events_count($data['from'],$data['to']);
            $data['report_url'] = "$param01/$param02";
        }

        $this->load->view('backend/index', $data);
    }

    private function getMonthBordersDays($date = false){
        if($date==false){
            $date = new DateTime();
        }
        $to = new DateTime($date->format('Y-m-1'));
        $from = new DateTime($date->format('Y-m-').'1');
        return array("from"=>$from->format('Y-m-d'), "to"=>$to->format('Y-m-t'));
    }
}